//
//  AnswerModel.swift
//  Form
//
//  Created by MSU IT CS on 1/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper

class AnswerModel:Mappable{
    
    var user_item_id:Int?
    var choice_id:Int?
    var value:String?
    var values = [valuesModel]()
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        choice_id <- map["choice_id"]
        user_item_id <- map["user_item_id"]
        value <- map["value"]
        values <- map["values"]
        
        
    }
    
    
    
    
    
    
}
