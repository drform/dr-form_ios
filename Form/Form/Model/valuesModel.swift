//
//  valuesModel.swift
//  Form
//
//  Created by MSU IT CS on 20/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper


class valuesModel:Mappable{
    
    var id:Int?
    var image:String?
    
    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        
        id <- map["id"]
        image <- map["image"]
        
    }
    
    
    
    
    
    
}
