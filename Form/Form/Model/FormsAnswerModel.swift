//
//  FormsAnswerModel.swift
//  Form
//
//  Created by MSU IT CS on 13/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper

class FormsAnswerModel:Mappable{
    
    var id:Int?
    var order_no:Int?
    var question:String?
    var user_item_id:Int?
    var answer = [AnswerModel]()
    var data_type_id:Int?
    var datatype:String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        order_no <- map["order_no"]
        data_type_id <- map["data_type_id"]
        datatype <- map["datatype"]
        question <- map["question"]
        user_item_id <- map["user_item_id"]
        answer <- map["answer"]
    }
    
    
}
