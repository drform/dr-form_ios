//
//  GroupsModel.swift
//  Form
//
//  Created by MSU IT CS on 5/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper

class GroupsModel:Mappable{
    
    
    var id:Int?
    var user_id:Int?
    var title:String?
    var description:String?
    var created_at:String?
    var updated_at:String?
    var image:String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        user_id <- map["user_id"]
        title <- map["title"]
        description <- map["description"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        image <- map["image"]
        
    }
    
    
    
    
    
    
}



