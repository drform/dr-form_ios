//
//  AllimageandfileModel.swift
//  Form
//
//  Created by MSU IT CS on 4/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper

class allfileModel:Mappable{
    
    var id_data:Int?
    var file:String?
    var created_at:String?
    var filename:String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id_data <- map["id"]
        created_at <- map["created_at"]
        filename <- map["filename"]
        file <- map["file"]
        
    }
    
    
    
    
    
}





