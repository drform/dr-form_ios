//
//  mapper.swift
//  Form
//
//  Created by MSU IT CS on 31/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper


class ModelUserInfo:Mappable{
    
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    var image:String?
    var role:String?
    var user_id:Int?
    required init?(map: Map) {
        
    }
    
   func mapping(map: Map) {
        username <- map["username"]
        name <- map["name"]
        email <- map["email"]
        position <- map["position"]
        tel <- map["tel"]
        image <- map["image"]
        role <- map["role"]
        user_id <- map["id"]
    }
    
    
    
}
