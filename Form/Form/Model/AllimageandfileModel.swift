//
//  AllimageandfileModel.swift
//  Form
//
//  Created by MSU IT CS on 4/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper

class AllimageandfileModel:Mappable{
    
    var id_data:Int?
    var image:String?
    var created_at:String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
         id_data <- map["id"]
        image <- map["image"]
        created_at <- map["created_at"]
        
    }
    
    
    
    
    
}





