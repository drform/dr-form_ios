//
//  Users.swift
//  Form
//
//  Created by MSU IT CS on 14/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper

class UsersModel:Mappable{
    
    var id:Int?
    var username:String?
    var name:String?
    var email:String?
    var tel:String?
    var image:String?

    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        username <- map["username"]
        name <- map["name"]
        email <- map["email"]
        tel <- map["tel"]
        image <- map["image"]
        
    }
    
    
    
    
}
