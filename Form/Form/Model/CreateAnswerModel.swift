//
//  CreateAnswerModel.swift
//  Form
//
//  Created by MSU IT CS on 26/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper
import UIKit
class CreateAnswerModel:Mappable{
    
    var id:Int?
    var data:dataAnswerMOdel?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        id <- map["id"]
       data <- map["data"]
    }
    
    typealias completion = (_ result:CreateAnswerModel?,_ error:Error?) -> ()
    
    class func createanswermodel(with params: [String: AnyObject], url: URL,isFile:Bool?, file:Data?,namefile:String?, token:String,completion: @escaping completion){
        
        Services.shared.services(with: params, url: url, method: .post, isFile: isFile,  file: file, namefile: namefile, token: token) {(result,error) in
            
            if error == nil{
                let model = Mapper<CreateAnswerModel>().map(JSONObject: result)!
                completion(model,nil)
            }else{
                completion(nil,error)
            }
            
        }
    }
    
    
    
    
}
