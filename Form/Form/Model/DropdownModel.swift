//
//  DropdownModel.swift
//  Form
//
//  Created by MSU IT CS on 3/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper

class DropdownModel:Mappable{
    
    var id:Int?
    var form_item_id:Int?
    var dropdown_value:String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        form_item_id <- map["form_item_id"]
        dropdown_value <- map["dropdown_value"]
        
    }
    
    
    
    
    
    
    
}
