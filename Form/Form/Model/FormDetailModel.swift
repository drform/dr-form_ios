//
//  FormDetailModel.swift
//  Form
//
//  Created by MSU IT CS on 12/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper

class FormDetailModel:Mappable{
    
    var id:Int?
    var user_id:Int?
    var form_id:Int?
    var name:String?
    var image:String?
    var update:String?
    var amount:Int?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        user_id <- map["user_id"]
        form_id <- map["form_id"]
        name <- map["name"]
        image <- map["image"]
        update <- map["updated_at"]
        amount <- map["amount"]
        
    }
    
    
    
    
}
