//
//  CreaterModel.swift
//  Form
//
//  Created by MSU IT CS on 20/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper

class CreaterModel:Mappable{
    
    var username:String?
    var created_at:String?
    var image:String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
      username <- map["username"]
        created_at <- map["created_at"]
        image <- map["image"]
    }
    
    
    
    
}
