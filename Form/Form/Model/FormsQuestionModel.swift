//
//  ModelForms.swift
//  Form
//
//  Created by MSU IT CS on 11/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper

class FormsQuestionModel:Mappable{
    
    var id:Int?
    var form_id:Int?
    var order_no:Int?
    var title:String?
    var description:String?
    var required:Bool = false
    var av_search:Bool = false
    var data_type:String?
    var data_type_id:Int?
    var dropdown = [DropdownModel]()
   
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        form_id <- map["form_id"]
        order_no <- map["order_no"]
        title <- map["title"]
        description <- map["description"]
        required <- map["required"]
        av_search <- map["av_search"]
        data_type <- map["data_type"]
        data_type_id <- map["data_type_id"]
        dropdown <- map["dropdown"]
      
    }
    
    
}
