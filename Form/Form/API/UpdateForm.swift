//
//  UpdateForm.swift
//  Form
//
//  Created by MSU IT CS on 17/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class UpdateForm:Mappable{
    
    
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    
    }
    
    typealias WebServiceResponse = ([UpdateForm]?,Error?) -> ()
    typealias Response = (UpdateForm?,Error?) -> ()
    

    class func update(_ url:URL,group_id:Int,title:String,description:String,image:String,token:String,form_items:Any,completion: @escaping Response){
        
        
        let Form: [String:AnyObject] = [
            "form":[
//                "group_id":group_id,
                "title":title,
                "description":description,
                "image":image
                
                ] as AnyObject,
            "form_item":form_items as AnyObject
            
        ]
        let headers: HTTPHeaders = [
            "token":  token,
            //"Content-Type": "application/json"
        ]
        
	        Alamofire.request(url , method: .put, parameters: Form, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<UpdateForm>().map(JSONObject: json as! [String:Any])
                completion(array,nil)
                print("requet success")
                
                
            case .failure(let error):
                completion(nil,error)
                
            }
            
        }
    }
    
    
}
