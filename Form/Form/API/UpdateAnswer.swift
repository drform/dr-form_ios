//
//  UpdateAnswer.swift
//  Form
//
//  Created by MSU IT CS on 18/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class UpdateAnswer:Mappable{
    
    var message:String?
    
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        message <- map["message"]
    }
    typealias WebServiceResponse = ([UpdateAnswer]?,Error?) -> ()
    typealias Response = (UpdateAnswer?,Error?) -> ()
    

    
    class func update(_ url:URL,group_id:Int?,token:String,user_form_item:Any,completion: @escaping Response){
        
        
        let Form: [String:AnyObject] = [
            "user_form_item": user_form_item as AnyObject,

          
        ]
        let headers: HTTPHeaders = [
            "token":  token,
            //"Content-Type": "application/json"
        ]
        
        Alamofire.request(url , method: .put, parameters: Form, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<UpdateAnswer>().map(JSONObject: json as! [String:Any])
                completion(array,nil)
                print("requet success")
                
                
            case .failure(let error):
                completion(nil,error)
                
            }
            
        }
    }
}
