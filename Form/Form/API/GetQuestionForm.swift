//
//  GetQuestionForm.swift
//  Form
//
//  Created by MSU IT CS on 22/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class  GetQuestion: Mappable {
    
    
    var answer_count = [Any]()
    var questionitems = [FormsQuestionModel]()
    
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        questionitems <- map["item"]
        answer_count <- map["answer"]
    }
    
    typealias WebServiceResponse = ([GetQuestion]?,Error?) -> ()
    typealias Response = (GetQuestion?,Error?) -> ()

    class func get(_ url:URL,token:String,completion:@escaping Response){
        
        let headers: HTTPHeaders = [
            "token": token,
            
            ]
        Alamofire.request(url , method: .get, parameters: nil, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<GetQuestion>().map(JSONObject: json)!
                
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }

        
    }
    
    
}
