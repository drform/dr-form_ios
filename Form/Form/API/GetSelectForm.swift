//
//  GetSelectForm.swift
//  Form
//
//  Created by MSU IT CS on 12/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class GetSelectForm:Mappable{
    
    var amount:Int?
    var user_form = [FormDetailModel]()
   
    
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        user_form <- map["user_form"]
        amount <- map["amount"]
        
    }
    
    typealias Response  = (GetSelectForm?,Error?) -> ()
    typealias WebServiceResponse = ([GetSelectForm]?,Error?) -> ()
    
    class func get(_ url:URL,keyword:String?,offset:Int?,group_id:Int,token:String,completion:@escaping Response)
    {
        let headers: HTTPHeaders = [
            "token": token,
           
        ]
        
        var params : [String:AnyObject] = [
           // "share_role_id" : 2 as AnyObject,
            "limit": "5" as AnyObject,
            "offset": offset as AnyObject,
            "group_id": group_id as AnyObject,
            
        ]
        if keyword != nil{
            
            params["keyword"] = keyword as AnyObject
        }
     
        
        Alamofire.request(url , method: .get, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let array = Mapper<GetSelectForm>().map(JSONObject: json)
                
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
        
        
        
    }
    
    
}
