//
//  DeleteImage.swift
//  Form
//
//  Created by MSU IT CS on 26/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper
class Deleteimage:Mappable{
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    typealias WebServiceResponse = ([Deleteimage]?,Error?) -> ()
    typealias Response = (Deleteimage?,Error?) -> ()
    
    class func delete(_ url:URL,token:String,images:Any,completion:@escaping Response){
        
        let headers: HTTPHeaders = [
            "token": token,
            
            ]
        let params: [String:AnyObject] = [
            "images":images as AnyObject
        ]
        
        Alamofire.request(url , method: .delete, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<Deleteimage>().map(JSONObject: json)!
                
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
        
    

    }
}
