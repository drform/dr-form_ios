//
//  CreateGroup.swift
//  Form
//
//  Created by MSU IT CS on 18/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper


class CreateGroup:Mappable{
    
    
    var error:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        error <- map["error"]
    }
    
    typealias WebServiceResponse = ([CreateGroup]?,Error?) -> ()
    typealias Response = (CreateGroup?,Error?) -> ()
    
    
    class func create(_ url:URL,title:String,description:String,image:String,token:String,completion: @escaping Response){
        
        
        let Form: [String:AnyObject] = [
            "group":[
              
                "title":title,
                "description":description,
                "image":image
                
                ] as AnyObject
            
        ]
        let headers: HTTPHeaders = [
            "token":  token,
            //"Content-Type": "application/json"
        ]
        
        Alamofire.request(url , method: .post, parameters: Form, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<CreateGroup>().map(JSONObject: json as! [String:Any])
                
                completion(array,nil)
                print("requet success")
                
                
            case .failure(let error):
                completion(nil,error)
                
            }
            
        }
    }
    
    
    
}
