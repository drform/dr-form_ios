//
//  AdvanceSearch.swift
//  Form
//
//  Created by MSU IT CS on 15/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class AdvanceSearch:Mappable{
    
    var users = [FormDetailModel]()
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        users <- map["user_form"]
    }
    
    typealias WebServiceResponse = ([AdvanceSearch]?,Error?) -> ()
    typealias Response = (AdvanceSearch?,Error?) -> ()
    
    class func post(_ url:URL,user_form_item:Any,token:String,completion:@escaping Response){
        
        let headers: HTTPHeaders = [
            "token":  token,
          
        ]
        
        var params = [String:AnyObject]()
        params["user_form_item"] = user_form_item as AnyObject
        
        Alamofire.request(url , method: .post, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let array = Mapper<AdvanceSearch>().map(JSONObject: json)!
                print("request succes")
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
        
    }
    
    
    
    
    
}
