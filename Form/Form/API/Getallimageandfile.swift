//
//  Getallimageandfile.swift
//  Form
//
//  Created by MSU IT CS on 4/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper
import Alamofire

class Getallimageandfile:Mappable{
    
    var date:String?
    var data = [AllimageandfileModel]()
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
            date <- map["date"]
            data <- map["data"]
    }
    
    typealias WebServiceResponse = ([Getallimageandfile]?,Error?) -> ()
    typealias Response = (Getallimageandfile?,Error?) -> ()

    
    class func get(_ url:URL,type:String,group_id:Int,token:String,completion:@escaping WebServiceResponse){
        
        let headers: HTTPHeaders = [
            "token": token
            
            ]
        
        let params: [String:AnyObject] = [
            "limit": "20" as AnyObject,
            "offset": "0" as AnyObject,
            "group_id": group_id as AnyObject,
            "type": type as AnyObject
        ]
        
        Alamofire.request(url, method: .get, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result{
                
            case .success(let json):
                let array = Mapper<Getallimageandfile>().mapArray(JSONObject:json) ?? nil
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
            
            
        }
       
        
    }
    
    
    
}

