//
//  GetShareUsers.swift
//  Form
//
//  Created by MSU IT CS on 15/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import  Alamofire
import ObjectMapper


class GetShareUsers:Mappable{

    var id:Int?
    var user_id:Int?
    var form_id:Int?
    var share_role_id:Int?
    var created_at:String?
    var name:String?
    var email:String?
    var image:String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        user_id <- map["user_id"]
        form_id <- map["form_id"]
        share_role_id <- map["share_role_id"]
        created_at <- map["created_at"]
        name <- map["name"]
        email <- map["email"]
        image <- map["image"]
        
    }
    
    
    typealias WebServiceResponse = ([GetShareUsers]?,Error?) -> ()
    typealias Response = (GetShareUsers?,Error?) -> ()
    
    
    class func get(_ url:URL,token:String,completion:@escaping WebServiceResponse){
        
        let headers: HTTPHeaders = [
            "token": token,
            
            ]
        
        Alamofire.request(url , method: .get, parameters: nil, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<GetShareUsers>().mapArray(JSONObject: json)!
                
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
        
    }
    
    
    
}
