//
//  GetGroups.swift
//  Form
//
//  Created by MSU IT CS on 14/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class GetGroups : Mappable{
    
    var amount:Int?
    var groupsmodel = [GroupsModel]()
    
    

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
       amount <- map["amount"]
        groupsmodel <- map["groups"]
        
    }
    
    
    typealias WebServiceResponse = ([GetGroups]?,Error?) -> ()
    typealias Response = (GetGroups?,Error?) -> ()

    class func get(_ url:URL,keyword:String?,offset:Int?,token:String,completion:@escaping Response){
        
        let headers: HTTPHeaders = [
            "token": token,
            
            ]
        var params : [String:AnyObject] = [

            "limit": "6" as AnyObject,
            "offset": offset as AnyObject,
           
        ]
        if keyword != nil {
            params["keyword"] = keyword as AnyObject
        }
        
        Alamofire.request(url , method: .get, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result {
                
            case .success(let json):
                
                let array = Mapper<GetGroups>().map(JSONObject: json)!
                
                completion(array,nil)
            case .failure(let error):
                
             
                
                
                completion(nil,error)
            }
            
        }
        
        
    }
    
    
    
    
}
