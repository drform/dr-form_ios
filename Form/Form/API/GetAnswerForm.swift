//
//  GetAnswerForm.swift
//  Form
//
//  Created by MSU IT CS on 18/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class GetAnswer:Mappable{

    var answer = [FormsAnswerModel]()
    var creater:CreaterModel?
    
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
       
        answer <- map["data"]
        creater <- map["creater"]
    }
    
    typealias WebServiceResponse = ([GetAnswer]?,Error?) -> ()
    typealias Response = (GetAnswer?,Error?) -> ()

    class func get(_ url:URL,group_id:Int,token:String,completion:@escaping Response)
    {
        let headers: HTTPHeaders = [
            "token": token,
            
            ]
        let params: [String:AnyObject] = [
        
            "group_id": group_id as AnyObject
        ]
        
        Alamofire.request(url , method: .get, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<GetAnswer>().map(JSONObject: json)!
                
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
        
        
        
    }
    
    
    
    
}
