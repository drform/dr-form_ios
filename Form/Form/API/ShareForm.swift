//
//  ShareForm.swift
//  Form
//
//  Created by MSU IT CS on 14/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class ShareForm: Mappable {
    
    
    
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    
    typealias WebServiceResponse = ([ShareForm]?,Error?) -> ()
    typealias Response = (ShareForm?,Error?) -> ()
    
    
    class func post(_ url:URL,username:String,token:String,completion:@escaping Response){
        
        let headers: HTTPHeaders = [
            "token": token,
            
            ]
        
        var params = [String:AnyObject]()
        params["username"] = username as AnyObject
      
        
        var share = [String:AnyObject]()
        share["share"] = params as AnyObject
        
        Alamofire.request(url , method: .post, parameters: share, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<ShareForm>().map(JSONObject: json)!
                
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
        
    }
    
    
   
    
    
}
