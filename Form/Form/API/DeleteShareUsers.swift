//
//  DeleteShareUsers.swift
//  Form
//
//  Created by MSU IT CS on 15/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class DeleteShareUsers:Mappable{
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    
    typealias WebServiceResponse = ([DeleteShareUsers]?,Error?) -> ()
    typealias Response = (DeleteShareUsers?,Error?) -> ()
    
    class func delete(_ url:URL,token:String,completion:@escaping Response){
        
        let headers: HTTPHeaders = [
            "token": token,
            
            ]
        
        Alamofire.request(url , method: .delete, parameters: nil, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<DeleteShareUsers>().map(JSONObject: json)!
                
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
        
    }
    
    
    
    
    
}
