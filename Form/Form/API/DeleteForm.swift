//
//  DeleteForm.swift
//  Form
//
//  Created by MSU IT CS on 28/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class deleteForm:Mappable{
    
    
    
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    typealias WebServiceResponse = ([deleteForm]?,Error?) -> ()
    typealias Response = (deleteForm?,Error?) -> ()
    
    
    
    class func delete(_ url:URL,token:String,completion:@escaping Response){
        
        
        let headers: HTTPHeaders = [
            "token":  token,
            
            //  "Content-Type": "application/json"
        ]
        
        
        
        Alamofire.request(url , method: .delete, parameters: nil, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let array = Mapper<deleteForm>().map(JSONObject: json)!
                
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
        
    }
    
}
