//
//  Services.swift
//  Wearable
//
//  Created by Riseplus on 13/2/2562 BE.
//  Copyright © 2562 Riseplus. All rights reserved.
//

import UIKit
import Alamofire

// MARK: = Access token


// MARK: = Normal URL


class Services {
    
    // MARK: = Share Instance
    static let shared = Services()
    
    // MARK: = Set Header
   
    
    // MARK: = Return service result componants
    typealias ServiceCompletion = (_ success: AnyObject?, _ error: NSError?) -> ()
    
    // MARK: = Service
//    func service(with params: [String: AnyObject], url: String!, method: HTTPMethod!, isHeader: Bool!, isActivity: Bool!, service completion: @escaping ServiceCompletion) -> Void {
//
//        let URL = specificURL+url
//        let header: HTTPHeaders?
//        if isHeader {
//            header = Services.isHeader
//        }else {
//            header = nil
//        }
//
//        Alamofire.request(URL, method: method, parameters: params, headers: header).validate().responseJSON { response in
//            switch response.result {
//            case .success(let json):
//                completion(json as AnyObject?, nil)
//
//            case .failure(let error):
//                completion(nil, error as NSError?)
//
//            }
//        }
//    }
    
    // MARK: = MultipartFormData service
    func services(with params: [String: AnyObject], url: URL, method: HTTPMethod!,isFile:Bool?, file:Data?,namefile:String?, token:String, service completion: @escaping ServiceCompletion) -> Void {
        let header: HTTPHeaders?
      
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if isFile == true{
            multipartFormData.append(file!, withName: "user_form_item[][value]", fileName: "\(namefile!)", mimeType: "application/pdf")
            }
            for (key, value) in params {
                
                 multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: "user_form_item[][\(key)]")
                
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: method, headers: ["token": token]) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let err = response.error {
                        completion(nil, err as NSError?)
                        return
                    }
                    completion(response.result.value as AnyObject?, nil)

                }
            case .failure(let error):
                completion(nil, error as NSError?)

            }
        }
    }
}
