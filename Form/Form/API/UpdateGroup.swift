//
//  UpdateGroup.swift
//  Form
//
//  Created by MSU IT CS on 18/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper
import Alamofire

class UpdateGroup:Mappable{
    
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    
    typealias WebServiceResponse = ([UpdateGroup]?,Error?) -> ()
    typealias Response = (UpdateGroup?,Error?) -> ()
    
    
    class func update(_ url:URL,title:String,description:String,image:String,token:String,completion: @escaping Response){
        
        
        let Form: [String:AnyObject] = [
            "group":[
                "title":title,
                "description":description,
                
                
                ] as AnyObject,
            
        ]
        let headers: HTTPHeaders = [
            "token":  token,
            //"Content-Type": "application/json"
        ]
        
        Alamofire.request(url , method: .put, parameters: Form, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<UpdateGroup>().map(JSONObject: json as! [String:Any])
                completion(array,nil)
                print("requet success")
                
                
            case .failure(let error):
                completion(nil,error)
                
            }
            
        }
    }
    
    
}
