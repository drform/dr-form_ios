//
//  CreateForm.swift
//  Form
//
//  Created by MSU IT CS on 11/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper
import Alamofire

class CreateForm:Mappable{
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    
    typealias WebServiceResponse = ([CreateForm]?,Error?) -> ()
    typealias Response = (CreateForm?,Error?) -> ()
    
    class func create(_ url:URL,group_id:Int,title:String,description:String,image:String,token:String,form_items:Any,completion: @escaping Response){
    

        let Form: [String:AnyObject] = [
            "form":[
                    "group_id":group_id,
                    "title":title,
                    "description":description,
                    "image":image
                
                ] as AnyObject,
            "form_item":form_items as AnyObject
        
            ]
        let headers: HTTPHeaders = [
            "token":  token,
              //"Content-Type": "application/json"
        ]
        
        Alamofire.request(url , method: .post, parameters: Form, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<CreateForm>().map(JSONObject: json as! [String:Any])
                completion(array,nil)
                print("requet success")
                
                
            case .failure(let error):
                completion(nil,error)
                
            }
            
        }
    }
    
    
    
}

