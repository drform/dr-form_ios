//
//  Forgetpassword.swift
//  Form
//
//  Created by MSU IT CS on 19/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class Forgetpassword:Mappable{
    
    var message:String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        message <- map["message"]
    }
    
    typealias WebServiceResponse = ([Forgetpassword]?,Error?) -> ()
    typealias Response = (Forgetpassword?,Error?) -> ()
    
    class func create(_ url:URL,email:String?,pin:String?,completion: @escaping Response){
        
        
        var Form: [String:AnyObject] = [
            "user":[
                
                "email":email
                
                ] as AnyObject
            
        ]
        var params = [String:AnyObject]()
        
        if pin != nil{
            params["token"] = pin as AnyObject
             params["email"] = email as AnyObject
            Form["user"] = params as AnyObject
           
        }
        
        
        Alamofire.request(url , method: .post, parameters: Form, headers: nil).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<Forgetpassword>().map(JSONObject: json as! [String:Any])
                
                completion(array,nil)
                print("requet success")
                
                
            case .failure(let error):
                completion(nil,error)
                
            }
            
        }
    }
    
    
    
}
