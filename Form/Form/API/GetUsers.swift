//
//  GetUsers.swift
//  Form
//
//  Created by MSU IT CS on 14/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class GetUsers:Mappable{
    
    
    var users = [UsersModel]()
    
    
    
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        users <- map["users"]
        
    }
    
    
    typealias WebServiceResponse = ([GetUsers]?,Error?) -> ()
    typealias Response = (GetUsers?,Error?) -> ()
    
    
    class func get(_ url:URL,username:String,token:String,completion:@escaping Response){
        
        let headers: HTTPHeaders = [
            "token": token,
            
            ]
         var params = [String:AnyObject]()
        params["username"] = username as AnyObject
        
        Alamofire.request(url , method: .get, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<GetUsers>().map(JSONObject: json)!
                
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
        
    }
    
    
    
}
