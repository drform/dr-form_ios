//
//  GetFormDetail.swift
//  Form
//
//  Created by MSU IT CS on 12/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class GetFormDetail:Mappable{
    
    var id:Int?
    var Form_id:Int?
    var title:String?
    var image:String?
    var des:String?
    var owner:String?
    var amount:Int?
    required init?(map: Map) {
    
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        Form_id <- map["form_id"]
        title <- map["title"]
        image <- map["image"]
        des <- map["description"]
        owner <- map["owner"]
        amount <- map["amount"]
    }
    
    typealias WebServiceResponse = ([GetFormDetail]?,Error?) -> ()
    typealias Response = (GetFormDetail?,Error?) -> ()
    
    class func get(_ url:URL,group_id:Int,offset:Int?,allform:Bool?,keyword:String?,token:String,completion: @escaping WebServiceResponse)
    {
        
        let headers: HTTPHeaders = [
            "token":  token,
       
            //  "Content-Type": "application/json"
        ]
        var params : [String:AnyObject] = [
            
            "limit": "6" as AnyObject,
            "offset": offset as AnyObject,
            "group_id": group_id as AnyObject
        ]
        if keyword != nil{
            params["keyword"] = keyword as AnyObject
        }
        if allform == false{
          params["share_role_id"] = "1" as AnyObject
        }
      
        Alamofire.request(url , method: .get, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                if json == nil{
                    
                }
                
                let array = Mapper<GetFormDetail>().mapArray(JSONObject: json)
                    completion(array,nil)
                
                
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
    }
    
    
    
}
