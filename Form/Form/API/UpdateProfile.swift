//
//  UpdateProfile.swift
//  Form
//
//  Created by MSU IT CS on 19/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper
import Alamofire

class UpdateProfile:Mappable{
    
    var message:String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        message <- map["message"]
        
    }
    
    typealias WebServiceResponse = ([UpdateProfile]?,Error?) -> ()
    typealias Response = (UpdateProfile?,Error?) -> ()
    
    
    class func update(_ url:URL,name:String?,tel:String?,email:String?,image:String?,position:String?,oldpassword:String?,newpassword:String?,token:String,completion: @escaping Response){
        
         var Form = [String:AnyObject]()
        
        if oldpassword == nil{
       
            Form["user"] = [
                //                "group_id":group_id,
                "name":name,
                "tel":tel,
                "email":email,
                "image":image,
                "position":position
                
                ] as AnyObject
            
            
        }else if oldpassword != nil{
            Form["user"] = [
                
                "password":oldpassword,
                "password_new":newpassword
            ] as AnyObject
        }
        let headers: HTTPHeaders = [
            "token":  token,
            //"Content-Type": "application/json"
        ]
        
        Alamofire.request(url , method: .put, parameters: Form, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<UpdateProfile>().map(JSONObject: json as! [String:Any])
                completion(array,nil)
                
                
            case .failure(let error):
                completion(nil,error)
                
            }
            
        }
    }
    
    
}
