//
//  CreateAnswerForm.swift
//  Form
//
//  Created by MSU IT CS on 22/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class createanswer:Mappable{
    
    
    
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    
    typealias WebServiceResponse = ([createanswer]?,Error?) -> ()
    typealias Response = (createanswer?,Error?) -> ()
    
    
    class func create(_ url:URL,group_id:Int,token:String,isImage:Bool?,user_form_item:Any,completion: @escaping Response){
    
        let headers: HTTPHeaders = [
            "token":  token,
            //  "Content-Type": "application/json"
        ]
        let Form: [String:AnyObject] = [
            "user_form_item": user_form_item as AnyObject,
           // "group_id": group_id as AnyObject
            
        ]
  
        Alamofire.request(url , method: .post, parameters: Form, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<createanswer>().map(JSONObject: json as! [String:Any])
                completion(array,nil)
                print("requet success")
                
                
            case .failure(let error):
                completion(nil,error)
            }
            
        }
    }
    
    
}
