//
//  GetFormDetail.swift
//  Form
//
//  Created by MSU IT CS on 12/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class GetFormShare:Mappable{
    
    var id:Int?
    var Form_id:Int?
    var title:String?
    var image:String?
    var des:String?
    var owner:String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        Form_id <- map["form_id"]
        title <- map["title"]
        image <- map["image"]
        des <- map["description"]
        owner <- map["owner"]
    }
    
    typealias WebServiceResponse = ([GetFormShare]?,Error?) -> ()
    typealias Response = (GetFormShare?,Error?) -> ()
    
    class func get(_ url:URL,group_id:Int,token:String,completion: @escaping WebServiceResponse)
    {
        
        let headers: HTTPHeaders = [
            "token":  token,
            
            //  "Content-Type": "application/json"
        ]
        let params : [String:AnyObject] = [
            "share_role_id" : 2 as AnyObject,
            "limit": 20 as AnyObject,
            "offset": 0 as AnyObject,
            "group_id": group_id as AnyObject

        ]
        
        
        Alamofire.request(url , method: .get, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                let array = Mapper<GetFormShare>().mapArray(JSONObject: json)
                
                completion(array,nil)
            case .failure(let error):
                completion(nil,error)
            }
            
        }
        
    }
    
    
    
}
