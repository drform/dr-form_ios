//
//  EditFormView.swift
//  Form
//
//  Created by MSU IT CS on 28/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import Photos
import AVKit
import AVFoundation
import MobileCoreServices
import ParallaxHeader

class EditFormView: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var addquestionBt: UIButton!
    var datatype_api = [Int]()
    var groups_id:Int? = 1
    var formTitle:String?
    var desText:String?
    var imageString:String?
    var form_id:Int?
    var titleSegue:String?
    var desSegue:String?
    var imageSegue:UIImage?
    var token:String?
    var  headerview:EditFormHeaderView?
    var base64String:String?
    var dataTypeId = [Int]()
    var datatype = [String]()
    var textdata = [String]()
    var actionrequired = [Bool]()
    var actionfilter = [Bool]()
    var questionitems = [FormsQuestionModel]()
    var item_id = [Int]()
    var rowsCheckbox = [Int]()
    var rows = [Int]()
    var choices = [String]()
    var Rows = [Rowdid]()
    var dropdown_id = [Int]()
    var share_count:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        rows = [0,2]
        rowsCheckbox = [0,2]
        hideKeyboardWhenTappedAround()
        setHeaderView()
        tableview.dataSource = self
        tableview.delegate = self
        tableview.separatorStyle = .none
        self.tableview.setDynamicRowHeight(withEstimatedHeight: 100.0)
        formTitle = titleSegue
        desText = desSegue
        tableview.register(UINib(nibName: "TextfieldCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        tableview.register(UINib(nibName: "TitleCell", bundle: Bundle.main), forCellReuseIdentifier: "cellsave")
        
        tableview.register(UINib(nibName: "DropdownCell1", bundle: Bundle.main), forCellReuseIdentifier: "DropdownCell1")
        tableview.register(UINib(nibName: "DropdownCell2", bundle: Bundle.main), forCellReuseIdentifier: "DropdownCell2")
        tableview.register(UINib(nibName: "DropdownCell3", bundle: Bundle.main), forCellReuseIdentifier: "DropdownCell3")
        
        
        self.navigationItem.title = "Edit Form"
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
        let createBt = UIButton()
        createBt.setImage(UIImage(named: "save-file-option"), for: .normal)
        createBt.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
        createBt.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        createBt.addTarget(self, action: #selector(saveForm), for: .touchUpInside)
        createBt.layer.cornerRadius = createBt.frame.height/2
        createBt.clipsToBounds = true
        let item = UIBarButtonItem(customView: createBt)
        
        self.navigationItem.rightBarButtonItem = item
        
        addquestionBt.layer.cornerRadius = 24
        addquestionBt.clipsToBounds = true
        
        let link = URL(string:"\(url!)/form_items/\(form_id!)")
        GetQuestion.get(link!, token: token!){
            (result,error) in
            var j:Int? = 0
            self.questionitems = (result?.questionitems)!
            
            for tmp in self.questionitems{
                self.item_id.append(tmp.id!)
                self.dataTypeId.append(tmp.data_type_id!)
                self.datatype_api.append(tmp.data_type_id!)
                self.textdata.append(tmp.title!)
                self.actionrequired.append(tmp.required)
                self.actionfilter.append(tmp.av_search)
                self.datatype.append(tmp.data_type!)
               
                if tmp.data_type_id == 9{
                    var i:Int? = 0
                    self.Rows.append(Rowdid(type: "dropdown", rows: [0,2],choices: [""],dropdown_id :[0]))
                    while(i!<tmp.dropdown.count){
                        self.Rows[j!].rows.append(1)
                        
                        if i == 0{
                            self.Rows[j!].choices = [(tmp.dropdown[i!].dropdown_value!)]
                            self.Rows[j!].dropdown_id = [(tmp.dropdown[i!].id!)]
                        }else{
                            self.Rows[j!].choices.append(tmp.dropdown[i!].dropdown_value!)
                            self.Rows[j!].dropdown_id.append(tmp.dropdown[i!].id!)
                        }
                        i = i! + 1
                    }
                    
                }else if tmp.data_type_id == 7{
                    var i:Int? = 0
                    self.Rows.append(Rowdid(type: "checkbox", rows: [0,2],choices: [""],dropdown_id :[0]))

                    while(i!<tmp.dropdown.count){
                      self.Rows[j!].rows.append(1)
                        
                        
                        if i == 0{
                            self.Rows[j!].choices = [(tmp.dropdown[i!].dropdown_value!)]
                            self.Rows[j!].dropdown_id = [(tmp.dropdown[i!].id!)]
                        }else{
                            self.Rows[j!].choices.append(tmp.dropdown[i!].dropdown_value!)
                            self.Rows[j!].dropdown_id.append(tmp.dropdown[i!].id!)
                        }
                        
                        
                        i = i! + 1
                    }
                    
                    
                    
                }else{
                    self.Rows.append(Rowdid(type: tmp.data_type, rows: [0], choices: [""],dropdown_id: [0]))

                }
                j = j! + 1
            }
            
            
            self.tableview.reloadData()
            
        }
        
        // Do any additional setup after loading the view.
        
    }
    
    func setHeaderView() -> Void {
        
         headerview = EditFormHeaderView.instanceFromNib()
            headerview!.setUpView()
            headerview!.setNeedsLayout()
            headerview!.layoutIfNeeded()
        
            headerview!.delegate = self
        
        headerview?.titleText.text = titleSegue
        headerview?.DescriptionTextview.text = desSegue
        headerview?.imageView.image = imageSegue
        headerview?.shareBtn.setTitle("Share with \(share_count!) people", for: .normal)
        
        headerview?.importBtn.addTarget(self, action: #selector(importImage), for: .touchUpInside)
        headerview?.deleteBtn.addTarget(self, action: #selector(deleteform), for: .touchUpInside)
        headerview?.shareBtn.addTarget(self, action: #selector(shareForm), for: .touchUpInside)
        
         tableview.parallaxHeader.view = headerview!
         tableview.parallaxHeader.height = 430
         tableview.parallaxHeader.minimumHeight = 0
         tableview.parallaxHeader.mode = .bottomFill
    }
    override func viewDidAppear(_ animated: Bool) {
   
        let link1 = URL(string:"\(url!)/forms/\(self.form_id!)/shares")
        
        GetShareUsers.get(link1!, token: self.token!){
            (result,error) in
            self.share_count = result?.count
            print("get")
            
            
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "shareform"{
            
            let next = segue.destination as! SharewithmeView
            
            next.form_id = self.form_id
            next.token = self.token
            
        }
    }
    
    @objc func deleteform() {

        let alert = UIAlertController(title: "Are you sure?", message: "Delete Form", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                
                let link = URL(string:"\(url!)/forms/\(self.form_id!)")
                
                                    deleteForm.delete(link!, token: self.token!){
                                        (result,error) in
                
                                       self.navigationController?.popToRootViewController(animated: true)
                
                                    }
                
                
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("cancel")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
//
        
    }
    
    @objc func shareForm(){
        
        self.performSegue(withIdentifier: "shareform", sender: self)

        
    }
    @IBAction func addQuestion(_ sender: Any) {
         let alertController = UIAlertController(title: "ChooseDataType", message: "", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Long Answer", style: .default) { (action:UIAlertAction) in
            


             self.Rows.append(Rowdid(type: "Text", rows: [0], choices: [""],dropdown_id: [0]))
            self.datatype.append("Text")
            self.textdata.append("")
            self.actionrequired.append(false)
            self.actionfilter.append(false)
           self.dataTypeId.append(4)
            self.tableview.reloadData()
        }
        let action6 = UIAlertAction(title: "Short Answer", style: .default) { (action:UIAlertAction) in
            

            self.dataTypeId.append(3)
            self.Rows.append(Rowdid(type: "Text", rows: [0], choices: [""],dropdown_id: [0]))
            self.datatype.append("String")
            self.textdata.append("")
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tableview.reloadData()

        }
        let action7 = UIAlertAction(title: "Integer", style: .default) { (action:UIAlertAction) in
            
            self.dataTypeId.append(5)
            self.textdata.append("")
            self.datatype.append("Integer")
            self.Rows.append(Rowdid(type: "Integer", rows: [0], choices: [""],dropdown_id: [0]))
            
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tableview.reloadData()
        }
        let action8 = UIAlertAction(title: "Decimal", style: .default) { (action:UIAlertAction) in
            
            self.dataTypeId.append(6)
            self.textdata.append("")
            self.datatype.append("Float")
            self.Rows.append(Rowdid(type: "Float", rows: [0], choices: [""],dropdown_id: [0]))
            
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tableview.reloadData()
        }
        let action2 = UIAlertAction(title: "Date", style: .default) { (action:UIAlertAction) in
            self.dataTypeId.append(1)
            self.textdata.append("")
            self.datatype.append("Date")
            self.Rows.append(Rowdid(type: "Date", rows: [0], choices: [""],dropdown_id: [0]))
            
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tableview.reloadData()
        }
        let action3 = UIAlertAction(title: "Dropdown", style: .default) { (action:UIAlertAction) in
             self.dataTypeId.append(9)
            self.textdata.append("")
            self.datatype.append("Dropdown")
            self.actionrequired.append(false)
            self.actionfilter.append(false)
           self.Rows.append(Rowdid(type: "dropdown", rows: [0,1,2], choices: [""],dropdown_id: [0]))
            
            self.tableview.reloadData()
        }
        let action4 = UIAlertAction(title: "Checkbox", style: .default) { (action:UIAlertAction) in
           self.dataTypeId.append(7)
            self.textdata.append("")
            self.datatype.append("Checkbox")
             self.Rows.append(Rowdid(type: "Checkbox", rows: [0,1,2], choices: [""],dropdown_id: [0]))
            
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tableview.reloadData()
        }
        let action5 = UIAlertAction(title: "Image", style: .default) { (action:UIAlertAction) in
            self.dataTypeId.append(10)
            self.textdata.append("")
            self.datatype.append("Image")
            self.Rows.append(Rowdid(type: "Image", rows: [0,1,2], choices: [""],dropdown_id: [0]))
            
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tableview.reloadData()
        }
        let action10 = UIAlertAction(title: "File", style: .default){(action:UIAlertAction) in
            
            self.dataTypeId.append(8)
            self.textdata.append("")
            self.datatype.append("File")
            self.Rows.append(Rowdid(type: "File", rows: [0], choices: [""],dropdown_id: [0]))
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tableview.reloadData()
        }
        let actionCancel = UIAlertAction(title: "Cancel",style: .cancel,handler: nil)
        
        alertController.addAction(action1)
        alertController.addAction(action6)
        alertController.addAction(action7)
        alertController.addAction(action8)
        alertController.addAction(action2)
        alertController.addAction(action3)
        alertController.addAction(action4)
        alertController.addAction(action5)
        alertController.addAction(action10)
     
        

        alertController.addAction(actionCancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func saveForm(){
        
        var tmp = [String:AnyObject]()
        var array = [Any]()
        var dropdown = [String:AnyObject]()
        var checkbox = [String:AnyObject]()

        var arraydropdown = [Any]()
        var arraycheckbox = [Any]()
        
        
        var i:Int? = 0
        
        
        while(i!<self.dataTypeId.count){
            
            if i!+1 <= datatype_api.count {
                tmp["order_no"] = i!+1 as AnyObject
                tmp["data_type"] = datatype[i!] as AnyObject
                tmp["title"] = textdata[i!] as AnyObject
                
                tmp["id"] = item_id[i!] as AnyObject
                
                if (datatype[i!] == "Dropdown"){
                    var j:Int? = 0
                    
                    while(j!<self.Rows[i!].choices.count){
                        
                        if self.Rows[i!].dropdown_id[j!] != 0{
                            dropdown["id"] = self.Rows[i!].dropdown_id[j!] as AnyObject
                            dropdown["data"] = self.Rows[i!].choices[j!] as AnyObject
                            
                        }else{
                            dropdown.removeValue(forKey: "id")
                            dropdown["data"] = self.Rows[i!].choices[j!] as AnyObject
                            
                            
                        }
                        
                        j = j! + 1
                        arraydropdown.append(dropdown)
                        
                    }
                    tmp["value"] = arraydropdown as AnyObject
                    arraydropdown.removeAll()
                    
                }else if (datatype[i!] == "Checkbox"){
                    var j:Int? = 0
                    
                    while(j!<self.Rows[i!].choices.count){
                        
                        if self.Rows[i!].dropdown_id[j!] != 0{
                            dropdown["id"] = self.Rows[i!].dropdown_id[j!] as AnyObject
                            dropdown["data"] = self.Rows[i!].choices[j!] as AnyObject
                            
                        }else{
                            dropdown.removeValue(forKey: "id")
                            dropdown["data"] = self.Rows[i!].choices[j!] as AnyObject
                            
                            
                        }
                        
                        j = j! + 1
                        arraydropdown.append(dropdown)
                        
                    }
                    tmp["value"] = arraydropdown as AnyObject
                    arraydropdown.removeAll()
                }
                
                tmp["description"] = "" as AnyObject
                tmp["required"] = actionrequired[i!] as AnyObject
                tmp["av_search"] = actionfilter[i!] as AnyObject
                
                array.append(tmp)
                
                i = i! + 1
            }else{
                tmp["order_no"] = i!+1 as AnyObject
                tmp["data_type"] = datatype[i!] as AnyObject
                tmp["title"] = textdata[i!] as AnyObject
                
               tmp.removeValue(forKey: "id")
                
                if (datatype[i!] == "Dropdown"){
                    var j:Int? = 0
                    
                    while(j!<self.Rows[i!].choices.count){
                        
                        if self.Rows[i!].dropdown_id[j!] != 0{
                            dropdown["id"] = self.Rows[i!].dropdown_id[j!] as AnyObject
                            dropdown["data"] = self.Rows[i!].choices[j!] as AnyObject
                            
                        }else{
                            dropdown.removeValue(forKey: "id")
                            dropdown["data"] = self.Rows[i!].choices[j!] as AnyObject
                            
                            
                        }
                        
                        j = j! + 1
                        arraydropdown.append(dropdown)
                        
                    }
                    tmp["value"] = arraydropdown as AnyObject
                    arraydropdown.removeAll()
                    
                }else if (datatype[i!] == "Checkbox"){
                    var j:Int? = 0
                    
                    while(j!<self.Rows[i!].choices.count){
                        
                        if self.Rows[i!].dropdown_id[j!] != 0{
                            dropdown["id"] = self.Rows[i!].dropdown_id[j!] as AnyObject
                            dropdown["data"] = self.Rows[i!].choices[j!] as AnyObject
                            
                        }else{
                            dropdown.removeValue(forKey: "id")
                            dropdown["data"] = self.Rows[i!].choices[j!] as AnyObject
                            
                            
                        }
                        
                        j = j! + 1
                        arraydropdown.append(dropdown)
                        
                    }
                    tmp["value"] = arraydropdown as AnyObject
                    arraydropdown.removeAll()
                }
                
                tmp["description"] = "" as AnyObject
                tmp["required"] = actionrequired[i!] as AnyObject
                tmp["av_search"] = actionfilter[i!] as AnyObject
                
                array.append(tmp)
                
                i = i! + 1
            }
            
           
        }
        
        
        
        
        let link = URL(string:"\(url!)/forms/\(form_id!)")
        UpdateForm.update(link!, group_id: groups_id!, title: formTitle!, description: desText!, image: base64String ?? "", token: token!, form_items: array){
            (result,error) in
            

            if error == nil {
                alert_successView.instance.showAlert(title: "Update Form Success", message: "", alertType: .success)
                alert_successView.instance.tmp = {
                    
                    self.navigationController?.popToRootViewController(animated: true)

                }
            }else{
                
            }

        }
        
    }
    
    @objc func importImage() {
        
        let image = UIImagePickerController()
        image.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source",message: "Choose a source ", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            
            
            image.sourceType = .camera
            image.mediaTypes = [kUTTypeImage as String]
            image.allowsEditing = false
            
            self.present(image ,animated: true,completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Libraby", style: .default, handler: { (action:UIAlertAction) in
            
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            image.allowsEditing = false
            self.present(image ,animated: true)
            
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil ))
        
        self.present(actionSheet , animated: true ,completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            base64String = convertImageTobase64(format: .jpeg(1.0), image: image)!
            
            
            // print("++++++++++\(base64String)")
            headerview?.imageView.image = image
            
            
        }
        
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    public enum ImageFormat {
        case png
        case jpeg(CGFloat)
    }
    func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = image.pngData()
        case .jpeg(let compression): imageData = image.jpegData(compressionQuality: compression)
        }
        return imageData?.base64EncodedString()
    }
    
    
    
    
    
    
    
    
    
    
    
    
}





extension EditFormView:UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataTypeId[section] == 9 {
            return Rows[section].rows.count
        }else if dataTypeId[section] == 7{
            return Rows[section].rows.count
        }else{
            return 1
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataTypeId.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  sortResult = [Int]()
        var isRow = [Int]()
        var sortCheckbox = [Int]()
        var isRowCheckbox = [Int]()
        if dataTypeId[indexPath.section] == 9{
            
            sortResult = self.Rows[indexPath.section].rows.sorted()
            self.Rows[indexPath.section].rows = sortResult
            isRow = self.Rows[indexPath.section].rows
        }else if dataTypeId[indexPath.section] == 7{
//
            sortCheckbox = self.Rows[indexPath.section].rows.sorted()
          self.Rows[indexPath.section].rows = sortCheckbox
            isRowCheckbox = self.Rows[indexPath.section].rows

        }
        
        switch dataTypeId[indexPath.section]{
        case 4:
        let celltext = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TextfieldCell
        
       
        celltext.deleteBtnview.isHidden = true
        celltext.TitleText.text = textdata[indexPath.section]
        celltext.DatatypeLabel.text = "Long Answer"
        
        if actionrequired[indexPath.section] == false{
            celltext.requireView.isSelected = false
        }else
        {
            celltext.requireView.isSelected = true
            celltext.requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
        }
        if actionfilter[indexPath.section] == false{
            celltext.filterView.isSelected = false

            
        }else{
            celltext.filterView.isSelected = true
            celltext.filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
        }
//
        celltext.delegate = self as? TextfieldCellDelegate
        
        
        celltext.actionText = { (text) in
            
            
            if self.textdata.count > 0{
                
                
                
                if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                    
                    
                    print("ok")
                    self.textdata[indexPath.section] = text
                    self.datatype[indexPath.section] = "Text"
                    if self.dataTypeId.count < self.actionrequired.count{
                        self.actionrequired.removeLast()
                        self.actionfilter.removeLast()
                    }
                    
                    
                }else{
                    self.textdata.append(text)
                    self.datatype.append("Text")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                    
                }
                
                
            }else {
                self.textdata.append(text)
                self.actionrequired.append(false)
                self.actionfilter.append(false)
                
                self.datatype.append("Text")
            }
            
            print(self.textdata)
            print(self.actionrequired)
            print(self.actionfilter)
            
        }
        
        
        celltext.actionRequired = {(Bool) in
            
            if self.actionrequired.count > 0{
                if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                    
                    
                    print("ok")
                    self.actionrequired[indexPath.section] = Bool
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                }
                
                
            }else{
                self.actionrequired.append(Bool)
                
            }
            
        }
        
        
        celltext.actionFilter = {(Bool) in
            
            if self.actionfilter.count > 0{
                
                if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                    
                    
                    print("ok")
                    self.actionfilter[indexPath.section] = Bool
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                }
                
                
            }else{
                self.actionfilter.append(Bool)
                
            }
        }
        celltext.actionSave = {(save)
            in
            
            celltext.frame.size.height = 80
            
            
        }
        
        
        return celltext
        case 3:
            let cellstring = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TextfieldCell
            cellstring.deleteBtnview.isHidden = true
            
            cellstring.TitleText.text = textdata[indexPath.section]
            cellstring.DatatypeLabel.text = "Short Answer"
            
            
            if actionrequired[indexPath.section] == false{
                cellstring.requireView.isSelected = false
            }else
            {
                cellstring.requireView.isSelected = true
                cellstring.requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            }
            if actionfilter[indexPath.section] == false{
                cellstring.filterView.isSelected = false
                
                
            }else{
                cellstring.filterView.isSelected = true
                cellstring.filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            }
            cellstring.delegate = self as? TextfieldCellDelegate
            
            cellstring.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "String"
                        if self.dataTypeId.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("String")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("String")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            cellstring.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            cellstring.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return cellstring
        case 5:
            let cellint = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TextfieldCell
            
            cellint.deleteBtnview.isHidden = true
            cellint.TitleText.text = textdata[indexPath.section]
            cellint.DatatypeLabel.text = "Integer"
            
            if actionrequired[indexPath.section] == false{
                cellint.requireView.isSelected = false
            }else
            {
                cellint.requireView.isSelected = true
                cellint.requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            }
            if actionfilter[indexPath.section] == false{
                cellint.filterView.isSelected = false
                
                
            }else{
                cellint.filterView.isSelected = true
                cellint.filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
                
            }
            cellint.delegate = self as? TextfieldCellDelegate
            
            cellint.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "Integer"
                        if self.dataTypeId.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("Integer")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("Integer")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            cellint.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            cellint.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return cellint
        case 6:
            let cellfloat = tableview.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! TextfieldCell
            
           cellfloat.deleteBtnview.isHidden = true
            cellfloat.TitleText.text = textdata[indexPath.section]
            cellfloat.DatatypeLabel.text = "Decimal"
            if actionrequired[indexPath.section] == false{
                cellfloat.requireView.isSelected = false
            }else
            {
                cellfloat.requireView.isSelected = true
                cellfloat.requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            }
            if actionfilter[indexPath.section] == false{
                cellfloat.filterView.isSelected = false
                
                
            }else{
                cellfloat.filterView.isSelected = true
                cellfloat.filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
                
            }
            cellfloat.delegate = self as? TextfieldCellDelegate
            
            
            cellfloat.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "Float"
                        if self.dataTypeId.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("Float")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("Float")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            cellfloat.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            cellfloat.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return cellfloat
        case 1:
            let celldate = tableview.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! TextfieldCell
            
            celldate.deleteBtnview.isHidden = true
            celldate.TitleText.text = textdata[indexPath.section]
            celldate.DatatypeLabel.text = "Date"
            if actionrequired[indexPath.section] == false{
                celldate.requireView.isSelected = false
            }else
            {
                celldate.requireView.isSelected = true
                celldate.requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            }
            if actionfilter[indexPath.section] == false{
                celldate.filterView.isSelected = false
                
                
            }else{
                celldate.filterView.isSelected = true
                celldate.filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
                
            }
            celldate.delegate = self as? TextfieldCellDelegate
            
            celldate.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "Date"
                        if self.dataTypeId.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("Date")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("Date")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            celldate.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            celldate.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return celldate
            
        case 8:
            let cellfile = tableview.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! TextfieldCell
            
            cellfile.deleteBtnview.isHidden = true
            cellfile.TitleText.text = textdata[indexPath.section]
            cellfile.DatatypeLabel.text = "File"
            if actionrequired[indexPath.section] == false{
                cellfile.requireView.isSelected = false
            }else
            {
                cellfile.requireView.isSelected = true
                cellfile.requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            }
            if actionfilter[indexPath.section] == false{
                cellfile.filterView.isSelected = false
                
                
            }else{
                cellfile.filterView.isSelected = true
                cellfile.filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
                
            }
            cellfile.delegate = self as? TextfieldCellDelegate
            
            
            cellfile.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "File"
                        if self.dataTypeId.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("File")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("File")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            cellfile.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            cellfile.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return cellfile
            
        case 9:
            
            switch indexPath.row{
            case 0:
                let cell = tableview.dequeueReusableCell(withIdentifier: "DropdownCell1", for: indexPath) as! DropdownCell1

                cell.dataLabel.text = "Dropdown"
                cell.titleText.text = textdata[indexPath.section]

                cell.deleteBt.isHidden = true
                cell.actionText = { (text) in


                    if self.textdata.count > 0{



                        if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){


                            print("ok")
                            self.textdata[indexPath.section] = text
                            self.datatype[indexPath.section] = "Dropdown"
                            if self.dataTypeId.count < self.actionrequired.count{
                                self.actionrequired.removeLast()
                                self.actionfilter.removeLast()
                            }


                        }else{
                            self.textdata.append(text)
                            self.datatype.append("Dropdown")
                            self.actionrequired.append(false)
                            self.actionfilter.append(false)

                        }


                    }else {
                        self.textdata.append(text)
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                        self.datatype.append("Dropdown")
                    }

                    print(self.textdata)
                    print(self.actionrequired)
                    print(self.actionfilter)

                }
                return cell
            case 1:
                let cell = tableview.dequeueReusableCell(withIdentifier: "DropdownCell2", for: indexPath) as! DropdownCell2
               
                
                cell.choiceText.text = self.Rows[indexPath.section].choices[indexPath.row - 1]

                cell.actionToAdd = {
                    self.Rows[indexPath.section].dropdown_id.append(0)
                    self.Rows[indexPath.section].rows.append(1)
                    self.Rows[indexPath.section].choices.append("")
                   // self.choices.append("")
                    self.tableview.reloadData()


                }
                cell.actionTextField = { (word) in

                    if ( self.Rows[indexPath.section].choices.count != indexPath.row && (self.Rows[indexPath.section].choices.index(where:{$0 == self.Rows[indexPath.section].choices[indexPath.row-1]})) != nil ){
                        
                        

                        print("ok")
                        self.Rows[indexPath.section].choices[indexPath.row-1] = word




                    }else{


                        self.Rows[indexPath.section].choices[0] = word


                    }



                }


                return cell



            default :
                if isRow[indexPath.row] == 1 {
                    let cell = tableview.dequeueReusableCell(withIdentifier: "DropdownCell2", for: indexPath) as! DropdownCell2
                    
                    if self.Rows[indexPath.section].choices[indexPath.row - 1] != ""{
                    cell.choiceText.text = self.Rows[indexPath.section].choices[indexPath.row - 1]
                        cell.AddBtn.isHidden = true
                    }else{
                        cell.choiceText.text = self.Rows[indexPath.section].choices[indexPath.row - 1]
                        cell.AddBtn.isHidden =  false
                    }

                    cell.actionToAdd = {

                        self.Rows[indexPath.section].rows.remove(at: indexPath.row)
                        self.Rows[indexPath.section].choices.remove(at: indexPath.row - 1)
                        self.tableview.reloadData()


                    }
                    cell.actionTextField = { (word) in


                        
                        if ( self.Rows[indexPath.section].choices.count != indexPath.row && (self.Rows[indexPath.section].choices.index(where:{$0 == self.Rows[indexPath.section].choices[indexPath.row-1]})) != nil ){
                            

                            print("ok")
                            self.Rows[indexPath.section].choices[indexPath.row-1] = word




                        }else{
                            self.Rows[indexPath.section].choices[indexPath.row-1] = word

                        }



                       

                    }


                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DropdownCell3", for: indexPath) as! DropdownCell3

                    
                    if actionrequired[indexPath.section] == false{
                        cell.requireView.isSelected = false
                    }else
                    {
                        cell.requireView.isSelected = true
                        cell.requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
                    }
                    if actionfilter[indexPath.section] == false{
                        cell.filterView.isSelected = false
                        
                        
                    }else{
                        cell.filterView.isSelected = true
                        cell.filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
                        
                    }
                    cell.actionRequired = {(Bool) in

                        if self.actionrequired.count > 0{
                            if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){


                                print("ok")
                                self.actionrequired[indexPath.section] = Bool


                            }else{
                                self.actionrequired.append(Bool)
                            }


                        }else{
                            self.actionrequired.append(Bool)

                        }

                    }


                    cell.actionFilter = {(Bool) in

                        if self.actionfilter.count > 0{

                            if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){


                                print("ok")
                                self.actionfilter[indexPath.section] = Bool


                            }else{
                                self.actionfilter.append(Bool)
                            }


                        }else{
                            self.actionfilter.append(Bool)

                        }
                    }




                    return cell
                }


            }

        case 7:
            switch indexPath.row{
            case 0:
                let cell = tableview.dequeueReusableCell(withIdentifier: "DropdownCell1", for: indexPath) as! DropdownCell1
                
                cell.dataLabel.text = "CheckBox"
                cell.titleText.text = textdata[indexPath.section]
                cell.deleteBt.isHidden = true

                cell.actionText = { (text) in
                    
                    
                    if self.textdata.count > 0{
                        
                        
                        
                        if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                            
                            
                            print("ok")
                            self.textdata[indexPath.section] = text
                            self.datatype[indexPath.section] = "Checkbox"
                            if self.dataTypeId.count < self.actionrequired.count{
                                self.actionrequired.removeLast()
                                self.actionfilter.removeLast()
                            }
                            
                            
                        }else{
                            self.textdata.append(text)
                            self.datatype.append("Checkbox")
                            self.actionrequired.append(false)
                            self.actionfilter.append(false)
                            
                        }
                        
                        
                    }else {
                        self.textdata.append(text)
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                        self.datatype.append("Checkbox")
                    }
                    
                    print(self.textdata)
                    print(self.actionrequired)
                    print(self.actionfilter)
                    
                }
                return cell
            case 1:
                let cell = tableview.dequeueReusableCell(withIdentifier: "DropdownCell2", for: indexPath) as! DropdownCell2
                
                cell.choiceText.text = self.Rows[indexPath.section].choices[indexPath.row-1]
                 cell.AddBtn.setImage(UIImage(named: "btn-plus"), for: .normal)
                
                cell.actionToAdd = {
                    
                    self.Rows[indexPath.section].rows.append(1)
                    self.Rows[indexPath.section].dropdown_id.append(0)

                   // self.indexChoices = self.indexChoices! + 1
                    self.Rows[indexPath.section].choices.append("")
                    self.tableview.reloadData()
                    
                    
                    
                }
                cell.actionTextField = { (word) in
                    
                    if ( self.Rows[indexPath.section].choices.count != indexPath.row && (self.Rows[indexPath.section].choices.index(where:{$0 == self.Rows[indexPath.section].choices[indexPath.row-1]})) != nil ){
                        
                        
                        print("ok")
                        self.Rows[indexPath.section].choices[indexPath.row-1] = word
                        
                        
                        
                        
                    }else{
                        
                        
                        self.Rows[indexPath.section].choices[0] = word
                        
                        
                    }
                    
                    
                    
                    print(self.Rows[indexPath.section].choices)
                }
                
                
                return cell
                
                
                
            default :
                if isRowCheckbox[indexPath.row] == 1 {
                    let cell = tableview.dequeueReusableCell(withIdentifier: "DropdownCell2", for: indexPath) as! DropdownCell2
                    
                    if self.Rows[indexPath.section].choices[indexPath.row - 1] != ""{
                        cell.choiceText.text = self.Rows[indexPath.section].choices[indexPath.row - 1 ]
                        cell.AddBtn.isHidden = true
                    }else{
                        cell.choiceText.text = self.Rows[indexPath.section].choices[indexPath.row - 1]
                        cell.AddBtn.isHidden = false
                        
                    }
                    cell.AddBtn.setImage(UIImage(named: "btn-delete"), for: .normal)

                    cell.actionToAdd = {
                        
                        self.Rows[indexPath.section].rows.remove(at: indexPath.row)
                        
                        self.Rows[indexPath.section].choices.remove(at: indexPath.row - 1)
                        
                        self.tableview.reloadData()
                        
                        
                    }
                    cell.actionTextField = { (word) in
                        
                        
                        
                        if ( self.Rows[indexPath.section].choices.count != indexPath.row && (self.Rows[indexPath.section].choices.index(where:{$0 == self.Rows[indexPath.section].choices[indexPath.row-1]})) != nil ){
                            
                            
                            print("ok")
                            self.Rows[indexPath.section].choices[indexPath.row-1] = word
                            
                            
                            
                        }else{
                            self.Rows[indexPath.section].choices[indexPath.row-1] = word
                            
                        }
                        
                        
                        
                        print(self.Rows[indexPath.section].choices)
                    }
                    
                    
                    return cell
                }else{
                    let cell = tableview.dequeueReusableCell(withIdentifier: "DropdownCell3", for: indexPath) as! DropdownCell3
                    
                    if actionrequired[indexPath.section] == false{
                        cell.requireView.isSelected = false
                    }else
                    {
                        cell.requireView.isSelected = true
                        cell.requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
                    }
                    if actionfilter[indexPath.section] == false{
                        cell.filterView.isSelected = false
                        
                        
                    }else{
                        cell.filterView.isSelected = true
                        cell.filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
                        
                    }
                    cell.actionRequired = {(Bool) in
                        
                        if self.actionrequired.count > 0{
                            if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                                
                                
                                print("ok")
                                self.actionrequired[indexPath.section] = Bool
                                
                                
                            }else{
                                self.actionrequired.append(Bool)
                            }
                            
                            
                        }else{
                            self.actionrequired.append(Bool)
                            
                        }
                        
                    }
                    
                    
                    cell.actionFilter = {(Bool) in
                        
                        if self.actionfilter.count > 0{
                            
                            if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                                
                                
                                print("ok")
                                self.actionfilter[indexPath.section] = Bool
                                
                                
                            }else{
                                self.actionfilter.append(Bool)
                            }
                            
                            
                        }else{
                            self.actionfilter.append(Bool)
                            
                        }
                    }
                    
                    
                    
                    
                    return cell
                }
                
                
            }
            
            return UITableViewCell()
            
            
            
            
            
        case 10:
            let cellimage = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! TextfieldCell
            
            cellimage.deleteBtnview.isHidden = true
            cellimage.TitleText.text = textdata[indexPath.section]
            cellimage.DatatypeLabel.text = "Image"
           // cellimage.RequiredSwitch.isOn = actionrequired[indexPath.section]
          //  cellimage.FilterSwitch.isOn = actionfilter[indexPath.section]
            cellimage.delegate = self as? TextfieldCellDelegate
           // cellimage.FilterSwitch.isHidden = true
           cellimage.filterView.isHidden = true
            if actionrequired[indexPath.section] == false{
                cellimage.requireView.isSelected = false
            }else
            {
                cellimage.requireView.isSelected = true
                cellimage.requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            }
            if actionfilter[indexPath.section] == false{
                cellimage.filterView.isSelected = false
                
                
            }else{
                cellimage.filterView.isSelected = true
                 cellimage.filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            }
            cellimage.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "Image"
                        if self.dataTypeId.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("Image")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("Image")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            cellimage.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            cellimage.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return cellimage
            
        default :
                return UITableViewCell()
            }
        }
        
    }
    
    


extension EditFormView:getedittextdelegate{
    func sendToTitle(_ string: String) {
        self.formTitle = string
    }
    
    func sendTodes(_ string: String) {
        self.desText = string
    }
    
    
    
}
