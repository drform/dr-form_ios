//
//  SignUpView.swift
//  Form
//
//  Created by MSU IT CS on 31/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class SignUpView: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {


    @IBOutlet weak var usernameText: MDCTextField!
    @IBOutlet weak var passwordText: MDCTextField!
    @IBOutlet weak var repasswordText: MDCTextField!
    @IBOutlet weak var emailText: MDCTextField!
    @IBOutlet weak var nameText: MDCTextField!
    @IBOutlet weak var TelText: MDCTextField!
    @IBOutlet weak var positionText: MDCTextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var signBtnView: UIButton!
    
    var usernameTextController : MDCTextInputControllerOutlined?
    var passwordTextController : MDCTextInputControllerOutlined?
    var repasswordTextController : MDCTextInputControllerOutlined?
    var emailTextController : MDCTextInputControllerOutlined?
    var TelTextController : MDCTextInputControllerOutlined?
    var positionTextController : MDCTextInputControllerOutlined?
    var nameTextController : MDCTextInputControllerOutlined?

    var base64String:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        
        usernameTextController = MDCTextInputControllerOutlined(textInput: usernameText)
       passwordTextController = MDCTextInputControllerOutlined(textInput: passwordText)
        repasswordTextController = MDCTextInputControllerOutlined(textInput: repasswordText)
        emailTextController = MDCTextInputControllerOutlined(textInput: emailText)
        TelTextController = MDCTextInputControllerOutlined(textInput: TelText)
        positionTextController = MDCTextInputControllerOutlined(textInput: positionText)
        nameTextController = MDCTextInputControllerOutlined(textInput: nameText)




        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true
        
        signBtnView.layer.cornerRadius = 24
        signBtnView.clipsToBounds = true
        
        base64String = convertImageTobase64(format: .jpeg(1.0), image: UIImage(named: "Group35")!)!

        
    }
    
    @IBAction func importimageBtn(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source",message: "Choose a source ", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            
            
            image.sourceType = .camera
           // image.mediaTypes = kUTTypeImage as String
            image.allowsEditing = false
            
            self.present(image ,animated: true,completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Libraby", style: .default, handler: { (action:UIAlertAction) in
             UINavigationBar.appearance().tintColor = UIColor.black
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            image.allowsEditing = false
            self.present(image ,animated: true)
            
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil ))
        
        self.present(actionSheet , animated: true ,completion: nil)
        
    }
    
    
    @IBAction func createBtn(_ sender: Any) {
        
        let username = self.usernameText.text
        let password = self.passwordText.text
        let email = self.emailText.text
        let name = self.nameText.text
        let tel = self.TelText.text
        let position = self.positionText.text
        var _: ModelUserInfo?
        
        
        let link = URL(string:"\(url!)/users")

        if usernameText.text?.isEmpty ?? true || emailText.text?.isEmpty ?? true || nameText.text?.isEmpty ?? true || TelText.text?.isEmpty ?? true || positionText.text?.isEmpty ?? true || passwordText.text?.isEmpty ?? true
        {
            print("error")
//
//
        }else{
            Signup.create(link!, username: username!, password: password!, name: name!, email: email!, tel: tel!, position: position!,image: base64String!) {(result,error) in
                let x = result?.message
                     print(result?.message)
                
                    if x == "Sign up Complete." {
                        
                            alert_successView.instance.showAlert(title: "Sign up Complete", message: "", alertType: .success)
                        }else if x == "error"{
                            alert_successView.instance.showAlert(title: "Some thing wrong", message: "", alertType: .failure)
                
                            }
                
                
                
            }
        }
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        
        self.performSegue(withIdentifier: "loginview", sender: self)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            
            
            base64String = convertImageTobase64(format: .jpeg(1.0), image: image)!
            
            
            // print("++++++++++\(base64String)")
            imageView.layer.cornerRadius = imageView.frame.size.width/2
            imageView.clipsToBounds = true
            imageView.image = image
            
            
        }
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    public enum ImageFormat {
        case png
        case jpeg(CGFloat)
    }
    func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = image.pngData()
        case .jpeg(let compression): imageData = image.jpegData(compressionQuality: compression)
        }
        return imageData?.base64EncodedString()
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
