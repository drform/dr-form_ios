//
//  FormView.swift
//  Form
//
//  Created by MSU IT CS on 4/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import ParallaxHeader
import SafariServices
import NVActivityIndicatorView


struct imageanddate {
    var date:String?
    var image = [UIImage]()
}
struct fileanddate {
    var date:String?
    var linkfile = [String]()
    var name = [String]()
}
class FormView: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
    var isloadanswer:Bool = true
    var groups_id:Int? = 1
    var user_id:Int?
    var owner:String?
     var id:Int? = 113
    var username:String?
    var idanswer:Int?
    var data_type:String?
    var nametitle:String? = "title"
    var des:String?
    var image:UIImage?
    var somedateString = [String]()
    var namesegue:String?
    var datesegue:String?
    var imagesegue:UIImage?
    var token:String? = "feed5d13631b180fdc31d872b9fb077f8dc39a7e"
    var isSearching = false
     let refreshControl = UIRefreshControl()
    var tmpfilter = [String]()
    var filter = [String]()
    var array = [String]()
    var search = [Any]()
    var user_form = [FormDetailModel]()
    var user_formnew = [FormDetailModel]()
    var user_formimage = [UIImage]()
    var getallimage = [Getallimageandfile]()
    var getallfile = [Getallfile]()
    var searchDelgate:AdvanceSearchView?
    var users = [GetShareUsers]()
    var questionitems = [FormsQuestionModel]()
    var typeview:Int? = 1
    var amount:Int = 0
    var offset:Int = 0
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        setHeaderView()
    }

    
    var allimage = [imageanddate]()
    var allfile = [fileanddate]()
    
    func setHeaderView() -> Void {
        let headerview = FormheaderView.instanceFromNib()
        headerview.setUpView()
        headerview.setNeedsLayout()
        headerview.layoutIfNeeded()
        
        tableView.parallaxHeader.view = headerview
        tableView.parallaxHeader.height = 250
        tableView.parallaxHeader.minimumHeight = 0
        tableView.parallaxHeader.mode = .bottomFill
        
        headerview.imageview.image = image
        headerview.titlelabel.text = nametitle
        headerview.deslabel.text  = des
        headerview.tmp = {
            self.typeview = 1
            self.offset = 0
            self.isloadanswer = true
            let link5 = URL(string:"\(url!)/forms/\(self.id!)/user_forms")
            
            GetSelectForm.get(link5!,keyword: nil,offset:self.offset,group_id:self.groups_id!, token: self.token!){
                (result,error) in
                self.user_form = (result?.user_form)!
                self.amount = (result?.amount)!
                
                if self.user_form != nil{
                    for tmp in self.user_form{
                        let raw = "\(url!)\(tmp.image!)"
                        let  Nsurl = NSURL(string: raw )!
                        
                        if let data = try? Data(contentsOf: Nsurl as URL)
                        {
                            //  images.append(UIImage(data: data)!)
                            self.user_formimage.append(UIImage(data:data)!)
                            //  cell.imageview.image = UIImage(data:data)
                            
                        }
                    }
                }
                
                self.tableView.reloadData()
            }
            
        }
        headerview.tmp2 = {
           self.typeview = 2
           self.allimage.removeAll()
                    let link2 = URL(string:"\(url!)/forms/\(self.id!)/user_forms")
                    
                    Getallimageandfile.get(link2!, type: "image", group_id: self.groups_id!, token: self.token!){
                        (result,error) in
                        
                        if result != nil {
                            self.getallimage = result!
                            var i:Int? = 0
                            for tmp in self.getallimage{
                                
                                let myDateString = tmp.date
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd"
                                let date = dateFormatter.date(from: myDateString!)
                                dateFormatter.dateFormat = "dd MMM yyyy"
                                
                                self.allimage.append(imageanddate(date: dateFormatter.string(from: date!), image: [UIImage()]))
                                
                                for sub in tmp.data{
                                    let raw = "\(url!)\(sub.image!)"
                                    
                                    let  Nsurl = NSURL(string: raw )!
                                    
                                    if let data = try? Data(contentsOf: Nsurl as URL)
                                    {
                                        //  images.append(UIImage(data: data)!)
                                        
                                        let image = UIImage(data:data)
                                        self.allimage[i!].image.append(image!)
                                        
                                        
                                    }
                                }
                                self.allimage[i!].image.removeFirst()
                                i = i! + 1
                            }
                            
                            self.tableView.reloadData()
                        }
                        
                        
                    }
            
                    self.tableView.reloadData()
                
                
                
                
            


        }
        headerview.tmp3 = {
            self.typeview = 3
            self.allfile.removeAll()
            let link3 = URL(string:"\(url!)/forms/\(self.id!)/user_forms")

            Getallfile.get(link3!, type: "file", group_id: self.groups_id!, token: self.token!){  (result,error) in
                
                if result != nil {
                    self.getallfile = result!
                    var i:Int? = 0
                    for tmp in self.getallfile{
                        
                        let myDateString = tmp.date
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        let date = dateFormatter.date(from: myDateString!)
                        dateFormatter.dateFormat = "dd MMM yyyy"
                        
                        self.allfile.append(fileanddate(date: dateFormatter.string(from: date!), linkfile: [String()], name: [String()]))
                        
                        for sub in tmp.data{
                            self.allfile[i!].name.append(sub.filename!)
                            self.allfile[i!].linkfile.append(sub.file!)
                        }
                        self.allfile[i!].name.removeFirst()
                        self.allfile[i!].linkfile.removeFirst()
                        
                        i = i! + 1
                    }
                    
                    self.tableView.reloadData()
                }
                
            }
                self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchDelgate?.searchDelegate = self
       
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        
        
        let link = URL(string:"\(url!)/forms/\(id!)/user_forms")
        DispatchQueue.global(qos: .background).async {
            self.startAnimating()
        GetSelectForm.get(link!,keyword: nil,offset:self.offset,group_id:self.groups_id!, token: self.token!){
            (result,error) in
            if result?.user_form != nil{
                self.user_form = (result?.user_form)!

            }
            self.amount = (result?.amount)!
            if self.user_form != nil{
                for tmp in self.user_form{
                    let raw = "\(url!)\(tmp.image!)"
                    let  Nsurl = NSURL(string: raw )!
                    
                    if let data = try? Data(contentsOf: Nsurl as URL)
                    {
                        //  images.append(UIImage(data: data)!)
                        self.user_formimage.append(UIImage(data:data)!)
                        //  cell.imageview.image = UIImage(data:data)
                        
                    }
                }
            }
            DispatchQueue.main.async {
                // reload your collection view here:
                
                self.tableView.reloadData()
                self.stopAnimating()
                
            }
        }
         
        // Do any additional setup after loading the view.
        let link1 = URL(string:"\(url!)/forms/\(self.id!)/shares")
        
        GetShareUsers.get(link1!, token: self.token!){
            (result,error) in
            if result != nil{
                self.users = result!

            }
            print("get")
     
         
         }
         
        }
        
        let link4 = URL(string:"\(url!)/form_items/\(id!)")
        GetQuestion.get(link4!, token: token!){
            (result,error) in
            
            self.questionitems = (result?.questionitems)!
            
            
        }
        
        setHeaderView()
        
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        
       self.tableView.refreshControl = refreshControl
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        
      
        let editBt = UIButton()
        var item = UIBarButtonItem()
        if owner == username{
        editBt.setImage(UIImage(named: "edit"), for: .normal)
        
        editBt.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        editBt.addTarget(self, action: #selector(EditBtn), for: .touchUpInside)
       
        editBt.clipsToBounds = true
         item = UIBarButtonItem(customView: editBt)
        }
        let uploadBt = UIButton()
        uploadBt.setImage(UIImage(named: "upload.png"), for: .normal)
       
        uploadBt.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        uploadBt.addTarget(self, action: #selector(UploadBtn), for: .touchUpInside)
        
        uploadBt.clipsToBounds = true
        let item2 = UIBarButtonItem(customView: uploadBt)
        
        self.navigationItem.rightBarButtonItems = [item,item2]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
         UINavigationBar.appearance().tintColor = UIColor.white
        
      
        
        doneButton.layer.cornerRadius = 20
        doneButton.clipsToBounds = true
        doneButton.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
     
        
        self.hideKeyboardWhenTappedAround()
        
        tableView.register(UINib(nibName: "allimagevcollectionView", bundle: Bundle.main), forCellReuseIdentifier: "allimagevcollectionView")

    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        self.navigationController?.navigationBar.tintColor = .white

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
      
        
     
    }
    override func viewDidAppear(_ animated: Bool) {
      
    }
    func showSafariVC(for path:String){
        guard let path = URL(string: path) else {
            return
        }
        let safariVC = SFSafariViewController(url: path)
        present(safariVC, animated: true)
    }
    
    
    @objc func doSomething(refreshControl: UIRefreshControl) {
        print("Hello World!")
        offset = 0
        user_formnew.removeAll()
        
        let link = URL(string:"\(url!)/forms/\(id!)/user_forms")

        GetSelectForm.get(link!,keyword: nil,offset:self.offset,group_id:groups_id!, token: token!){
            (result,error) in
            self.user_form = (result?.user_form)!
            self.amount = (result?.amount)!
            if self.user_form != nil{
                for tmp in self.user_form{
                    let raw = "\(url!)\(tmp.image!)"
                    let  Nsurl = NSURL(string: raw )!
                    
                    if let data = try? Data(contentsOf: Nsurl as URL)
                    {
                        //  images.append(UIImage(data: data)!)
                        self.user_formimage.append(UIImage(data:data)!)
                        //  cell.imageview.image = UIImage(data:data)
                        
                    }
                }
            }
            self.tableView.reloadData()
        }
        
        
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline){
             refreshControl.endRefreshing()
        }
        // somewhere in your code you might need to call:
       
    }
    
    
    @IBAction func createAnswerBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "newanswersegue", sender: self)

    }
    
   
    @objc func UploadBtn(){
        
    }
    
    @objc func EditBtn(){
        self.performSegue(withIdentifier: "EditFormView", sender: self)
    }
    
    @IBAction func allimage(_ sender: Any) {
       
        
    }
    
    @IBAction func allFile(_ sender: Any) {
       

    }
  
    
    
    //TABLEVIEW
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if typeview == 1 && indexPath.row == user_form.count - 1{
            
            if isloadanswer == true{
                offset = offset + 5
                
                let link = URL(string:"\(url!)/forms/\(id!)/user_forms")
                
                GetSelectForm.get(link!,keyword: nil,offset:self.offset, group_id:groups_id!, token: token!){
                    (result,error) in
                    
                    if result != nil {
                         self.user_formnew = (result?.user_form)!
                    }else{
                        self.isloadanswer = false
                    }
                   
                    
                    if self.user_formnew.count == 0 {
                        
                        self.isloadanswer = false
                        print(self.offset)
                        
                    }else if self.isloadanswer != false{
                        
                        var i:Int = 0
                        while i < (self.user_formnew.count){
                            
                            self.user_form.append(self.user_formnew[i])
                            
                            if self.user_form != nil{
                                
                                let raw = "\(url!)\(self.user_formnew[i].image!)"
                                    let  Nsurl = NSURL(string: raw )!
                                    
                                    if let data = try? Data(contentsOf: Nsurl as URL)
                                    {
                                        //  images.append(UIImage(data: data)!)
                                        self.user_formimage.append(UIImage(data:data)!)
                                        //  cell.imageview.image = UIImage(data:data)
                                        
                                    }
                                
                            }
                            i = i + 1
                            
                        }
                        
                    }
                    
                    
                    
                    self.tableView.reloadData()
                }
            }
                
            
            
                
            
                
            
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if typeview == 1{
            
       tableView.deselectRow(at: indexPath, animated: true)
        
        idanswer = user_form[indexPath.row].id
        namesegue = user_form[indexPath.row].name
        datesegue = somedateString[indexPath.row]
        let raw = "\(url!)\(user_form[indexPath.row].image!)"
        let  Nsurl = NSURL(string: raw )!
        
        if let data = try? Data(contentsOf: Nsurl as URL)
        {
            //  images.append(UIImage(data: data)!)
            imagesegue = UIImage(data:data)
        }
        performSegue(withIdentifier: "answersegue", sender: nil)
        
        }else if typeview == 3{
            tableView.deselectRow(at: indexPath, animated: true)
            
         

            
            
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if typeview != 1{
            return 300
        }else
        {
            return 150
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SearchBarView.instanceFromNib()
        view.setUpView()
        if typeview != 1{
            view.avBtn.isHidden = true
            view.ResultAnswer.isHidden = true
        }
        view.id = self.id
        view.group_id = self.groups_id
        view.applyBtn.isHidden = true
        view.searchbar.frame.size.width = 312
        view.searchbar.delegate = self
        view.searchbar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        view.avBtn.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
        view.avBtn.layer.cornerRadius = 15
        view.avBtn.clipsToBounds = true
        
        view.av = {
            
            self.performSegue(withIdentifier: "advancesegue", sender: self)
            
        }
        
        view.ResultAnswer.textColor = UIColor(red:0.65, green:0.65, blue:0.65, alpha:1.0)
        
        view.ResultAnswer.text = "\(amount) Answer"
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
        if typeview == 1 {
        return user_form.count
        }else
        {
            return 1
        }
        
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if typeview == 1{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FormCell
        
        cell.nameText.text = user_form[indexPath.row].name
        
        
        _ = UIBezierPath(rect: cell.bounds)
        //cell.layer.masksToBounds = true
        
        // 2019-02-18T16:28:24.200+07:00
      
        let myDateString = user_form[indexPath.row].update
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
        let date = dateFormatter.date(from: myDateString!)
        dateFormatter.dateFormat = "dd MMM yyyy HH:mm"
         somedateString.append(dateFormatter.string(from: date!))
     
        cell.dateText.text = somedateString[indexPath.row]
       
       cell.imageview.image = user_formimage[indexPath.row]
            
        return cell
        }
        else if typeview == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "allimagevcollectionView", for: indexPath) as! allimagevcollectionView
            
            cell.section = allimage
            
            cell.typeview = 2
            cell.collectionview.reloadData()
            return cell
            
            
        }else if typeview == 3{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "allimagevcollectionView", for: indexPath) as! allimagevcollectionView
            
            cell.sectionfile = allfile
            cell.typeview = 3
            cell.collectionview.reloadData()
            
            cell.actionPath = {(path) in
                
                self.showSafariVC(for: "\(url!)\(path)")
                
                
            }
            return cell
            
            
        }
        return UITableViewCell()
    }
   //END TABLEVIEW
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "answersegue" {
            let next = segue.destination as! FormDetailView
            next.idanswer = idanswer
            next.id = id
            next.token = token
            next.name = namesegue
            next.date = datesegue
            next.image = imagesegue
           
            
        }else if segue.identifier == "newanswersegue"{
            let next = segue.destination as! NewAnswerView
            
            self.tableView.reloadData()
            next.form_id = id!
            next.token = token!
            next.image = image
            next.nametitle = nametitle
            next.des = des
        }else if segue.identifier == "EditFormView"{
            let next = segue.destination as! EditFormView
            
            next.form_id = id
            next.token = token
            next.titleSegue = nametitle
            next.desSegue = des
            next.imageSegue = image
            next.groups_id = self.groups_id
            next.share_count = self.users.count
            
        }else if segue.identifier == "advancesegue"{
            
            let next = segue.destination as! AdvanceSearchView
            
            next.form_id = self.id
            next.token = self.token
            next.searchDelegate = self
            
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

}
extension FormView:UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
       
        let link = URL(string:"\(url!)/forms/\(id!)/user_forms")
        
        GetSelectForm.get(link!,keyword: searchBar.text!, offset: self.offset,group_id:groups_id!, token: token!){
            (result,error) in
            self.user_form = (result?.user_form)!
            self.amount = (result?.amount)!
            
            self.tableView.reloadData()
        }
        
        
        
    }
    
    

}
extension FormView:sendAvSearch{

    func send(_ users: [Any]) {
        
        self.user_form = users as! [FormDetailModel]
        self.tableView.reloadData()
    }
    
    
    
    
}

