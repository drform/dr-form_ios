//
//  SearchBarView.swift
//  Form
//
//  Created by MSU IT CS on 26/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class SearchBarView: UIView {

    var id:Int?
    var group_id:Int?
    var tmp: (() -> ())?
    var av: (() -> ())?
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var avBtn: UIButton!
    @IBOutlet weak var ResultAnswer: UILabel!
    @IBOutlet weak var shareLabel: UILabel!
    
    class func instanceFromNib() -> SearchBarView {
        return UINib(nibName: "SearchBarView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SearchBarView
    }
    
    
    func setUpView() -> Void{
       
     //  searchbar.setImage(UIImage(), for: .search, state: .normal)
//        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
//        view.frame = self.bounds
        let searchTextField:UITextField = searchbar.subviews[0].subviews.last as! UITextField
        searchTextField.layer.cornerRadius = 24
        searchTextField.placeholder = "Search by username or date"
        
   
    }
    
    @IBAction func shareBtn(_ sender: Any) {
        tmp?()
    }
    
    @IBAction func avBt(_ sender: Any) {
        av?()
        
    }
    
}

