//
//  ReusableView.swift
//  Form
//
//  Created by MSU IT CS on 25/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class ReusableView: UICollectionReusableView {
         @IBOutlet weak var labelheader:UILabel!
        @IBOutlet weak var view: UIView!
    
}
