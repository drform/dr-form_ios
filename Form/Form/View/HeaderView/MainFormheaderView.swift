//
//  MainFormheaderView.swift
//  Form
//
//  Created by MSU IT CS on 22/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class MainFormheaderView: UIView {

    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var footerfirstBtn: UIView!
    @IBOutlet weak var footersecindBtn: UIView!
    
    var tmp : (() -> ())?
    var tmp2 : (() -> ())?
    
    class func instanceFromNib() -> MainFormheaderView {
        return UINib(nibName: "MainFormheaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MainFormheaderView
        
    }
    func setUpView() -> Void {
        
        mainview.frame = self.bounds
        mainview.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        imageview.layer.cornerRadius = 10
        imageview.clipsToBounds = true
        
        footerfirstBtn.backgroundColor = .black
        
    }
    func deselectAllButtons(){
        for subView in mainview.subviews
        {
            // Set all the other buttons as normal state
            if let button = subView as? UIButton {
                button.isSelected = false
                button.tintColor = .white
                button.setTitleColor(.black, for: .normal)

                
            }
            
        }
        footerfirstBtn.backgroundColor = .white
        footersecindBtn.backgroundColor = .white
        
    }
    
    @IBAction func myformview(_ sender: UIButton) {
        deselectAllButtons()
        sender.isSelected = true
        sender.setTitleColor(.black, for: .normal)
        sender.tintColor = .white
        sender.setTitleColor(.black, for: .normal)
        footerfirstBtn.backgroundColor = UIColor(red:0.40, green:0.40, blue:0.40, alpha:1.0)
        tmp?()
    }
    
    @IBAction func sharewithmeview(_ sender: UIButton) {
        deselectAllButtons()
        sender.isSelected = true
        sender.setTitleColor(.black, for: .normal)
        sender.tintColor = .black
        sender.setTitleColor(.black, for: .normal)

        footersecindBtn.backgroundColor = UIColor(red:0.40, green:0.40, blue:0.40, alpha:1.0)
        tmp2?()
    }
}
