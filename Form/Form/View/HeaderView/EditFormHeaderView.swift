//
//  EditFormHeaderView.swift
//  Form
//
//  Created by MSU IT CS on 3/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

protocol getedittextdelegate {
    
    func sendToTitle(_ string: String)
    func sendTodes(_ string: String)
    
}

class EditFormHeaderView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleText: MDCTextField!
    @IBOutlet weak var DescriptionTextview: MDCTextField!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var importBtn: UIButton!
    @IBOutlet weak var view: UIView!
    
    var delegate: getedittextdelegate?
    var titleTextController:MDCTextInputControllerOutlined?
    var descriptionTextController:MDCTextInputControllerOutlined?
    class func instanceFromNib() -> EditFormHeaderView{
         return UINib(nibName: "EditFormHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EditFormHeaderView
    }
    
    func setUpView() -> Void{
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        deleteBtn.layer.cornerRadius = 24
        deleteBtn.clipsToBounds = true
        
        
        shareBtn.layer.cornerRadius = 24
        shareBtn.clipsToBounds = true
        
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        
        titleText.tag = 1
        DescriptionTextview.tag = 2
        titleTextController = MDCTextInputControllerOutlined(textInput: titleText)
        descriptionTextController = MDCTextInputControllerOutlined(textInput: DescriptionTextview)
        
        titleTextController?.floatingPlaceholderScale = 0.99
        descriptionTextController?.floatingPlaceholderScale = 0.99

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        titleText.delegate = self
        DescriptionTextview.delegate = self
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension EditFormHeaderView:UITextFieldDelegate{
    func  textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 1{
        if let text = textField.text{
           delegate?.sendToTitle(text)
            }
            
        }else{
            if let text = textField.text{
            delegate?.sendTodes(text)
            }
        }
        
    }
}

