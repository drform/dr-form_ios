//
//  NewFormHeaderView.swift
//  Form
//
//  Created by MSU IT CS on 26/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

protocol gettextdelegate {
    func sendToTitle(_ string: String)
    func sendTodes(_ string: String)
}
class NewFormHeaderView: UIView {
    
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var DescriptionTextview: MDCTextField!
    @IBOutlet weak var titleText: MDCTextField!
    
    var actionText: ((String) -> ())?
    var actionTextview: ((String) -> ())?
   var delegate: gettextdelegate?
    var titleTextController:MDCTextInputControllerOutlined?
    var DescriptionTextviewController:MDCTextInputControllerOutlined?
    class func instanceFromNib() -> NewFormHeaderView {
        return UINib(nibName: "NewFormHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NewFormHeaderView
    }
    
    func setUpView() -> Void {
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
//        titleText.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
//        DescriptionTextview.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
//        
        titleText.textColor = .white
        DescriptionTextview.textColor = .white
        titleText.tag = 1
        DescriptionTextview.tag = 2
        titleTextController = MDCTextInputControllerOutlined(textInput: titleText);
        DescriptionTextviewController = MDCTextInputControllerOutlined(textInput:  DescriptionTextview)
        
        titleTextController?.floatingPlaceholderNormalColor = UIColor(red:0.65, green:0.65, blue:0.65, alpha:1.0)
        DescriptionTextviewController?.floatingPlaceholderNormalColor = UIColor(red:0.65, green:0.65, blue:0.65, alpha:1.0)
        titleTextController?.inlinePlaceholderColor = .white
        DescriptionTextviewController?.inlinePlaceholderColor = .white
        titleTextController?.floatingPlaceholderScale = 0.99
        DescriptionTextviewController?.floatingPlaceholderScale = 0.99
        imageview.layer.cornerRadius = 10
        imageview.clipsToBounds = true
       
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        view.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        titleText.delegate = self
        DescriptionTextview.delegate = self
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
extension NewFormHeaderView:UITextFieldDelegate{
    func  textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 1{
            if let text = textField.text{
            delegate?.sendToTitle(text)
            }
            
        }else{
            let text = textField.text
            delegate?.sendTodes(text ?? "")

        }
    }
}

extension NewFormHeaderView:UITextViewDelegate{
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if let text = textView.text{
            delegate?.sendTodes(text)
        }
    }
    
}
