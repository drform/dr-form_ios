//
//  FormheaderView.swift
//  Form
//
//  Created by MSU IT CS on 26/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class FormheaderView: UIView {

    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var deslabel: UILabel!
    @IBOutlet weak var formview: UIButton!
    @IBOutlet weak var footerform: UIView!
    @IBOutlet weak var footerimage: UIView!
    @IBOutlet weak var footerfile: UIView!
    @IBOutlet weak var formBtn: UIButton!
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var fileBtn: UIButton!
    
    
    var tmp : (() -> ())?
    var tmp2 : (() -> ())?
    var tmp3 : (() -> ())?
    class func instanceFromNib() -> FormheaderView {
        return UINib(nibName: "FormheaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FormheaderView
    }

    @IBAction func fileview(_ sender: UIButton) {
        deselectAllButtons()
        sender.isSelected = true
        let origImage = UIImage(named: "folder");
        let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        sender.setImage(tintedImage, for: .normal)
        sender.tintColor = UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0)
        sender.setTitleColor(UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0), for: .normal)
        footerfile.backgroundColor = UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0)
        tmp3?()
        print("file")
    }
    @IBAction func imageview(_ sender: UIButton) {
        deselectAllButtons()
        sender.isSelected = true
        sender.setTitleColor(UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0), for: .normal)
        let origImage = UIImage(named: "image");
        let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        sender.setImage(tintedImage, for: .normal)
         sender.tintColor = UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0)
        sender.setTitleColor(UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0), for: .normal)
        footerimage.backgroundColor = UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0)
        tmp2?()
        print("image")

    }
    @IBAction func formview(_ sender: UIButton) {
        deselectAllButtons()
        sender.isSelected = true
        sender.setTitleColor(UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0), for: .normal)
        let origImage = UIImage(named: "file-1");
        let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        sender.setImage(tintedImage, for: .normal)
        sender.tintColor = UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0)
        sender.setTitleColor(UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0), for: .normal)
        footerform.backgroundColor = UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0)
        tmp?()
        print("form")

    }
    
    func deselectAllButtons(){
        for subView in view.subviews
        {
            // Set all the other buttons as normal state
            if let button = subView as? UIButton {
                button.isSelected = false
                button.setTitleColor(.white, for: .normal)
                button.tintColor = .white
            }
          
        }
        footerform.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        footerimage.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        footerfile.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
    }
    func setUpView() -> Void {
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        view.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        imageview.layer.cornerRadius = 10
        imageview.clipsToBounds = true
        
        let origImage = UIImage(named: "file-1");
        let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        formview.setImage(tintedImage, for: .normal)
        formview.tintColor = UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0)
        formview.setTitleColor(UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0), for: .normal)
        footerform.backgroundColor = UIColor(red:1.00, green:0.55, blue:0.00, alpha:1.0)
        
        footerimage.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        footerfile.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
    }

}



