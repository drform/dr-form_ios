//
//  ShareHeaderView.swift
//  Form
//
//  Created by MSU IT CS on 12/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class ShareHeaderView: UIView {

    @IBOutlet weak var view: UIView!
    
    class func instanceFromNib() -> ShareHeaderView{
          return UINib(nibName: "ShareHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ShareHeaderView
    }
    func setUpView() -> Void {
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
