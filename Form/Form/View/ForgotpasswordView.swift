//
//  ForgotpasswordView.swift
//  Form
//
//  Created by MSU IT CS on 2/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class ForgotpasswordView: UIViewController {

   
    @IBOutlet weak var numbetText: MDCTextField!
    @IBOutlet weak var sendBtn: UIButton!
    
   var textController: MDCTextInputControllerLegacyDefault?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       sendBtn.layer.cornerRadius = 24
        sendBtn.clipsToBounds = true
        
        
        numbetText.keyboardType = .emailAddress
        textController = MDCTextInputControllerLegacyDefault(textInput: numbetText)
    }
    
    @IBAction func sendBt(_ sender: Any) {
        
//        alert_successView.instance.showAlert(title: "Failure", message: "Error", alertType: .failure)
//        alert_successView.instance.tmp = {
//             self.dismiss(animated: true, completion: nil)
//        }
        
        let link = URL(string: "\(url!)/password/forgot")
        
        Forgetpassword.create(link!, email: numbetText.text!, pin: nil){(result,error) in
            
            if result?.message == "Email reset password sent."{
                
                 alert_successView.instance.showAlert(title: "Reset password sent.", message: "", alertType: .success)
                
                alert_successView.instance.tmp = {
                    
                    self.performSegue(withIdentifier: "verifysegue", sender: self)
                }
                
                
            }else{
                alert_successView.instance.showAlert(title: "Email not found", message: "Email address not found. Please check and try again.", alertType: .failure)
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "verifysegue"{
            let next = segue.destination as! VerifyView
            
            next.email = numbetText.text!
        }
    }
    
}
