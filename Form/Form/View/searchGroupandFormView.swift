//
//  searchGroupandFormView.swift
//  Form
//
//  Created by MSU IT CS on 17/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class searchGroupandFormView: UIViewController {

    var datagroup:GetGroups?
    var datamyform = [GetFormDetail]()
    var datashareform = [GetFormShare]()
    
    var token:String? = "bd16da17776c721dd043ff8c47c1172141698c79"
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    var image:String?
    var image_group:UIImage?
    var role:String?
    var user_id:Int?
    var rolestatus:Bool?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.dataSource = self
        collectionView.delegate = self
        
        let createBtn = UIButton(type: .custom)
        createBtn.setImage(UIImage(named: "add-group"), for: .normal)
        createBtn.addTarget(self, action: #selector(createpage), for: .touchUpInside)
        createBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        createBtn.clipsToBounds = true
        
        let barButton = UIBarButtonItem(customView: createBtn)
        
        let searchBtn = UIButton(type: .custom)
        searchBtn.setImage(UIImage(named: "search"), for: .normal)
        searchBtn.addTarget(self, action: #selector(searchGroups), for: .touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        searchBtn.clipsToBounds = true
        let barButton2 = UIBarButtonItem(customView: searchBtn)
        
        
        if rolestatus == true{
            
            self.navigationItem.rightBarButtonItems = [barButton,barButton2]
        }else{
            self.navigationItem.rightBarButtonItem = barButton2
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func createpage() {
        self.performSegue(withIdentifier: "createsegue", sender: self)
    }
    @objc func searchGroups(){
        print("search")
        searchView.instance.show()
        searchView.instance.from = "group"
        searchView.instance.token = self.token
        
        searchView.instance.dataGroups = {(dataGroup) in
            
            self.datagroup = dataGroup as! GetGroups
            self.collectionView.reloadData()
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "mainformview"{
            let next = segue.destination as! MainFormView
            let cell = sender as? searchGroupandFormCollectionViewCell
            let index = collectionView.indexPath(for: cell!)
            
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email
            next.position = self.position
            next.tel = self.tel
            next.image = self.image
            next.image_group = self.image_group
            next.description_group = datagroup?.groupsmodel[(index?.item)!].description
            next.groups_id = datagroup?.groupsmodel[(index?.item)!].id
            next.titlegroup = datagroup?.groupsmodel[(index?.item)!].title
            
            next.user_id = self.user_id
            
            
        }else if segue.identifier == "myformsegue"{
            let next = segue.destination as! FormView
            let cell = sender as? searchGroupandFormCollectionViewCell
            let index = collectionView.indexPath(for: cell!)
            
            next.username = self.username
            next.des = datamyform[(index?.item)!].des
            next.owner = datamyform[(index?.item)!].owner
            next.token = self.token
            next.nametitle = datamyform[(index?.item)!].title
            next.id = datamyform[(index?.item)!].id
             next.image = self.image_group
            
            
        }
    }

}
extension searchGroupandFormView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        if datagroup != nil{
        performSegue(withIdentifier: "mainformview", sender: cell)
            
        }else if datamyform != nil{
            
            performSegue(withIdentifier: "myformsegue", sender: cell)

        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 2
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if datagroup != nil{
        return (datagroup?.groupsmodel.count)!
        }
        else{
            return (datamyform.count)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! searchGroupandFormCollectionViewCell
        
        if datagroup != nil{
            if rolestatus == true{
                cell.editBtn.isHidden = false
            }
        cell.titleLabel.text = datagroup?.groupsmodel[indexPath.row].title
        
        let raw = "\(url!)\(datagroup!.groupsmodel[indexPath.row].image!)"
        let Nsurl = NSURL(string: raw)!
        
        if let data = try? Data(contentsOf: Nsurl as URL)
            {
            
                cell.imageview.image = UIImage(data:data)
                self.image_group = UIImage(data:data)
            }
            
        }else if datamyform != nil{
            
            cell.titleLabel.text = datamyform[indexPath.row].title
            let raw = "\(url!)\(datamyform[indexPath.row].image!)"
            let Nsurl = NSURL(string: raw)!
            
            if let data = try? Data(contentsOf: Nsurl as URL)
            {
                
                cell.imageview.image = UIImage(data:data)
                self.image_group = UIImage(data:data)
            }
        }
        return cell
        
    }
    
    
}
