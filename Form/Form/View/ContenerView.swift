//
//  FormView.swift
//  Form
//
//  Created by MSU IT CS on 30/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class ContenerView: UIViewController {

    var token:String?
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    var image:String?
    var role:String?
    var user_id:Int?
    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    var sideMenuOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        username = UserDefaults.standard.getusername()
        name = UserDefaults.standard.getname()
        email = UserDefaults.standard.getemail()
        position = UserDefaults.standard.getposition()
        tel = UserDefaults.standard.gettel()
        image = UserDefaults.standard.getimage()
        role = UserDefaults.standard.getrole()
        user_id = UserDefaults.standard.getuser_id()
        
        NotificationCenter.default.addObserver( self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "groupsview"{
            let next = segue.destination.children[0] as! GroupView
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email	
            next.position = self.position
            next.tel = self.tel
            next.image = self.image
            next.role = self.role
            next.user_id = self.user_id
        }
        else if segue.identifier == "sidemenusegue"{
            let next = segue.destination as! SideMenuView
            next.role = self.role
            next.image = self.image
            next.name = self.name
            
        }
        
    }
    
    @objc func toggleSideMenu(){
        if sideMenuOpen {
            sideMenuOpen = false
            sideMenuConstraint.constant = -240
        }else{
            sideMenuOpen = true
            sideMenuConstraint.constant = 0
        }
        UIView.animate(withDuration: 0.3)
        {
            self.view.layoutIfNeeded()
        }
    }

    
}
