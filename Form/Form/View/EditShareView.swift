//
//  EditShareView.swift
//  Form
//
//  Created by MSU IT CS on 14/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class EditShareView: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    var token:String?
    var form_id:Int?
    var users = [GetShareUsers]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        // Do any additional setup after loading the view.
        setNev(backimage: "arrow-left")
        let link = URL(string:"\(url!)/forms/\(self.form_id!)/shares")
        
        GetShareUsers.get(link!, token: self.token!){
            (result,error) in
            self.users = result!
            print("get")
            
            self.tableview.reloadData()
        }
        
          self.navigationItem.title = "Edit Share People"
    }
    


}
extension EditShareView:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SearchBarView.instanceFromNib()
        view.setUpView()
        
        view.applyBtn.isHidden = true
        view.ResultAnswer.isHidden = true
        view.searchbar.delegate = self
        view.searchbar.placeholder = "Username"
        view.avBtn.isHidden = true
        view.searchbar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true

        return view
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! EditShareCell
        
        cell.tmp = {
            
            
            let link = URL(string:"\(url!)/forms/\(self.form_id!)/shares/\(self.users[indexPath.row].user_id!)")
            
            DeleteShareUsers.delete(link!, token: self.token!){
                (result,error) in
               
              
                
                self.tableview.reloadData()
            }
            
        }
        cell.nameLabel.text = users[indexPath.row].name
        cell.emailLabel.text = users[indexPath.row].email
        
        return cell
    }
    
    
    
}
extension EditShareView:UISearchBarDelegate{
    
    
    
}
