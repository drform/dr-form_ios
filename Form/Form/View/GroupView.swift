//
//  GroupView.swift
//  Form
//
//  Created by MSU IT CS on 14/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import NVActivityIndicatorView



class GroupView: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var collectionview: UICollectionView!
    
    
    private let sectionInsets = UIEdgeInsets(top: 15.0,
                                             left: 15.0,
                                             bottom: 50.0,
                                             right: 15.0)
    private let itemsPerRow: CGFloat = 2
    var isloadgroup:Bool = true
    var token:String? = "bd16da17776c721dd043ff8c47c1172141698c79"
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    var image:String?
    var image_group:UIImage?
    var image_profile:UIImage?
    var role:String?
    var rolestatus:Bool?
    var user_id:Int?
    var editimage:UIImage?
    var edittitle:String?
    var editdescription:String?
    var editid:Int?
    var dataGroup:GetGroups?
    var getGroups:GetGroups?
    var getGroupsnew:GetGroups?
    var refreshControl = UIRefreshControl()
    var offset:Int = 0
    var amount:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionview.dataSource = self
        collectionview.delegate = self
        // Do any additional setup after loading the view.
        username = UserDefaults.standard.getusername()
        name = UserDefaults.standard.getname()
        email = UserDefaults.standard.getemail()
        position = UserDefaults.standard.getposition()
        tel = UserDefaults.standard.gettel()
        image = UserDefaults.standard.getimage()
        role = UserDefaults.standard.getrole()
        user_id = UserDefaults.standard.getuser_id()
        token = UserDefaults.standard.gettoken()
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        self.collectionview.refreshControl = refreshControl
        let link = URL(string: "\(url!)/groups")
        let size = CGSize(width: 30.0, height: 30.0)
        DispatchQueue.global(qos: .background).async {
            
            // load your data here
             self.startAnimating()
            GetGroups.get(link!,keyword: nil,offset: self.offset, token: self.token!){
                (result,error) in
                
               
                self.getGroups = result!
                self.getGroupsnew = result!
              
                
                
            }
            DispatchQueue.main.async {
                // reload your collection view here:
                 self.stopAnimating()
                self.collectionview.reloadData()
               
            }
        }
    
        let raw = "\(url!)\(self.image!)"
        let  Nsurl = NSURL(string: raw )!
        
        if let data = try? Data(contentsOf: Nsurl as URL)
        {
            //  images.append(UIImage(data: data)!)
           image_profile = UIImage(data:data)
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        UINavigationBar.appearance().tintColor = .white

        let createBtn = UIButton(type: .custom)
        createBtn.setImage(UIImage(named: "add-group"), for: .normal)
        createBtn.addTarget(self, action: #selector(createpage), for: .touchUpInside)
        createBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
       
        createBtn.clipsToBounds = true
        
        let barButton = UIBarButtonItem(customView: createBtn)
        
        
        let settingBtn = UIButton(type: .custom)
        settingBtn.setImage(UIImage(named: "menu"), for: .normal)
        settingBtn.addTarget(self, action: #selector(SideMenu), for: .touchUpInside)
        settingBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
   
        settingBtn.clipsToBounds = true
        let barButton1 = UIBarButtonItem(customView: settingBtn)
        
        
        
        let searchBtn = UIButton(type: .custom)
        searchBtn.setImage(UIImage(named: "search"), for: .normal)
        searchBtn.addTarget(self, action: #selector(searchGroups), for: .touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
       
        searchBtn.clipsToBounds = true
        let barButton2 = UIBarButtonItem(customView: searchBtn)
        
        if role == "User Admin"{
            self.navigationItem.leftBarButtonItem = barButton1
            self.navigationItem.rightBarButtonItems = [barButton,barButton2]
            self.rolestatus = true
        }else{
            self.navigationItem.leftBarButtonItem = barButton1
            self.navigationItem.rightBarButtonItems = [barButton2]
        }
        
        
        NotificationCenter.default.addObserver( self, selector: #selector(showprofile), name: NSNotification.Name("profilesegue"), object: nil)
         NotificationCenter.default.addObserver( self, selector: #selector(changepassword), name: NSNotification.Name("changepasswordsegue"), object: nil)
        self.navigationItem.title = "MoMe"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0),NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 22.0)]

      
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.white

    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"),object:nil)

    }
    @objc func doSomething(refreshControl: UIRefreshControl) {
        print("Hello World!")
        offset = 0
        let link = URL(string: "\(url!)/groups")
        GetGroups.get(link!,keyword: nil,offset: self.offset, token: token!){
            (result,error) in
            
            self.getGroups = result!
            
            self.collectionview.reloadData()
            
            
        }
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline){
            refreshControl.endRefreshing()
        }
        // somewhere in your code you might need to call:
        
    }
    
    @objc func searchGroups(){
        print("search")
        searchView.instance.show()
        searchView.instance.from = "group"
        searchView.instance.token = self.token
        
        searchView.instance.dataGroups = {(dataGroup) in
            print(dataGroup)
            self.dataGroup = dataGroup as! GetGroups
            
            self.performSegue(withIdentifier: "searchgroupsegue", sender: self)
            
        }
        searchView.instance.tmp = {
            self.dataGroup = nil
            self.performSegue(withIdentifier: "searchgroupsegue", sender: self)
        }
    }
    
    @objc func createpage() {
        self.performSegue(withIdentifier: "createsegue", sender: self)
    }
    @objc func showprofile(){
        performSegue(withIdentifier: "profilesegue", sender: nil)
    }
    @objc func changepassword(){
        performSegue(withIdentifier: "changepasswordsegue", sender: nil)

    }
    @objc func SideMenu(){
        
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"),object:nil)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "mainformview"{
            let next = segue.destination as! MainFormView
            let cell = sender as? GroupCollectionCell
            let index = collectionview.indexPath(for: cell!)
            
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email
            next.position = self.position
            next.tel = self.tel
            next.image = self.image
           next.role = self.role
            if getGroups!.groupsmodel[(index?.item)!].image != nil{
            let raw = "\(url!)\(getGroups!.groupsmodel[(index?.item)!].image!)"
            let Nsurl = NSURL(string: raw)!
            
            if let data = try? Data(contentsOf: Nsurl as URL)
            {
                 next.image_group =  UIImage(data:data)
            }
            }else{
                next.image_group = UIImage(named: "Group35")
            }
            next.description_group = getGroups?.groupsmodel[(index?.item)!].description
            next.groups_id = getGroups?.groupsmodel[(index?.item)!].id
            next.titlegroup = getGroups?.groupsmodel[(index?.item)!].title
            next.user_id = self.user_id
            
        }else if segue.identifier == "profilesegue"{
            let next = segue.destination  as! ProfileView
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email
            next.position = self.position
            next.tel = self.tel
            next.image = self.image_profile
            next.user_id = self.user_id
            
            
        }else if segue.identifier == "createsegue"{
            let next = segue.destination as! NewGroupView
            
            next.token = self.token
            
        }else if segue.identifier == "searchgroupsegue"{
         
            let next = segue.destination as! searchGroupandFormView
            next.rolestatus = self.rolestatus
            next.datagroup = self.dataGroup
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email
            next.position = self.position
            next.tel = self.tel
            next.image = self.image
            
        }else if segue.identifier == "editsegue"{
            
            let next = segue.destination as! NewGroupView
            next.token = self.token
            next.editid_group = editid
            next.edittile = edittitle
            next.editdescriptione = editdescription
            next.editimage = editimage
            next.updateStatus = 1
            
        }else if segue.identifier == "changepasswordsegue"{
            let next = segue.destination as! ChangepasswordView
            
            next.token = self.token
            
        }
        
        
        
    }


}
extension GroupView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if isloadgroup{
            let link = URL(string: "\(url!)/groups")
            offset = offset + 6
        GetGroups.get(link!,keyword: nil,offset: self.offset, token: token!){
            (result,error) in
            
            self.getGroupsnew = result!
            
            if (self.getGroupsnew?.groupsmodel.count)! == 0 {
                self.isloadgroup = false
                print(self.getGroupsnew?.groupsmodel.count )
                print(self.offset)
            }else{
                
                var i:Int = 0
                while i < (self.getGroupsnew?.groupsmodel.count)!{
                    self.getGroups?.groupsmodel.append((self.getGroupsnew?.groupsmodel[i])!)
                    i = i + 1
                    
                }
             
            }
            self.collectionview.reloadData()
            
        }
        }

    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reuse : UICollectionReusableView? = nil
        if(kind == UICollectionView.elementKindSectionHeader){
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "reuse", for: indexPath) as! GroupReuseableView
            
           
            reuse = view
            
        }
        return reuse!
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 2
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: size)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (self.getGroups?.groupsmodel.count) ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GroupCollectionCell
        
        if role == "User Admin"{
            cell.editBtn.isHidden = false
            cell.actionid = {(id) in
                self.editid = id
            }
            cell.actiontitle =  {(title) in
                self.edittitle = title
            }
            cell.actionimage = {(image) in
                self.editimage = image
            }
            cell.actiondes = {(description) in
                self.editdescription = description
            }
            cell.tmp = {
                self.performSegue(withIdentifier: "editsegue", sender: self)
            
            }
            cell.delete = {
                
                 let alert = UIAlertController(title: "Are you sure?", message: "Delete Group", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        
                        let link = URL(string: "\(url!)/groups/\(self.getGroups!.groupsmodel[indexPath.row].id!)")
                        
                        deleteForm.delete(link!, token: self.token!){
                            (result,error) in
                            
                            alert_successView.instance.showAlert(title: "Delete Groups Success", message: "", alertType: .success)
                            alert_successView.instance.tmp = {
                                
                                let link = URL(string: "\(url!)/groups")
                                GetGroups.get(link!,keyword: nil, offset: self.offset, token: self.token!){
                                    (result,error) in
                                    
                                    self.getGroups = result!
                                    
                                    self.collectionview.reloadData()
                                    
                                    
                                }
                            }
                            
                            
                            
                        }
                            
                    case .cancel:
                         print("cancel")
                    case .destructive:
                         print("destructive")
                   }}))
                        
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("cancel")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                        
                    }}))
                self.present(alert, animated: true, completion: nil)
                        
                        
               
             
                
            }
           
        }
        cell.id_group = getGroups?.groupsmodel[indexPath.row].id
        cell.titlegroup.text = getGroups?.groupsmodel[indexPath.row].title
        cell.desText = getGroups?.groupsmodel[indexPath.row].description
        if getGroups?.groupsmodel[indexPath.row].image != nil{
            
            let raw = "\(url!)\(getGroups!.groupsmodel[indexPath.row].image!)"
            let Nsurl = NSURL(string: raw)!

            if let data = try? Data(contentsOf: Nsurl as URL)
                {
           
                    cell.imageview.image = UIImage(data:data)
                    self.image_group = UIImage(data:data)
            
                }
        }else{
            cell.imageview.image = UIImage(named: "Group35")
        }
        
        
        
        return cell
    }
    
    
    
}
