//
//  NewGroupView.swift
//  Form
//
//  Created by MSU IT CS on 18/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos
import MaterialComponents.MaterialTextFields

class NewGroupView: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var titleText: MDCTextField!
    @IBOutlet weak var descriptionText: MDCTextField!
    @IBOutlet weak var viewbackgroud: UIView!
    
    var editimage:UIImage?
    var edittile:String?
    var editdescriptione:String?
    var updateStatus:Int?
    var editid_group:Int?
    
    var base64String:String?
    var token:String?
    var titleTextController : MDCTextInputControllerOutlined?
    var desTextController : MDCTextInputControllerOutlined?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if edittile != nil{
            
            titleText.text = edittile
            descriptionText.text = editdescriptione
            imageview.image = editimage
            
        }
        desTextController = MDCTextInputControllerOutlined(textInput: descriptionText)
        titleTextController = MDCTextInputControllerOutlined(textInput: titleText)
        
        desTextController?.floatingPlaceholderScale = 0.99
        titleTextController?.floatingPlaceholderScale = 0.99
        
        
        viewbackgroud.layer.cornerRadius = 10
        viewbackgroud.clipsToBounds = true
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: label)
        self.navigationItem.title = "New Group"
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 22.0)]

           self.navigationController?.navigationBar.tintColor = .white
        

        self.setNev(backimage: "close")
        
        
        // Do any additional setup after loading the view.
        let createBt = UIButton()
        createBt.setImage(UIImage(named: "save"), for: .normal)
        createBt.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
        createBt.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        createBt.addTarget(self, action: #selector(createpage), for: .touchUpInside)
        createBt.layer.cornerRadius = createBt.frame.height/2
        createBt.clipsToBounds = true
        let item = UIBarButtonItem(customView: createBt)
        
        self.navigationItem.rightBarButtonItem = item
        
        self.hideKeyboardWhenTappedAround()
        
        base64String = convertImageTobase64(format: .jpeg(1.0), image: UIImage(named: "medical-report")!)!

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        UINavigationBar.appearance().tintColor = .white

    }
    @objc func createpage() {
        
        if updateStatus == 1{
            print("update")
            let link = URL(string: "\(url!)/groups/\(editid_group!)")
            
            UpdateGroup.update(link!, title: titleText.text!, description: descriptionText.text!, image: base64String!, token: token!){
                (result,error) in
                
                alert_successView.instance.showAlert(title: "Update Group Success", message: "", alertType: .success)
                alert_successView.instance.tmp = {
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
                
            }
            
        }else{
        let link = URL(string: "\(url!)/groups")
        
        CreateGroup.create(link!, title: titleText.text ?? "Form", description: descriptionText.text ?? "", image: base64String!, token: token!){
            (result,error) in
            
            if result?.error != "You\'re not admin."{
            alert_successView.instance.showAlert(title: "Create Groups Success", message: "", alertType: .success)
            alert_successView.instance.tmp = {
                self.navigationController?.popViewController(animated: true)
                
                }
            }else{
                alert_successView.instance.showAlert(title: "Create Group Failure", message: "", alertType: .failure)
            }
          }
        }
    }
    @IBAction func importBtn(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source",message: "Choose a source ", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            
            
            image.sourceType = .camera
            image.mediaTypes = [kUTTypeImage as String]
            image.allowsEditing = false
            
            self.present(image ,animated: true,completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Libraby", style: .default, handler: { (action:UIAlertAction) in
             UINavigationBar.appearance().tintColor = UIColor.black
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            image.allowsEditing = false
            self.present(image ,animated: true)
            
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil ))
        
        self.present(actionSheet , animated: true ,completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            base64String = convertImageTobase64(format: .jpeg(1.0), image: image)!
            
            
            // print("++++++++++\(base64String)")
                imageview.image = image
            
        }
        
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    public enum ImageFormat {
        case png
        case jpeg(CGFloat)
    }
    func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = image.pngData()
        case .jpeg(let compression): imageData = image.jpegData(compressionQuality: compression)
        }
        return imageData?.base64EncodedString()
    }
    
    
}
extension UIViewController{
    
    func setNev(backimage:String){
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: backimage)
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: backimage)
        
    }
    
    
}
