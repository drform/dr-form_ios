//
//  FormDetailView.swift
//  Form
//
//  Created by MSU IT CS on 5/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import SafariServices
import NVActivityIndicatorView

struct imageanswer {
    var id = Int()
    var image = UIImage()
}

class FormDetailView: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
 
    
    var imageanswers = [imageanswer]()
    var idanswer:Int?
    var id:Int? = 39
    var token:String?
    var name:String?
    var date:String?
    var image:UIImage?
    var allimages = [UIImage]()
    var allidimages = [Int]()
    var answercell = [Int]()
    var answervalues = [String]()
    var answerModel = [FormsAnswerModel]()
    var group_id:Int? = 1
    var images = [UIImage]()
    var somedateString:String?
    var rowimage = [didRow]()
    var user_item_id:Int?
    var createrModel:CreaterModel?
    var imageapi = [UIImage]()
    var idimageapi = [Int]()
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var secondview: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        secondview.backgroundColor = UIColor(red:0.04, green:0.14, blue:0.33, alpha:1.0)

        tableview.separatorStyle = .none
        
        nameText.text = name
        imageview.layer.cornerRadius = imageview.frame.size.width/2
        imageview.clipsToBounds = true
        imageview.image = image
        
        tableview.dataSource = self
        tableview.delegate = self
        self.hideKeyboardWhenTappedAround()
      
        let editBt = UIButton()
        editBt.setImage(UIImage(named: "edit"), for: .normal)
        editBt.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        editBt.addTarget(self, action: #selector(EditBtn), for: .touchUpInside)
        editBt.clipsToBounds = true
        let item = UIBarButtonItem(customView: editBt)
        
        let deleteBt = UIButton()
        deleteBt.setImage(UIImage(named: "delete"), for: .normal)
        deleteBt.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        deleteBt.addTarget(self, action: #selector(deleteBtn), for: .touchUpInside)
        deleteBt.clipsToBounds = true
        let item2 = UIBarButtonItem(customView: deleteBt)
        
        self.navigationItem.rightBarButtonItems = [item,item2]
        
        
        tableview.register(UINib(nibName: "QuestionTypeTextCell", bundle: Bundle.main), forCellReuseIdentifier: "TextCell")
        tableview.register(UINib(nibName: "QuestionTypeStringCell", bundle: Bundle.main), forCellReuseIdentifier: "StringCell")
        tableview.register(UINib(nibName: "QuestionTypeIntCell", bundle: Bundle.main), forCellReuseIdentifier: "IntCell")
        tableview.register(UINib(nibName: "QuestionTypeFloatCell", bundle: Bundle.main), forCellReuseIdentifier: "FloatCell")
        tableview.register(UINib(nibName: "QuestionTypeDateCell", bundle: Bundle.main), forCellReuseIdentifier: "DateCell")
        tableview.register(UINib(nibName: "QuestionTypeImageCell", bundle: Bundle.main), forCellReuseIdentifier: "ImageCell")
        tableview.register(UINib(nibName: "QuestionTypeCheckBoxCell", bundle: Bundle.main), forCellReuseIdentifier: "CheckBoxCell")
        tableview.register(UINib(nibName: "QuestionTypeDropdownCell", bundle: Bundle.main), forCellReuseIdentifier: "DropdownCell")
         tableview.register(UINib(nibName: "HeaderCheckBoxCell", bundle: Bundle.main), forCellReuseIdentifier: "HeaderCheckBoxCell")
        tableview.register(UINib(nibName: "ShowMultipleImageCell", bundle: Bundle.main), forCellReuseIdentifier: "ShowMultipleImageCell")
        tableview.register(UINib(nibName: "allimagevcollectionView", bundle: Bundle.main), forCellReuseIdentifier: "allimagevcollectionView")
        tableview.register(UINib(nibName: "QuestionTypeFileCell", bundle: Bundle.main), forCellReuseIdentifier: "filecell")
        tableview.register(UINib(nibName: "ClicktolinkTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "LinkCell")
        
        self.tableview.setDynamicRowHeight(withEstimatedHeight: 500.0)

    }
    override func viewDidAppear(_ animated: Bool) {
        rowimage.removeAll()
        allimages.removeAll()
        allidimages.removeAll()
        imageapi.removeAll()
        let link = URL(string:"\(url!)/forms/\(id!)/user_forms/\(idanswer!)")
        DispatchQueue.global(qos: .background).async {
            self.startAnimating()
            GetAnswer.get(link!,group_id:self.group_id!,token: self.token!){
            (result,error)in
            
            self.answerModel = (result?.answer)!
            self.createrModel = result?.creater
            var j:Int? = 0
            self.imageanswers.removeAll()
            for tmp in self.answerModel{
                self.rowimage.append(didRow(type: "", rows: [0],choices: [""],image: [UIImage()]))
                self.answercell.append(tmp.data_type_id!)
                for sub in tmp.answer{
                    self.answervalues.append(sub.value ?? "")
                    if tmp.data_type_id == 10{
                        var i:Int? = 0
                        self.user_item_id = sub.user_item_id
                        while i!<sub.values.count{
                            let raw = "\(url!)\(sub.values[i!].image!)"
                            let  Nsurl = NSURL(string: raw )!
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                self.rowimage[j!].image.append(UIImage(data: data)!)
                                self.imageanswers.append(imageanswer(id: sub.values[i!].id!, image: UIImage(data: data)!))
                                self.imageapi.append(UIImage(data: data)!)
                                self.allimages = self.imageapi
                            }
                            self.allidimages.append(sub.values[i!].id!)
                            self.idimageapi.append(sub.values[i!].id!)
                            i = i!+1
                        }
                    }
                }
                
                self.rowimage[j!].rows.removeFirst()
                self.rowimage[j!].image.removeFirst()
                j = j! + 1
            }
            
         
                DispatchQueue.main.async {
                    // reload your collection view here:
                    self.stopAnimating()
                    self.tableview.reloadData()
                }
         }
          
        }
    }
 
    @objc func EditBtn(){
        self.performSegue(withIdentifier: "editsegue", sender: self)

    }
    @objc func deleteBtn(){
        
        let alert = UIAlertController(title: "Are you sure?", message: "Delete Answer", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                
                let link = URL(string:"\(url!)/forms/\(self.id!)/user_forms/\(self.idanswer!)")
                
                deleteForm.delete(link!, token: self.token!){
                    (result,error) in
                    
                    self.navigationController?.popViewController(animated: true)

                }
                
                
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("cancel")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
        //
        
        
        
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editsegue"{
            
            let next = segue.destination as! EditAnswerView
            
            next.token = self.token
            next.form_id = self.id
            next.answerModel = self.answerModel
            next.somedateString = self.somedateString
            next.user_form_id = self.idanswer
            next.group_id = self.group_id
            next.imageanswers = self.imageanswers
            next.allimages = self.allimages
            next.allidimages = self.allidimages
            next.user_item_id = self.user_item_id
            next.imageapi = self.imageapi
            next.idimageapi = self.idimageapi
        }
    }
    
    func showSafariVC(for path:String){
        guard let path = URL(string: path) else {
            return
        }
        let safariVC = SFSafariViewController(url: path)
        present(safariVC, animated: true)
    }
    
    
    //TABLEVIEW
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if answerModel[section].data_type_id == 7{
            return answerModel[section].answer.count + 1
        }
        else{
            return 1
            
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return answerModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch answercell[indexPath.section] {
        
        case 1:
            let celldate = tableView.dequeueReusableCell(withIdentifier: "DateCell",for:indexPath) as! QuestionTypeDateCell
             if answerModel[indexPath.section].answer.count != 0{
            if answerModel[indexPath.section].answer[indexPath.row].value != nil{
            let myDateString = answerModel[indexPath.section].answer[indexPath.row].value
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: myDateString!)
            dateFormatter.dateFormat = "dd MMMM yyyy"
            somedateString = dateFormatter.string(from: date!)
            celldate.answerText.text = somedateString
                }
            }
             celldate.questionLabel.text = "\(indexPath.section+1).  \(answerModel[indexPath.section].question!)"
            celldate.answerText.isUserInteractionEnabled = false

            return celldate
            
      //  case 2:
            //datetime
        case 3:
            let cellstring = tableView.dequeueReusableCell(withIdentifier: "StringCell",for:indexPath) as! QuestionTypeStringCell
            if answerModel[indexPath.section].answer.count != 0{
            cellstring.answerText.text = answerModel[indexPath.section].answer[indexPath.row].value
            }
            cellstring.answerText.isUserInteractionEnabled = false

            cellstring.questionLabel.text = "\(indexPath.section+1).  \(answerModel[indexPath.section].question!)"
            return cellstring
        case 4:
            let celltext = tableView.dequeueReusableCell(withIdentifier: "TextCell",for: indexPath) as! QuestionTypeTextCell
            
            celltext.answerText.isUserInteractionEnabled = false
            if answerModel[indexPath.section].answer.count != 0{
            celltext.answerText.text = answerModel[indexPath.section].answer[indexPath.row].value
            }
            celltext.questionLabel.text = "\(indexPath.section+1).  \(answerModel[indexPath.section].question!)"
            
            return celltext
            
        case 5:
            let cellint = tableView.dequeueReusableCell(withIdentifier: "IntCell",for: indexPath) as! QuestionTypeIntCell
             if answerModel[indexPath.section].answer.count != 0{
            cellint.answerText.text = answerModel[indexPath.section].answer[indexPath.row].value
            }
            cellint.answerText.isUserInteractionEnabled = false

            cellint.questionLabel.text = "\(indexPath.section+1).  \(answerModel[indexPath.section].question!)"
            
            
            return cellint
        case 6:
            let cellfloat = tableView.dequeueReusableCell(withIdentifier: "FloatCell",for:indexPath) as! QuestionTypeFloatCell
             if answerModel[indexPath.section].answer.count != 0{
            cellfloat.answerText.text = answerModel[indexPath.section].answer[indexPath.row].value
            }
            cellfloat.answerText.isUserInteractionEnabled = false
            cellfloat.questionLabel.text = "\(indexPath.section+1).  \(answerModel[indexPath.section].question!)"
            
            return cellfloat
        case 7:
            switch indexPath.row{
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCheckBoxCell", for: indexPath) as! HeaderCheckBoxCell
                
                
                
                cell.questionLabel.text = "\(indexPath.section+1).  \(answerModel[indexPath.section].question!)"
                
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CheckBoxCell", for: indexPath) as! QuestionTypeCheckBoxCell
                cell.checkBoxview.backgroundColor = UIColor(red:0.11, green:0.62, blue:0.76, alpha:1.0)
                cell.optionLabel.text = answerModel[indexPath.section].answer[indexPath.row - 1].value
                
                
                return cell

            }
        case 8:
            let cellfile = tableView.dequeueReusableCell(withIdentifier: "LinkCell", for: indexPath) as! ClicktolinkTableViewCell
            
                   cellfile.questionLabel.text = "\(indexPath.section+1).  \(answerModel[indexPath.section].question!)"
            
            if answerModel[indexPath.section].answer.count != 0 {
                
            if  answerModel[indexPath.section].answer[indexPath.row].value != nil{
            
            let path = URL(string: answerModel[indexPath.section].answer[indexPath.row].value!)
            let namefile = path?.lastPathComponent
            cellfile.answerLabel.text = namefile
            cellfile.tap = {
                print("tap tap")
                self.showSafariVC(for: "\(url!)\(self.answerModel[indexPath.section].answer[indexPath.row].value!)")
                }
                
            }
        }
            
            return cellfile
        case 9:
            let celldropdown = tableView.dequeueReusableCell(withIdentifier: "DropdownCell", for: indexPath) as! QuestionTypeDropdownCell
            
            celldropdown.questionText.text = "\(indexPath.section+1).  \(answerModel[indexPath.section].question!)"
            
            
            if answerModel[indexPath.section].data_type_id == 9{
               
                if answerModel[indexPath.section].answer.count != 0{
                celldropdown.dropdownbtn.setTitle(answerModel[indexPath.section].answer[indexPath.row].value, for: .normal)
                }
                
            }
                celldropdown.dropdownbtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
            celldropdown.isUserInteractionEnabled = false
            celldropdown.layer.borderWidth = 0
            celldropdown.dropdownbtn.contentHorizontalAlignment = .center
            
            return celldropdown
            
        default:
            
            let cellimage = tableView.dequeueReusableCell(withIdentifier: "ShowMultipleImageCell", for: indexPath) as! ShowMultipleImageCell
            
            
            
            cellimage.allimage = self.rowimage[indexPath.section].image
            cellimage.questionLabel.text = "\(indexPath.section+1).  \(answerModel[indexPath.section].question!)"
            cellimage.collectionview.reloadData()
            return cellimage
            
        
        }
        
        
    }
    //END TABLEVIEW
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
