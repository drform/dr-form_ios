//
//  searchView.swift
//  Form
//
//  Created by MSU IT CS on 9/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class searchView: UIView {

    static let instance = searchView()
    var from:String?
    var token:String?
    var group_id:Int?
    var tmp: (() -> ())?
    var dataGroups: ((AnyObject) -> ())?
    var dataForm: (([GetFormDetail]) -> ())?

    @IBOutlet var parantview: UIView!
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var applyBtn: UIView!
    @IBOutlet weak var cancelBt: UIView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override init(frame: CGRect){
        super.init(frame:frame)
        Bundle.main.loadNibNamed("searchView", owner: self, options: nil)
        commonInit()
    }
    
    private func commonInit(){
        
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        
        applyBtn.layer.cornerRadius = 5
        applyBtn.clipsToBounds = true
        
        
        
        parantview.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parantview.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    
    func show() {
        UIApplication.shared.keyWindow?.addSubview(parantview)
        self.parantview.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.parantview.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.parantview.alpha = 1.0
            self.parantview.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }

    @IBAction func cancelBtn(_ sender: Any)
    {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.parantview.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.parantview.alpha = 0.0;
            
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.parantview.removeFromSuperview()
            }
        });
        from = nil
        searchbar.text = nil
    }
    
    @IBAction func applyBt(_ sender: Any) {
        if from == "group"{
            print(searchbar.text)
            let link = URL(string: "\(url!)/groups")
            GetGroups.get(link!, keyword: searchbar.text!, offset: 0, token: token!){ (result,error) in
                if result?.groupsmodel.count != 0{
                    
                   
                    self.dataGroups?(result!)
                    
                }else{
                   self.tmp?()
                }
                

                
            }
            
        }else{
            let link = URL(string: "\(url!)/forms")
            GetFormDetail.get(link!, group_id: group_id!,offset: 0, allform: false, keyword: searchbar.text!, token: token!){
                (result,error) in
                if result?.count != 0{
                    
                    self.dataForm?(result!)
                    
                }else{
                    
                }
            }
            print("Another")
        }
        
        
        UIView.animate(withDuration: 0.25, animations: {
            self.parantview.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.parantview.alpha = 0.0;
            
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.parantview.removeFromSuperview()
            }
        });
        searchbar.text = nil

    }
    

}


