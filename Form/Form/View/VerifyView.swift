//
//  VerifyView.swift
//  Form
//
//  Created by MSU IT CS on 19/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class VerifyView: UIViewController {

    @IBOutlet weak var verifyText: MDCTextField!
    @IBOutlet weak var confirmBtn: UIButton!
    var email:String?
    
    var verifyTextController:MDCTextInputControllerOutlined?
   
    override func viewDidLoad() {
        super.viewDidLoad()

            verifyTextController = MDCTextInputControllerOutlined(textInput: verifyText)
   
        
        confirmBtn.layer.cornerRadius = 24
        confirmBtn.clipsToBounds = true
        confirmBtn.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        
        confirmBtn.addTarget(self, action: #selector(confirmBt), for: .touchUpInside)

    }
    
    
    @objc func confirmBt(){
        
        let link = URL(string: "\(url!)/password/reset")
        
        Forgetpassword.create(link!, email: nil,pin:verifyText.text!){(result,error) in
            
            if result?.message == "Correct token."{
                
                self.performSegue(withIdentifier: "resetsegue", sender: self)
                
            }else{
                alert_successView.instance.showAlert(title: "Pin incorrect", message: "", alertType: .failure)
            }
            
        }


    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "resetsegue"{
            
            let next = segue.destination as! ResetpasswordView
            
            next.pin = verifyText.text!

            
            
        }
    }
    
    
    
    
}
