//
//  AdvanceSearchView.swift
//  Form
//
//  Created by MSU IT CS on 15/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

protocol sendAvSearch {
    func send(_ users:[Any])

}
struct titlequestion {
    var title:String?
    var item_id:Int?
}
class AdvanceSearchView: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchview: UIView!
    @IBOutlet weak var searchBtn: UIButton!
    
    var users = [FormDetailModel]()
    var form_id:Int?
    var token:String?
    var questionitems = [FormsQuestionModel]()
    var questionarray = [Int]()
    var questionsearch = [FormsQuestionModel]()
    var values = [String]()
    var choices = [DropdownModel]()
    var choicesCheckBox = [DropdownModel]()
    var rows = [Int]()
    var checklist = [Bool]()
    var valuesAlls = [valuesall]()
    var data_type = [Int]()
    var item_id = [Int]()
    var questiontitle = [titlequestion]()

    var searchDelegate:sendAvSearch?
    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.delegate = self
        tableview.dataSource = self
        // Do any additional setup after loading the view.
        regisNib()
        self.hideKeyboardWhenTappedAround()
        tableview.separatorStyle = .none
        
        searchBtn.layer.cornerRadius = 24
        searchBtn.clipsToBounds = true
        
        self.navigationItem.title = "Advance Search"

        let link = URL(string:"\(url!)/form_items/\(form_id!)")
        GetQuestion.get(link!, token: token!){
            (result,error) in
            
            self.questionitems = (result?.questionitems)!
            var j:Int? = 0
            var a:Int? = 0
            for tmp in self.questionitems{
                
                if tmp.av_search == true{
                self.questionsearch.append(tmp)
                self.questiontitle.append((titlequestion(title: tmp.title, item_id: tmp.id)))
                self.questionarray.append(tmp.data_type_id!)
                self.values.append("")
                self.item_id.append(0)
                self.data_type.append(0)
                if (self.questionarray[j!] == 9){
                    for sub in tmp.dropdown{
                        
                        self.choices.append(sub)
                        
                        
                    }
                }else if (self.questionarray[j!] == 7){
                    
                    for sub in tmp.dropdown{
                        self.choicesCheckBox.append(sub)
                    }
                    
                }
                j = j! + 1
                }
                
            self.rows = [0]
            var i:Int? = 0
            while (i!<self.choicesCheckBox.count){
                
                self.rows.append(1)
                
                
                i = i! + 1
            }
                
            

            
            self.tableview.reloadData()
            
            }
            
        }
    }
    
    func regisNib () -> Void{
        
        tableview.register(UINib(nibName: "QuestionTypeTextCell", bundle: Bundle.main), forCellReuseIdentifier: "TextCell")
        tableview.register(UINib(nibName: "QuestionTypeStringCell", bundle: Bundle.main), forCellReuseIdentifier: "StringCell")
        tableview.register(UINib(nibName: "QuestionTypeIntCell", bundle: Bundle.main), forCellReuseIdentifier: "IntCell")
        tableview.register(UINib(nibName: "QuestionTypeFloatCell", bundle: Bundle.main), forCellReuseIdentifier: "FloatCell")
        tableview.register(UINib(nibName: "QuestionTypeDateCell", bundle: Bundle.main), forCellReuseIdentifier: "DateCell")
        tableview.register(UINib(nibName: "QuestionTypeImageCell", bundle: Bundle.main), forCellReuseIdentifier: "ImageCell")
        tableview.register(UINib(nibName: "QuestionTypeCheckBoxCell", bundle: Bundle.main), forCellReuseIdentifier: "CheckBoxCell")
        tableview.register(UINib(nibName: "QuestionTypeDropdownCell", bundle: Bundle.main), forCellReuseIdentifier: "DropdownCell")
        
        tableview.register(UINib(nibName: "HeaderCheckBoxCell", bundle: Bundle.main), forCellReuseIdentifier: "HeaderCheckBoxCell")
        
        self.tableview.setDynamicRowHeight(withEstimatedHeight: 200.0)

    }
    
    
    

    @IBAction func searchBt(_ sender: Any) {
        
        var i:Int? = 0
        var tmp = [String:AnyObject]()
        var array = [Any]()
        
        while (i!<values.count){
            
            if values[i!] != ""{
            tmp["form_item_id"] = item_id[i!] as AnyObject
            tmp["value"] = values[i!] as AnyObject
            tmp["type"] = data_type[i!] as AnyObject
            
            array.append(tmp)
            }
            i = i! + 1
        }
        
        let link = URL(string:"\(url!)/user_form_items")
        
        AdvanceSearch.post(link!, user_form_item: array, token: token!){
            (result,error) in
            
            
            self.users = (result?.users)!
            self.searchDelegate?.send(self.users)
            self.navigationController?.popViewController(animated: true)

        }
        
    }
    
}
extension AdvanceSearchView:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (questionarray[section] == 7)
        {
            return  questionsearch[section].dropdown.count + 1
            
        }
        else{
            return 1
            
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return questionarray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch questionarray[indexPath.section] {
            
        case 1:
            let celldate = tableView.dequeueReusableCell(withIdentifier: "DateCell",for:indexPath) as! QuestionTypeDateCell
            
            celldate.backgroundColor = .white
            celldate.answerText.backgroundColor = .white
            celldate.answerText.textColor = .gray
            celldate.answerText.layer.borderWidth = 0.25
            celldate.answerText.layer.cornerRadius = 10
            celldate.answerText.clipsToBounds = true
            celldate.questionLabel.textColor = .black
            
            
            celldate.questionLabel.text = "\(indexPath.section+1).  \(questiontitle[indexPath.section].title!)"
            
            
            
            celldate.actionText = {(text) in
                
                if self.questionarray.count < self.values.count{
                    self.values.removeLast()
                }
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        self.data_type[indexPath.section] = 1
                        self.item_id[indexPath.section] = self.questiontitle[indexPath.section].item_id!
                        
                        
                        
                        
                    }else{
                        self.values.append(text)
                        self.data_type.append(1)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                    self.data_type.append(1)
                }
                
                
                print(self.values.count)
            }
            
            return celldate
            
        case 2:
            
            
            
            return UITableViewCell()
            
        case 3:
            let cellstring = tableView.dequeueReusableCell(withIdentifier: "StringCell",for:indexPath) as! QuestionTypeStringCell
            cellstring.backgroundColor = .white
            cellstring.answerText.backgroundColor = .white
            cellstring.answerText.textColor = .gray
            cellstring.answerText.layer.borderWidth = 0.25
            cellstring.answerText.layer.cornerRadius = 10
            cellstring.answerText.clipsToBounds = true
            
            cellstring.questionLabel.textColor = .black
            
            
            cellstring.questionLabel.text = "\(indexPath.section+1).  \(questiontitle[indexPath.section].title!)"
            
            
            cellstring.actionText = {(text) in
                
                
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        self.data_type[indexPath.section] = 3
                        self.item_id[indexPath.section] = self.questiontitle[indexPath.section].item_id!
                        
                        
                    }else{
                        self.values.append(text)
                        self.data_type.append(3)

                    }
                    
                    
                }else {
                    self.values.append(text)
                    self.data_type.append(3)

                }
                
                
                print(self.values.count)
            }
            
            return cellstring
        case 4:
            let celltext = tableView.dequeueReusableCell(withIdentifier: "TextCell",for: indexPath) as! QuestionTypeTextCell
            
            celltext.backgroundColor = .white
            celltext.answerText.backgroundColor = .white
            celltext.answerText.textColor = .gray
            celltext.questionLabel.textColor = .black
            celltext.answerText.layer.borderWidth = 0.25
            celltext.answerText.layer.cornerRadius = 10
            celltext.answerText.clipsToBounds = true
            
            celltext.questionLabel.text = "\(indexPath.section+1).  \(questiontitle[indexPath.section].title!)"
            
            
            print(questionarray)
            
            
            celltext.actionText = {(text) in
                
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        self.values[indexPath.section] = text
                        self.data_type[indexPath.section] = 4
                        self.item_id[indexPath.section] = self.questiontitle[indexPath.section].item_id!
                        
                        
                    }else{
                        self.values.append(text)
                        self.data_type.append(4)
                    }
                    
                    
                }else {
                    self.values.append(text)
                    self.data_type.append(4)

                }
                
                
                print("datatyoe :\(self.data_type)")
            }
            
            
            return celltext
            
        case 5:
            let cellint = tableView.dequeueReusableCell(withIdentifier: "IntCell",for: indexPath) as! QuestionTypeIntCell
            cellint.backgroundColor = .white
            cellint.answerText.backgroundColor = .white
            cellint.answerText.textColor = .gray
            cellint.answerText.layer.borderWidth = 0.25
            cellint.answerText.layer.cornerRadius = 10
            cellint.answerText.clipsToBounds = true
            cellint.questionLabel.textColor = .black
            cellint.questionLabel.text = "\(indexPath.section+1).  \(questiontitle[indexPath.section].title!)"
            
            
            
            cellint.actionText = {(text) in
                
                if self.questionarray.count < self.values.count{
                    self.values.removeLast()
                }
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                       
                        self.data_type[indexPath.section] = 5
                        self.item_id[indexPath.section] = self.questiontitle[indexPath.section].item_id!
                        
                        
                        
                    }else{
                        self.values.append(text)
                        self.data_type.append(5)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                    self.data_type.append(5)
                }
                
                
                print(self.values.count)
            }
            
            return cellint
        case 6:
            let cellfloat = tableView.dequeueReusableCell(withIdentifier: "FloatCell",for:indexPath) as! QuestionTypeFloatCell
            cellfloat.backgroundColor = .white
            cellfloat.answerText.backgroundColor = .white
            cellfloat.answerText.textColor = .gray
            cellfloat.answerText.layer.borderWidth = 0.25
            cellfloat.answerText.layer.cornerRadius = 10
            cellfloat.answerText.clipsToBounds = true
            
            cellfloat.questionLabel.textColor = .black
            
            cellfloat.questionLabel.text = "\(indexPath.section+1).  \(questiontitle[indexPath.section].title!)"
            
            
            cellfloat.actionText = {(text) in
                
                if self.questionarray.count < self.values.count{
                    self.values.removeLast()
                }
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        self.data_type[indexPath.section] = 6
                        self.item_id[indexPath.section] = self.questiontitle[indexPath.section].item_id!
                        
                        
                        
                        
                    }else{
                        self.values.append(text)
                        self.data_type.append(6)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                    self.data_type.append(6)
                }
                
                
                print(self.values.count)
            }
            
            return cellfloat
            
        case 7:
            switch indexPath.row{
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCheckBoxCell", for: indexPath) as! HeaderCheckBoxCell
                
                cell.questionLabel.text = "\(indexPath.section+1).  \(questiontitle[indexPath.section].title!)"
                
                return cell
                
            default:
                
                let cellcheckbox  = tableView.dequeueReusableCell(withIdentifier: "CheckBoxCell", for: indexPath) as! QuestionTypeCheckBoxCell
                
                valuesAlls.append(valuesall(order_no: indexPath.section+1, Bool: false, value: self.questionsearch[indexPath.section].dropdown[indexPath.row - 1].id!))
                
                checklist.append(false)
                
                
                cellcheckbox.optionLabel.text = questionsearch[indexPath.section].dropdown[indexPath.row - 1].dropdown_value
                
                
                cellcheckbox.tmp = {
                    
                    let checkbox = cellcheckbox.checkBoxview
                    //
                    //
                    if ((checkbox?.isSelected)!){
                        checkbox?.isSelected = false
                        checkbox?.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
                        
                        
                        self.valuesAlls[indexPath.row-1] = valuesall(order_no: indexPath.section+1, Bool: false, value:  self.questionsearch[indexPath.section].dropdown[indexPath.row - 1].id!)
                        
                        
                        
                        
                    }else{
                        checkbox?.backgroundColor = UIColor(red:0.11, green:0.62, blue:0.76, alpha:1.0)
                        checkbox?.isSelected = true
                        
                        //  self.checklist[indexPath.row-1] = true
                        self.valuesAlls[indexPath.row-1] = (valuesall(order_no: indexPath.section+1, Bool: true, value: self.questionsearch[indexPath.section].dropdown[indexPath.row - 1].id!))
                        let str:String = String(describing: self.questionsearch[indexPath.section].dropdown[indexPath.row - 1].id!)
                        self.values[indexPath.section] = str
                        self.data_type[indexPath.section] = 7
                        self.item_id[indexPath.section] = self.questiontitle[indexPath.section].item_id!

                        // self.valueCheckbox[indexPath.row-1].value? = str
                        //        self.valuesAll[indexPath.row-1].Bool = true
                        
                        
                        
                    }
                    
                    
                }
                return cellcheckbox
            }
            
            
            
        case 9:
            let celldropdown = tableview.dequeueReusableCell(withIdentifier: "DropdownCell", for: indexPath) as! QuestionTypeDropdownCell
             
            celldropdown.questionText.text = "\(indexPath.section+1).  \(questiontitle[indexPath.section].title!)"
            
            var item = [Int]()
            var dataSource = [String]()
            celldropdown.dropdownbtn.setTitleColor(UIColor.gray, for: UIControl.State.normal)
            
            for tmp in choices{
                if self.questionitems[indexPath.section].id == tmp.form_item_id{
                    item.append(tmp.id!)
                    dataSource.append(tmp.dropdown_value!)
                }
            }
            celldropdown.dropdown.dataSource = dataSource
            
            
            celldropdown.actionText = {(text) in
                
                let id:Int =  dataSource.index(where:{$0 == text})!
                
                let values_id = item[id]
                
                self.values[indexPath.section] = String(describing: values_id)
                self.data_type[indexPath.section] = 9
                self.item_id[indexPath.section] = self.questiontitle[indexPath.section].item_id!

                
            }
            return celldropdown
        default:
            return UITableViewCell()
        }
        
        
    }
    
    
    
    
}
