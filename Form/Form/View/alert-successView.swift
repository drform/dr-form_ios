//
//  alert-successView.swift
//  Form
//
//  Created by MSU IT CS on 5/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import Foundation
class alert_successView: UIView {

    static let instance = alert_successView()
    @IBOutlet var parentView: UIView!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var tmp:(() -> ())?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override init(frame: CGRect){
        super.init(frame:frame)
        Bundle.main.loadNibNamed("alert-successView", owner: self, options: nil)
        commonInit()
    }
    
    private func commonInit(){
        confirmBtn.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        
        confirmBtn.layer.cornerRadius = 24
        confirmBtn.clipsToBounds = true
        
        
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    enum AlertType {
        case success
        case failure
    }
    func showAlert(title:String,message:String,alertType:AlertType) {
        
        self.descriptionLabel.text = message
        self.titleLabel.text  = title
        switch alertType {
        case .success:
            imageview.image = UIImage(named: "icon-success")
        default:
            imageview.image = UIImage(named: "icon-fail")
        }
        
        UIApplication.shared.keyWindow?.addSubview(parentView)
        self.parentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.parentView.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.parentView.alpha = 1.0
            self.parentView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)

        });
        
    }
    
    @IBAction func confirmBt(_ sender: Any) {
        
        tmp?()
        
        UIView.animate(withDuration: 0.25, animations: {
            self.parentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.parentView.alpha = 0.0;
            
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.parentView.removeFromSuperview()
            }
        });
    }
    
    
}


