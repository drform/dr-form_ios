//
//  ResetpasswordView.swift
//  Form
//
//  Created by MSU IT CS on 20/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class ResetpasswordView: UIViewController {

    
    @IBOutlet weak var passwordText: MDCTextField!
    @IBOutlet weak var repasswordText: MDCTextField!
    @IBOutlet weak var confirmBtn: UIButton!
    
    var passwordTextController:MDCTextInputControllerOutlined?
    var repasswordTextController:MDCTextInputControllerOutlined?
    
    var pin:String?
    override func viewDidLoad() {
        super.viewDidLoad()

        passwordTextController = MDCTextInputControllerOutlined(textInput: passwordText)
        repasswordTextController = MDCTextInputControllerOutlined(textInput: repasswordText)

        confirmBtn.layer.cornerRadius = 24
        confirmBtn.clipsToBounds = true
        confirmBtn.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        
        confirmBtn.addTarget(self, action: #selector(confirmBt), for: .touchUpInside)

    }
    
    
    @objc func confirmBt(){
    
        let link = URL(string: "\(url!)/password/reset")
        
        Forgetpassword.create(link!, email: repasswordText.text!,pin:pin!){(result,error) in
            
            
            if self.passwordText.text == self.repasswordText.text{
            if result?.message == "Correct token."{
                
                self.performSegue(withIdentifier: "resetsuccesssegue", sender: self)
                
                }else{
               
                    }
            }
            else{
                alert_successView.instance.showAlert(title: "Password not match", message: "", alertType: .failure)
            }
            
            
            
        }

    
    }



}
