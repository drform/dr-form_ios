//
//  NewFormView.swift
//  Form
//
//  Created by MSU IT CS on 30/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import Photos
import AVKit
import AVFoundation
import MobileCoreServices
import ParallaxHeader
import MaterialComponents.MaterialTextFields

protocol test {
    func test(titleText:String)
}

struct didRow {
    var type:String?
    var rows = [Int]()
    var choices = [String]()
    var image = [UIImage]()
    
}
struct Rowdid {
    var type:String?
    var rows = [Int]()
    var choices = [String]()
    var dropdown_id = [Int]()
    
}
class NewFormView: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate{
   
    
   
    
    @IBOutlet weak var addQuestionview: UIButton!
    @IBOutlet weak var tabelview: UITableView!
    
    
    
    
    var base64String:String?
    var token:String?
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    var x = 0
    var groups_id:Int? = 1
    
    var Save:Bool? = false
    var titleText:String?
    var DescriptionText:String?
    var headerview:NewFormHeaderView?
    var switchrequired:Bool? = false
    var switchfilter:Bool? = false
    var Form_item:[Form_itemsModel]?
    var textdata = [String]()
    var actionrequired = [Bool]()
    var actionfilter = [Bool]()
    var insertCell = [Int]()
    var datatype = [String]()
    var indexDropdowns = [Int]()
    var indexDropdown:Int? = -1
    var indexIsRows:Int? = 2
    var indexChoices:Int? = 0
    var Multirows = [[Int]]()
    var rows = [didRow]()
    var choices = [String]()
    var rowsCheckbox = [Int]()
    var choiceCheckbox = [String]()
   // var arrayTitle = [String]()
    var cellarray = [TextfieldCell]()
   
    
    var choosecell = [UITableViewCell]()
  
    func setHeaderView() -> Void{
        headerview = NewFormHeaderView.instanceFromNib()
        headerview!.setUpView()
        headerview!.setNeedsLayout()
        headerview!.layoutIfNeeded()
       
        headerview!.imageBtn.addTarget(self, action: #selector(importImage), for: .touchUpInside)
        
        
        
        
        headerview?.delegate = self
        tabelview.parallaxHeader.view = headerview!
        tabelview.parallaxHeader.height = 280
        tabelview.parallaxHeader.minimumHeight = 0
        tabelview.parallaxHeader.mode = .centerFill
        
   
        
       
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //print("New Form: \(token!)")
        setHeaderView()
        tabelview.dataSource = self
        tabelview.delegate = self
        self.tabelview.setDynamicRowHeight(withEstimatedHeight: 100.0)
        
        base64String = convertImageTobase64(format: .jpeg(1.0), image: UIImage(named: "medical-report")!)!

        rowsCheckbox = [0,1,2]
        
        let createBt = UIButton()
        createBt.setImage(UIImage(named: "save"), for: .normal)
        createBt.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
        createBt.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        createBt.addTarget(self, action: #selector(createFormBtn), for: .touchUpInside)
        createBt.layer.cornerRadius = createBt.frame.height/2
        createBt.clipsToBounds = true
        let item = UIBarButtonItem(customView: createBt)
        
        self.navigationItem.rightBarButtonItem = item

        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        tabelview.separatorStyle = .none
        tabelview.register(UINib(nibName: "TextfieldCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
      tabelview.register(UINib(nibName: "TitleCell", bundle: Bundle.main), forCellReuseIdentifier: "cellsave")
        
        tabelview.register(UINib(nibName: "DropdownCell1", bundle: Bundle.main), forCellReuseIdentifier: "DropdownCell1")
        tabelview.register(UINib(nibName: "DropdownCell2", bundle: Bundle.main), forCellReuseIdentifier: "DropdownCell2")
        tabelview.register(UINib(nibName: "DropdownCell3", bundle: Bundle.main), forCellReuseIdentifier: "DropdownCell3")
        
        
        
        self.navigationItem.title = "New Form"
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 22.0)]
  self.navigationController?.navigationBar.tintColor = .white
        self.setNev(backimage: "close")
        addQuestionview.layer.cornerRadius = 24
        addQuestionview.clipsToBounds = true
        
        
    }
    
   
    
    
    
    
   
    @objc func createFormBtn(){
        
        
        
        print("Sended")
        var tmp = [String:AnyObject]()
        var array = [Any]()
        var dropdown = [String:AnyObject]()
        var checkbox = [String:AnyObject]()

       
        var arraydropdown = [Any]()
        var arraycheckbox = [Any]()
      
        
        

        var i:Int? = 0
        
  
        while(i!<self.rows.count){
            
            
            tmp["order_no"] = i!+1 as AnyObject
            tmp["data_type"] = datatype[i!] as AnyObject
            tmp["title"] = textdata[i!] as AnyObject
            tmp.removeValue(forKey: "value")
            
            if (datatype[i!] == "Dropdown"){
                var j:Int? = 0
               
                while(j!<self.rows[i!].choices.count){
                    
                    dropdown["data"] = self.rows[i!].choices[j!] as AnyObject
                    
                    arraydropdown.append(dropdown)
                    j = j! + 1
                }
                tmp["value"] = arraydropdown as AnyObject
                arraydropdown.removeAll()

            }else if (datatype[i!] == "Checkbox"){
                 var k:Int? = 0
                
                while(k!<self.rows[i!].choices.count){
                    
                    checkbox["data"] = self.rows[i!].choices[k!] as AnyObject
                    
                    arraycheckbox.append(checkbox)
                    k = k! + 1
                }
                tmp["value"] = arraycheckbox as AnyObject
                arraycheckbox.removeAll()
            }
            
           
            tmp["required"] = actionrequired[i!] as AnyObject
            tmp["av_search"] = actionfilter[i!] as AnyObject
            
            array.append(tmp)
            
            i = i! + 1
        }
        
    


        
        let link = URL(string: "\(url!)/forms")
        
        CreateForm.create(link!,group_id:groups_id!, title: (titleText) ?? "title", description: (DescriptionText) ?? "", image: base64String ?? "",token:token!, form_items: array){
                    (result,error) in
            
            alert_successView.instance.showAlert(title: "Create Form Success", message: "", alertType: .success)
            alert_successView.instance.tmp = {
                self.navigationController?.popViewController(animated: true)

            }


            
                }

        
        
    }
    @objc func importImage() {
        
        let image = UIImagePickerController()
        image.delegate = self
       
        let actionSheet = UIAlertController(title: "Photo Source",message: "Choose a source ", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            
            
            image.sourceType = .camera
            image.mediaTypes = [kUTTypeImage as String]
            image.allowsEditing = false
            
            self.present(image ,animated: true,completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Libraby", style: .default, handler: { (action:UIAlertAction) in
             UINavigationBar.appearance().tintColor = UIColor.black
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            image.allowsEditing = false
            self.present(image ,animated: true)
            
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil ))
        
        self.present(actionSheet , animated: true ,completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            base64String = convertImageTobase64(format: .jpeg(1.0), image: image)!
            
            
            // print("++++++++++\(base64String)")
           headerview?.imageview.image = image
           
            
        }
        
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    public enum ImageFormat {
        case png
        case jpeg(CGFloat)
    }
    func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = image.pngData()
        case .jpeg(let compression): imageData = image.jpegData(compressionQuality: compression)
        }
        return imageData?.base64EncodedString()
    }
    
    
    @IBAction func addQuestion(_ sender: Any) {
        
        self.tabelview.isHidden = false
      
        
        let alertController = UIAlertController(title: "ChooseDataType", message: "", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Long Answer", style: .default) { (action:UIAlertAction) in
            
            
            self.insertCell.append(1)
            self.textdata.append("")
            self.datatype.append("Text")
            self.rows.append(didRow(type: "Text", rows: [0],choices: [""],image:[UIImage()]))
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tabelview.reloadData()
            
           
        }
        let action6 = UIAlertAction(title: "Short Answer", style: .default) { (action:UIAlertAction) in
            
            self.textdata.append("")
            self.datatype.append("String")
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.rows.append(didRow(type: "String", rows: [0],choices: [""],image:[UIImage()]))
            self.insertCell.append(2)
            self.tabelview.reloadData()
//            let index = IndexPath(row: 1, section: self.numberOfSections(in: self.tabelview)-1)
//            self.tabelview.scrollToRow(at: index, at: .bottom, animated: false)
//
        }
        let action7 = UIAlertAction(title: "Integer", style: .default) { (action:UIAlertAction) in
            
            self.insertCell.append(3)
            self.textdata.append("")
            self.datatype.append("Integer")
            self.rows.append(didRow(type: "Integer", rows: [0],choices: [""],image:[UIImage()]))

            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tabelview.reloadData()
            
        }
        let action8 = UIAlertAction(title: "Decimal", style: .default) { (action:UIAlertAction) in
            
            self.insertCell.append(4)
            self.textdata.append("")
            self.datatype.append("Float")
            self.rows.append(didRow(type: "Float", rows: [0],choices: [""],image:[UIImage()]))

            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tabelview.reloadData()
            
        }
        let action2 = UIAlertAction(title: "Date", style: .default) { (action:UIAlertAction) in
            self.insertCell.append(5)
            self.textdata.append("")
            self.datatype.append("Date")
            self.rows.append(didRow(type: "Date", rows: [0],choices: [""],image:[UIImage()]))

            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tabelview.reloadData()
            
        }
        let action3 = UIAlertAction(title: "Dropdown", style: .default) { (action:UIAlertAction) in
            self.insertCell.append(6)
            self.textdata.append("")
            self.datatype.append("Dropdown")
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            //self.Multirows.append(self.rows)
            self.rows.append(didRow(type: "dropdown", rows: [0,1,2],choices: [""],image:[UIImage()]))
            
            self.tabelview.reloadData()
            
        }
        let action4 = UIAlertAction(title: "Checkbox", style: .default) { (action:UIAlertAction) in
            self.insertCell.append(7)
            self.textdata.append("")
            self.datatype.append("Checkbox")
            self.rows.append(didRow(type: "Checkbox", rows: [0,1,2],choices: [""],image:[UIImage()]))

            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tabelview.reloadData()
            
        }
        let action5 = UIAlertAction(title: "Image", style: .default) { (action:UIAlertAction) in
            self.insertCell.append(8)
            self.textdata.append("")
            self.datatype.append("Image")
            self.rows.append(didRow(type: "Image", rows: [0],choices: [""],image:[UIImage()]))

            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tabelview.reloadData()
            
        }
        let action10 = UIAlertAction(title: "File", style: .default){(action:UIAlertAction) in
            
            self.insertCell.append(9)
            self.textdata.append("")
            self.datatype.append("File")
            self.rows.append(didRow(type: "File", rows: [0],choices: [""],image:[UIImage()]))
            
            self.actionrequired.append(false)
            self.actionfilter.append(false)
            self.tabelview.reloadData()
            
        }
        let actionCancel = UIAlertAction(title: "Cancel",style: .cancel,handler: nil)
        
        alertController.addAction(action1)
        alertController.addAction(action6)
        alertController.addAction(action7)
        alertController.addAction(action8)
        alertController.addAction(action2)
        alertController.addAction(action3)
        alertController.addAction(action4)
        alertController.addAction(action5)
        alertController.addAction(action10)
        
        alertController.addAction(actionCancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
}
extension NewFormView: UITableViewDelegate,UITableViewDataSource  {
    
   
  
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if rows[section].type == "dropdown"{
            return rows[section].rows.count
        }else if rows[section].type == "Checkbox"{
            return rows[section].rows.count
        }else{
            return 1
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return rows.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var  sortResult = [Int]()
        var isRow = [Int]()
        var sortCheckbox = [Int]()
        var isRowCheckbox = [Int]()
        if self.rows[indexPath.section].type == "dropdown"{
        
            sortResult = self.rows[indexPath.section].rows.sorted()
               self.rows[indexPath.section].rows = sortResult
             isRow = self.rows[indexPath.section].rows
        }else if self.rows[indexPath.section].type == "Checkbox"{
            
            sortCheckbox = self.rows[indexPath.section].rows.sorted()
            self.rows[indexPath.section].rows = sortCheckbox
            isRowCheckbox = self.rows[indexPath.section].rows
            
        }
     
    
        
        if self.rows[indexPath.section].type == "Text"{
            
            let celltext = tabelview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TextfieldCell
            
            celltext.tmp = {
                self.rows.remove(at: indexPath.section)
                self.textdata.remove(at: indexPath.section)
                self.datatype.remove(at: indexPath.section)
                self.actionfilter.remove(at: indexPath.section)
                self.actionrequired.remove(at: indexPath.section)
                self.tabelview.reloadData()
            }
            
            celltext.TitleText.text = textdata[indexPath.section]
            celltext.DatatypeLabel.text = "Long Answer"
            
            if actionrequired[indexPath.section] == false{
                celltext.requireView.isSelected = false
                
            }else
            {
                celltext.requireView.isSelected = true

                
            }
            if actionfilter[indexPath.section] == false{
                celltext.filterView.isSelected = false

                
            }else{
                celltext.requireView.isSelected = true

                
            }
//            
            celltext.delegate = self as TextfieldCellDelegate
           

            celltext.actionText = { (text) in


                if self.textdata.count > 0{



                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){


                            print("ok")
                            self.textdata[indexPath.section] = text
                            self.datatype[indexPath.section] = "Text"
                        if self.insertCell.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }


                    }else{
                        self.textdata.append(text)
                        self.datatype.append("Text")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)

                    }


                }else {
                    self.textdata.append(text)
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)

                     self.datatype.append("Text")
                }

                print(self.textdata)
                print(self.actionrequired)
                print(self.actionfilter)

            }


            celltext.actionRequired = {(Bool) in

                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){


                        print("ok")
                        self.actionrequired[indexPath.section] = Bool


                    }else{
                        self.actionrequired.append(Bool)
                    }


                }else{
                    self.actionrequired.append(Bool)

                }

            }


            celltext.actionFilter = {(Bool) in

                if self.actionfilter.count > 0{

                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){


                        print("ok")
                        self.actionfilter[indexPath.section] = Bool


                    }else{
                        self.actionfilter.append(Bool)
                    }


                }else{
                    self.actionfilter.append(Bool)

                }
            }
            celltext.actionSave = {(save)
                in

                celltext.frame.size.height = 80

                
            }

            
                return celltext
        }else if self.rows[indexPath.section].type == "String"{
       
            let cellstring = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TextfieldCell
            
            cellstring.tmp = {
                self.rows.remove(at: indexPath.section)
                self.textdata.remove(at: indexPath.section)
                self.datatype.remove(at: indexPath.section)
                self.actionfilter.remove(at: indexPath.section)
                self.actionrequired.remove(at: indexPath.section)
                self.tabelview.reloadData()
            }
            
            cellstring.TitleText.text = textdata[indexPath.section]
            cellstring.DatatypeLabel.text = "Short Answer"
            
            if actionrequired[indexPath.section] == false{
                cellstring.requireView.isSelected = false
                
            }else
            {
                cellstring.requireView.isSelected = true
                
            }
            if actionfilter[indexPath.section] == false{
                cellstring.filterView.isSelected = false
            }else{
                cellstring.filterView.isSelected = true

                
            }
            
            cellstring.delegate = self as TextfieldCellDelegate
            
            cellstring.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                       self.datatype[indexPath.section] = "String"
                        if self.insertCell.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }

                        
                    }else{
                        self.textdata.append(text)
                         self.datatype.append("String")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                     self.datatype.append("String")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            cellstring.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            cellstring.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return cellstring
        }else if self.rows[indexPath.section].type == "Integer"{
        
            let cellint = tabelview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TextfieldCell
            
            cellint.tmp = {
                 self.rows.remove(at: indexPath.section)
                self.textdata.remove(at: indexPath.section)
                self.datatype.remove(at: indexPath.section)
                self.actionfilter.remove(at: indexPath.section)
                self.actionrequired.remove(at: indexPath.section)
                self.tabelview.reloadData()
            }
            
            cellint.TitleText.text = textdata[indexPath.section]
            cellint.DatatypeLabel.text = "Integer"
            if actionrequired[indexPath.section] == false{
                cellint.requireView.isSelected = false
            }else
            {
                cellint.requireView.isSelected = true
            }
            if actionfilter[indexPath.section] == false{
                cellint.filterView.isSelected = false
            }else{
                cellint.filterView.isSelected = true
            }
            cellint.delegate = self as TextfieldCellDelegate
            
            cellint.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "Integer"
                        if self.insertCell.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("Integer")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("Integer")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            cellint.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            cellint.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return cellint
        }else if self.rows[indexPath.section].type == "Float"{
        
            let cellfloat = tabelview.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! TextfieldCell
            
            cellfloat.tmp = {
                 self.rows.remove(at: indexPath.section)
                self.textdata.remove(at: indexPath.section)
                self.datatype.remove(at: indexPath.section)
                self.actionfilter.remove(at: indexPath.section)
                self.actionrequired.remove(at: indexPath.section)
                self.tabelview.reloadData()
            }
            
            cellfloat.TitleText.text = textdata[indexPath.section]
            cellfloat.DatatypeLabel.text = "Decimal"
            
            if actionrequired[indexPath.section] == false{
                cellfloat.requireView.isSelected = false

            }else
            {
                cellfloat.requireView.isSelected = true
            }
            if actionfilter[indexPath.section] == false{
                cellfloat.filterView.isSelected = false
            }else{
                cellfloat.filterView.isSelected = true
            }
            
            cellfloat.delegate = self as TextfieldCellDelegate

            
            cellfloat.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "Float"
                        if self.insertCell.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("Float")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("Float")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            cellfloat.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            cellfloat.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return cellfloat
        }else if self.rows[indexPath.section].type == "Date"{
       
            let celldate = tabelview.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! TextfieldCell
            
            celldate.tmp = {
                 self.rows.remove(at: indexPath.section)
                self.textdata.remove(at: indexPath.section)
                self.datatype.remove(at: indexPath.section)
                self.actionfilter.remove(at: indexPath.section)
                self.actionrequired.remove(at: indexPath.section)
                self.tabelview.reloadData()
            }
            
            celldate.TitleText.text = textdata[indexPath.section]
            celldate.DatatypeLabel.text = "Date"
            
            if actionrequired[indexPath.section] == false{
                celldate.requireView.isSelected = false
            }else
            {
                celldate.requireView.isSelected = true
            }
            if actionfilter[indexPath.section] == false{
                celldate.filterView.isSelected = false
            }else{
                celldate.filterView.isSelected = true
            }
            
            celldate.delegate = self as TextfieldCellDelegate

            celldate.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "Date"
                        if self.insertCell.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("Date")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("Date")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            celldate.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            celldate.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return celldate
        }else if self.rows[indexPath.section].type == "dropdown"{
            
            switch indexPath.row{
            case 0:
                let cell = tabelview.dequeueReusableCell(withIdentifier: "DropdownCell1", for: indexPath) as! DropdownCell1
                
                cell.dataLabel.text = "Dropdown"
                cell.titleText.text = textdata[indexPath.section]
                
                
                cell.delete = {
                    
                     self.rows.remove(at: indexPath.section)
                    self.textdata.remove(at: indexPath.section)
                    self.datatype.remove(at: indexPath.section)
                    self.actionfilter.remove(at: indexPath.section)
                    self.actionrequired.remove(at: indexPath.section)
                    
                    self.tabelview.reloadData()
                }
                cell.actionText = { (text) in
                    
                    
                    if self.textdata.count > 0{
                        
                        
                        
                        if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                            
                            
                            print("ok")
                            self.textdata[indexPath.section] = text
                            self.datatype[indexPath.section] = "Dropdown"
                            if self.insertCell.count < self.actionrequired.count{
                                self.actionrequired.removeLast()
                                self.actionfilter.removeLast()
                            }
                            
                            
                        }else{
                            self.textdata.append(text)
                            self.datatype.append("Dropdown")
                            self.actionrequired.append(false)
                            self.actionfilter.append(false)
                            
                        }
                        
                        
                    }else {
                        self.textdata.append(text)
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                        self.datatype.append("Dropdown")
                    }
                    
                    print(self.textdata)
                    print(self.actionrequired)
                    print(self.actionfilter)
                    
                }
                return cell
            case 1:
                let cell = tabelview.dequeueReusableCell(withIdentifier: "DropdownCell2", for: indexPath) as! DropdownCell2
                
                cell.choiceText.text = self.rows[indexPath.section].choices[indexPath.row-1]
                cell.AddBtn.setImage(UIImage(named: "btn-plus"), for: .normal)
                cell.actionToAdd = {
                
                    self.rows[indexPath.section].rows.append(1)
                    
                   self.indexChoices = self.indexChoices! + 1
                    self.rows[indexPath.section].choices.append("")
                    self.tabelview.reloadData()
                    
                    
                }
                cell.actionTextField = { (word) in
                    
                        if ( self.rows[indexPath.section].choices.count != indexPath.row && (self.rows[indexPath.section].choices.index(where:{$0 == self.rows[indexPath.section].choices[indexPath.row-1]})) != nil ){
                            
                            
                            print("ok")
                            self.rows[indexPath.section].choices[indexPath.row-1] = word
                            
                            
                            
                            
                        }else{
                            
                            
                           self.rows[indexPath.section].choices[0] = word
                            
                       
                        }
                        
                        
                  
                    print(self.rows[indexPath.section].choices)
                }
                
                
                return cell
                
                
                
            default :
             
                
                if isRow[indexPath.row] == 1 {
                    let cell = tabelview.dequeueReusableCell(withIdentifier: "DropdownCell2", for: indexPath) as! DropdownCell2
                    
                    cell.AddBtn.setImage(UIImage(named: "btn-delete"), for: .normal)
                    
                    cell.choiceText.text = self.rows[indexPath.section].choices[indexPath.row-1]
                
                    cell.actionToAdd = {
                        
                        self.rows[indexPath.section].rows.remove(at: indexPath.row)
                        
                        self.rows[indexPath.section].choices.remove(at: indexPath.row - 1)
                       
                        self.tabelview.reloadData()
                        
                        
                    }
                    cell.actionTextField = { (word) in
                       
                      
                            
                        if ( self.rows[indexPath.section].choices.count != indexPath.row && (self.rows[indexPath.section].choices.index(where:{$0 == self.rows[indexPath.section].choices[indexPath.row-1]})) != nil ){
                            
                            
                            print("ok")
                            self.rows[indexPath.section].choices[indexPath.row-1] = word
                            
                            
                            
                            
                            }else{
                               self.rows[indexPath.section].choices[indexPath.row-1] = word
                                
                            }
                            
                            
                      
                        print(self.rows[indexPath.section].choices)
                    }
                    
                    
                    return cell
                }else{
                    let cell = tabelview.dequeueReusableCell(withIdentifier: "DropdownCell3", for: indexPath) as! DropdownCell3
                    
                    
                    if actionrequired[indexPath.section] == false{
                        cell.requireView.isSelected = false
                    }else
                    {
                        cell.requireView.isSelected = true
                    }
                    if actionfilter[indexPath.section] == false{
                        cell.filterView.isSelected = false
                    }else{
                        cell.filterView.isSelected = true
                    }
                    cell.actionRequired = {(Bool) in
                        
                        if self.actionrequired.count > 0{
                            if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                                
                                
                                print("ok")
                                self.actionrequired[indexPath.section] = Bool
                                
                                
                            }else{
                                self.actionrequired.append(Bool)
                            }
                            
                            
                        }else{
                            self.actionrequired.append(Bool)
                            
                        }
                        
                    }
                    
                    
                    cell.actionFilter = {(Bool) in
                        
                        if self.actionfilter.count > 0{
                            
                            if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                                
                                
                                print("ok")
                                self.actionfilter[indexPath.section] = Bool
                                
                                
                            }else{
                                self.actionfilter.append(Bool)
                            }
                            
                            
                        }else{
                            self.actionfilter.append(Bool)
                            
                        }
                    }
                
                    
                    
                    
                    return cell
                }
                
                
            }
            
            return UITableViewCell()
        }else if self.rows[indexPath.section].type == "Checkbox"{
        
            switch indexPath.row{
            case 0:
                let cell = tabelview.dequeueReusableCell(withIdentifier: "DropdownCell1", for: indexPath) as! DropdownCell1
                
                cell.dataLabel.text = "CheckBox"
                cell.titleText.text = textdata[indexPath.section]
                
                
                cell.delete = {
                    
                     self.rows.remove(at: indexPath.section)
                    self.textdata.remove(at: indexPath.section)
                    self.datatype.remove(at: indexPath.section)
                    self.actionfilter.remove(at: indexPath.section)
                    self.actionrequired.remove(at: indexPath.section)
                    self.tabelview.reloadData()
                }
                cell.actionText = { (text) in
                    
                    
                    if self.textdata.count > 0{
                        
                        
                        
                        if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                            
                            
                            print("ok")
                            self.textdata[indexPath.section] = text
                            self.datatype[indexPath.section] = "Checkbox"
                            if self.insertCell.count < self.actionrequired.count{
                                self.actionrequired.removeLast()
                                self.actionfilter.removeLast()
                            }
                            
                            
                        }else{
                            self.textdata.append(text)
                            self.datatype.append("Checkbox")
                            self.actionrequired.append(false)
                            self.actionfilter.append(false)
                            
                        }
                        
                        
                    }else {
                        self.textdata.append(text)
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                        self.datatype.append("Checkbox")
                    }
                    
                    print(self.textdata)
                    print(self.actionrequired)
                    print(self.actionfilter)
                    
                }
                return cell
            case 1:
                let cell = tabelview.dequeueReusableCell(withIdentifier: "DropdownCell2", for: indexPath) as! DropdownCell2
                
                cell.choiceText.text = self.rows[indexPath.section].choices[indexPath.row-1]
                cell.AddBtn.setImage(UIImage(named: "btn-plus"), for: .normal)

                cell.actionToAdd = {
                    
                    self.rows[indexPath.section].rows.append(1)
                    
                    self.indexChoices = self.indexChoices! + 1
                    self.rows[indexPath.section].choices.append("")
                    self.tabelview.reloadData()
                    
                    
                    
                }
                cell.actionTextField = { (word) in
                    
                    if ( self.rows[indexPath.section].choices.count != indexPath.row && (self.rows[indexPath.section].choices.index(where:{$0 == self.rows[indexPath.section].choices[indexPath.row-1]})) != nil ){
                        
                        
                        print("ok")
                        self.rows[indexPath.section].choices[indexPath.row-1] = word
                        
                        
                        
                        
                    }else{
                        
                        
                        self.rows[indexPath.section].choices[0] = word
                        
                        
                    }
                    
                    
                    
                    print(self.rows[indexPath.section].choices)
                }
                
                
                return cell
                
                
                
            default :
                if isRowCheckbox[indexPath.row] == 1 {
                    let cell = tabelview.dequeueReusableCell(withIdentifier: "DropdownCell2", for: indexPath) as! DropdownCell2
                    
                    cell.choiceText.text = self.rows[indexPath.section].choices[indexPath.row-1]
                    cell.AddBtn.setImage(UIImage(named: "btn-delete"), for: .normal)

                    
                    cell.actionToAdd = {
                        
                        self.rows[indexPath.section].rows.remove(at: indexPath.row)
                        
                        self.rows[indexPath.section].choices.remove(at: indexPath.row - 1)
                        
                        self.tabelview.reloadData()
                        
                        
                    }
                    cell.actionTextField = { (word) in
                        
                        
                        
                        if ( self.rows[indexPath.section].choices.count != indexPath.row && (self.rows[indexPath.section].choices.index(where:{$0 == self.rows[indexPath.section].choices[indexPath.row-1]})) != nil ){
                            
                            
                            print("ok")
                            self.rows[indexPath.section].choices[indexPath.row-1] = word
                            
                            
                            
                        }else{
                            self.rows[indexPath.section].choices[indexPath.row-1] = word
                            
                        }
                        
                        
                        
                        print(self.rows[indexPath.section].choices)
                    }
                    
                    
                    return cell
                }else{
                    let cell = tabelview.dequeueReusableCell(withIdentifier: "DropdownCell3", for: indexPath) as! DropdownCell3
                    
                    
                    if actionrequired[indexPath.section] == false{
                        cell.requireView.isSelected = false
                    }else
                    {
                        cell.requireView.isSelected = true
                    }
                    if actionfilter[indexPath.section] == false{
                        cell.filterView.isSelected = false
                    }else{
                        cell.filterView.isSelected = true
                    }
                    
                    cell.actionRequired = {(Bool) in
                        
                        if self.actionrequired.count > 0{
                            if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                                
                                
                                print("ok")
                                self.actionrequired[indexPath.section] = Bool
                                
                                
                            }else{
                                self.actionrequired.append(Bool)
                            }
                            
                            
                        }else{
                            self.actionrequired.append(Bool)
                            
                        }
                        
                    }
                    
                    
                    cell.actionFilter = {(Bool) in
                        
                        if self.actionfilter.count > 0{
                            
                            if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                                
                                
                                print("ok")
                                self.actionfilter[indexPath.section] = Bool
                                
                                
                            }else{
                                self.actionfilter.append(Bool)
                            }
                            
                            
                        }else{
                            self.actionfilter.append(Bool)
                            
                        }
                    }
                    
                    
                    
                    
                    return cell
                }
                
                
            }
            
            return UITableViewCell()
            
            
        }else if self.rows[indexPath.section].type == "File"{
            
            let cellfile = tabelview.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! TextfieldCell
            
            cellfile.tmp = {
                self.rows.remove(at: indexPath.section)
                self.textdata.remove(at: indexPath.section)
                self.datatype.remove(at: indexPath.section)
                self.actionfilter.remove(at: indexPath.section)
                self.actionrequired.remove(at: indexPath.section)
                self.tabelview.reloadData()
            }
            
            cellfile.TitleText.text = textdata[indexPath.section]
            cellfile.DatatypeLabel.text = "File"
            
            if actionrequired[indexPath.section] == false{
                cellfile.requireView.isSelected = false
                
            }else
            {
                cellfile.requireView.isSelected = true
            }
            if actionfilter[indexPath.section] == false{
                cellfile.filterView.isSelected = false
            }else{
                cellfile.filterView.isSelected = true
            }
            
            cellfile.delegate = self as TextfieldCellDelegate
            
            
            cellfile.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "File"
                        if self.insertCell.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("File")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("File")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            cellfile.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            cellfile.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return cellfile
        
        
        
        
        }else{
            let cellimage = tabelview.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! TextfieldCell
            
            cellimage.tmp = {
                 self.rows.remove(at: indexPath.section)
                self.textdata.remove(at: indexPath.section)
                self.datatype.remove(at: indexPath.section)
                self.actionfilter.remove(at: indexPath.section)
                self.actionrequired.remove(at: indexPath.section)
                self.tabelview.reloadData()
            }
           // cellimage.filterLabel.isHidden = true
            cellimage.filterView.isHidden = true
            cellimage.TitleText.text = textdata[indexPath.section]
            cellimage.DatatypeLabel.text = "Image"
            
            if actionrequired[indexPath.section] == false{
                cellimage.requireView.isSelected = false
            }else
            {
                cellimage.requireView.isSelected = true
            }
            if actionfilter[indexPath.section] == false{
                cellimage.filterView.isSelected = false
            }else{
                cellimage.filterView.isSelected = true
            }
            cellimage.delegate = self as TextfieldCellDelegate
       
            cellimage.actionText = { (text) in
                
                
                if self.textdata.count > 0{
                    
                    
                    
                    if ( self.textdata.count != indexPath.section && (self.textdata.index(where:{$0 == self.textdata[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.textdata[indexPath.section] = text
                        self.datatype[indexPath.section] = "Image"
                        if self.insertCell.count < self.actionrequired.count{
                            self.actionrequired.removeLast()
                            self.actionfilter.removeLast()
                        }
                        
                        
                    }else{
                        self.textdata.append(text)
                        self.datatype.append("Image")
                        self.actionrequired.append(false)
                        self.actionfilter.append(false)
                    }
                    
                    
                }else {
                    self.textdata.append(text)
                    self.datatype.append("Image")
                    self.actionrequired.append(false)
                    self.actionfilter.append(false)
                }
                
                print(self.textdata)
                
            }
            cellimage.actionRequired = {(Bool) in
                
                if self.actionrequired.count > 0{
                    if ( self.actionrequired.count != indexPath.section && (self.actionrequired.index(where:{$0 == self.actionrequired[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionrequired[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionrequired.append(Bool)
                    }
                    
                    
                }else{
                    self.actionrequired.append(Bool)
                    
                }
                print(self.actionrequired)
            }
            cellimage.actionFilter = {(Bool) in
                
                if self.actionfilter.count > 0{
                    
                    if ( self.actionfilter.count != indexPath.section && (self.actionfilter.index(where:{$0 == self.actionfilter[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.actionfilter[indexPath.section] = Bool
                        
                        
                    }else{
                        self.actionfilter.append(Bool)
                    }
                    
                    
                }else{
                    self.actionfilter.append(Bool)
                    
                }
            }
            return cellimage
        }
        return UITableViewCell()
    }
    
}

extension NewFormView: gettextdelegate {
    func sendToTitle(_ string: String) {
        self.titleText = string
        
    }
    
    func sendTodes(_ string: String) {
       self.DescriptionText = string
        
    }
    
}
extension UITableView{
    
    func setDynamicRowHeight(withEstimatedHeight height: CGFloat){
        self.estimatedRowHeight = height
        self.rowHeight = UITableView.automaticDimension
        
        
    }
    
}
extension   NewFormView:TextfieldCellDelegate{

    func turnonoffrequired(_ required: Bool) {
        
    }
    
    func turnonofffilter(_ filter: Bool) {
        
    }
    
    func deleteTapped(_ cell: UITableViewCell) {

    }
    
    
}

