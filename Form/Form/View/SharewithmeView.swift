//
//  SharewithmeView.swift
//  Form
//
//  Created by MSU IT CS on 18/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class SharewithmeView: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    var token:String? = "57820805bc50eeab31af548698c717118731ee2b"
    var users = [UsersModel]()
    var username:String?
    var form_id:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        setNev(backimage: "close-1")
        
        let editBt = UIButton()
        editBt.setImage(UIImage(named: "edit"), for: .normal)
        editBt.backgroundColor = UIColor.white
        editBt.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        editBt.addTarget(self, action: #selector(EditBtn), for: .touchUpInside)
        editBt.layer.cornerRadius = editBt.frame.height/2
        editBt.clipsToBounds = true
        let item = UIBarButtonItem(customView: editBt)
        
        self.navigationItem.rightBarButtonItem = item
        
         self.navigationItem.title = "Share People"
    }
    
    @objc func EditBtn(){
        self.performSegue(withIdentifier: "editsharesegue", sender: self)
    }
    
    func setHeaderView() -> Void {
        let headerview = ShareHeaderView.instanceFromNib()
        headerview.setUpView()
        headerview.setNeedsLayout()
        headerview.layoutIfNeeded()
        
        
        tableview.parallaxHeader.view = headerview
        tableview.parallaxHeader.height = 150
        tableview.parallaxHeader.minimumHeight = 0
        tableview.parallaxHeader.mode = .centerFill
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editsharesegue"{
            let next = segue.destination as! EditShareView
            next.form_id = self.form_id
            next.token = self.token
            
            
        }
    }
    
    
    
    

}
extension SharewithmeView:UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        let currentCell = tableView.cellForRow(at: indexPath!) as! ShareTableCell
        
       username = currentCell.user!
        tableview.reloadData()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SearchBarView.instanceFromNib()
        view.setUpView()
        view.searchbar.placeholder = "Username"
        view.ResultAnswer.isHidden = true
        view.avBtn.isHidden = true
        view.shareLabel.isHidden = false
        view.shareLabel.text = "Share with \(self.users.count) people"
        
        if username != nil{
             view.searchbar.text = username
        }
        view.tmp = {
            
            let link = URL(string:"\(url!)/forms/\(self.form_id!)/shares")
            
            ShareForm.post(link!,username: view.searchbar.text!, token: self.token!){
                (result,error) in
                
                alert_successView.instance.showAlert(title: "Share to \(self.username!) Success", message: "", alertType: .success)
                alert_successView.instance.tmp = {
                    self.navigationController?.popViewController(animated: true)
                    
                }
             
            }
            
            
            
        }
        view.searchbar.delegate = self
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ShareTableCell
        
//        if users[indexPath.row].image != nil{
//
//            let raw = "\(url!)\(users[indexPath.row].image!)"
//            let Nsurl = NSURL(string: raw)!
//
//            if let data = try? Data(contentsOf: Nsurl as URL)
//            {
//
//                cell.imageview.image = UIImage(data:data)
//
//            }
//        }else{
//            cell.imageview.image = UIImage(named: "Group35")
//        }
//
//
        cell.user = users[indexPath.row].username
        cell.nameLabel.text = users[indexPath.row].name
        cell.EmailLabel.text = users[indexPath.row].email
        return cell
    }
    
    
    
}
extension SharewithmeView:UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        
        let link = URL(string:"\(url!)/users")
        
        GetUsers.get(link!,username: searchBar.text!, token: token!){
            (result,error) in
            
            self.users = (result?.users)!
            
           self.tableview.reloadData()
        }
        
        
        
    }
    
}
