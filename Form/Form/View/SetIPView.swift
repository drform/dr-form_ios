//
//  SetIPView.swift
//  Form
//
//  Created by MSU IT CS on 26/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class SetIPView: UIViewController {

    @IBOutlet weak var addIPBtn: UIButton!
    @IBOutlet weak var iptext: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        iptext.keyboardType = UIKeyboardType.decimalPad

        // Do any additional setup after loading the view.
    }
    
    @IBAction func addIP(_ sender: Any) {
        
        url = URL(string: "http://\(iptext.text!):3000")
        self.performSegue(withIdentifier: "Logosegue", sender: self)

        
    }
    

}
