//
//  ProfileView.swift
//  Form
//
//  Created by MSU IT CS on 1/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields
import Photos
import MobileCoreServices

class ProfileView: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var token:String?
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    var image:UIImage?
    var user_id:Int?
    var base64String:String?
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var usernametext: MDCTextField!
    @IBOutlet weak var positiontext: MDCTextField!
    @IBOutlet weak var teltext: MDCTextField!
    @IBOutlet weak var nametext: MDCTextField!
    @IBOutlet weak var emailtext: MDCTextField!
    @IBOutlet weak var confirmBtn: UIButton!
    
    var UsernameTextController : MDCTextInputControllerOutlined?
    var positiontextController:MDCTextInputControllerOutlined?
    var teltextController:MDCTextInputControllerOutlined?
    var nametextController:MDCTextInputControllerOutlined?
    var emailtextController:MDCTextInputControllerOutlined?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        usernametext.text = username
        emailtext.text = email
        nametext.text = name
        teltext.text = tel
        positiontext.text = position
        imageview.image = image
        usernametext.isUserInteractionEnabled = false
        UsernameTextController = MDCTextInputControllerOutlined(textInput: usernametext)
        UsernameTextController?.floatingPlaceholderScale = 0.99
        positiontextController = MDCTextInputControllerOutlined(textInput: positiontext)
        positiontextController?.floatingPlaceholderScale = 0.99
        teltextController = MDCTextInputControllerOutlined(textInput: teltext)
        teltextController?.floatingPlaceholderScale = 0.99
        nametextController = MDCTextInputControllerOutlined(textInput: nametext)
        nametextController?.floatingPlaceholderScale = 0.99
        emailtextController = MDCTextInputControllerOutlined(textInput: emailtext)
        emailtextController?.floatingPlaceholderScale = 0.99
        
        imageview.layer.cornerRadius = imageview.frame.height/2
        imageview.clipsToBounds = true
        
        confirmBtn.layer.cornerRadius = 24
        confirmBtn.clipsToBounds = true
        confirmBtn.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        
        
        confirmBtn.addTarget(self, action: #selector(confirmEdit), for: .touchUpInside)
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "arrow-left")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "arrow-left")
        
        if image != nil{
            base64String = convertImageTobase64(format: .jpeg(1.0), image: image!)!
            
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.tintColor = UIColor.black

    }
    
    @objc func confirmEdit(){
       
        let link = URL(string: "\(url!)/users")

        UpdateProfile.update(link!, name: nametext.text!, tel: teltext.text!, email: emailtext.text!, image: base64String!, position: positiontext.text!, oldpassword: nil,newpassword: nil, token: token!){(result,error) in
            
            print(result?.message)
            if result?.message == "Update successfully."{
                
                alert_successView.instance.showAlert(title: "Update Profile Success", message: "", alertType: .success)
                alert_successView.instance.tmp = {
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
            
        }
        
    }
    
    @IBAction func importimage(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source",message: "Choose a source ", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            
            
            image.sourceType = .camera
            image.mediaTypes = [kUTTypeImage as String]
            image.allowsEditing = false
            
            self.present(image ,animated: true,completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Libraby", style: .default, handler: { (action:UIAlertAction) in
             UINavigationBar.appearance().tintColor = UIColor.black
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            image.allowsEditing = false
            self.present(image ,animated: true)
            
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil ))
        
        self.present(actionSheet , animated: true ,completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            base64String = convertImageTobase64(format: .jpeg(1.0), image: image)!
            
            
            // print("++++++++++\(base64String)")
          imageview.image = image
            
        }
        
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    public enum ImageFormat {
        case png
        case jpeg(CGFloat)
    }
    func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = image.pngData()
        case .jpeg(let compression): imageData = image.jpegData(compressionQuality: compression)
        }
        return imageData?.base64EncodedString()
    }

}
