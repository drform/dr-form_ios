//
//  LogInView.swift
//  Form
//
//  Created by MSU IT CS on 30/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import Alamofire
import MaterialComponents.MaterialTextFields

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

var url = URL(string:"http://api.mome.riseplus.tech")

class LogInView: UIViewController {

    var token:String? = nil
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    var image:String?
    var mapper:ModelUserInfo?
    var info:ModelUserInfo?
    var UsernameTextController : MDCTextInputControllerOutlined?
    var PasswordTextController:MDCTextInputControllerOutlined?
    @IBOutlet weak var UsernameText: MDCTextField!
    @IBOutlet weak var PasswordText: MDCTextField!
    @IBOutlet weak var SigninBtnView : UIButton?
    @IBOutlet weak var SignuoBtnView : UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.hideKeyboardWhenTappedAround()
        
      UsernameTextController = MDCTextInputControllerOutlined(textInput: UsernameText)
       
        UsernameTextController?.roundedCorners = UIRectCorner(rawValue: 24)
        UsernameTextController?.roundedCorners = .allCorners
        UsernameTextController?.floatingPlaceholderScale = 0.99
        
        
      PasswordTextController = MDCTextInputControllerOutlined(textInput: PasswordText)
        
        PasswordTextController?.floatingPlaceholderScale = 0.99
        
        SigninBtnView?.layer.cornerRadius = 24
        SigninBtnView?.clipsToBounds = true
        
        SigninBtnView?.layer.shadowColor = UIColor.blue.cgColor
        SigninBtnView?.layer.shadowOpacity = 0.8
        SigninBtnView?.layer.shadowRadius = 4
        SigninBtnView?.layer.shadowOffset = CGSize(width: 0, height: 0)

        // Do any additional setup after loading the view.
    
    }

    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.gettoken() != nil{
            
            self.performSegue(withIdentifier: "FormView", sender: self)
            
        }
    }
    @IBAction func SigninBtn(_ sender: Any) {
        
        let username:String? = UsernameText.text
        let password:String? = PasswordText.text
        let link = URL(string:"\(url!)/login")
        
        checkLogin.check(link!, username: username!, password: password!){(result,error) in
            self.token = result?.token
            self.info = result?.userinfo
         // self.mapper = info
            
            if self.token != nil{
                
                self.performSegue(withIdentifier: "FormView", sender: self)

                UserDefaults.standard.savetoken(token: self.token!)
                print("token : \(self.token!)")
               

            }else{
                alert_successView.instance.showAlert(title: "Username/Password incorrect", message: "", alertType: .failure)
                UIApplication.shared.statusBarStyle = .lightContent
            }
        }
     //
    }
  
   
    @IBAction func SignupBtn(_ sender: Any) {
         self.performSegue(withIdentifier: "SingUpView", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FormView"{
            
            let next = segue.destination as! ContenerView
            next.token = self.token
            next.username = self.info?.username
            next.name = self.info?.name
            next.email = self.info?.email
            next.position = self.info?.position
            next.tel = self.info?.tel
            next.image = self.info?.image
            next.role = self.info?.role
            next.user_id = self.info?.user_id
            
            if self.info?.username != nil{
                
            UserDefaults.standard.saveinfo(username: self.info?.username, name: self.info?.name, email: self.info?.email, position: self.info?.position, tel: self.info?.tel, image: self.info?.image, role: self.info?.role, user_id: self.info?.user_id)
                
            }
            
        }
        
    }
    
    
}
