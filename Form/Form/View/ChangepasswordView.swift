//
//  ChangepasswordView.swift
//  Form
//
//  Created by MSU IT CS on 19/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class ChangepasswordView: UIViewController {

    @IBOutlet weak var oldpasswordText: MDCTextField!
    @IBOutlet weak var newpasswordText: MDCTextField!
    @IBOutlet weak var renewpasswordText: MDCTextField!
    @IBOutlet weak var confirmBtn: UIButton!
    
    var oldpasswordTextController : MDCTextInputControllerOutlined?
    var newpasswordTextController : MDCTextInputControllerOutlined?
    var renewpasswordTextController : MDCTextInputControllerOutlined?
    var token:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        
        oldpasswordTextController = MDCTextInputControllerOutlined(textInput: oldpasswordText)
        newpasswordTextController = MDCTextInputControllerOutlined(textInput: newpasswordText)
        renewpasswordTextController = MDCTextInputControllerOutlined(textInput: renewpasswordText)
        
        oldpasswordTextController?.floatingPlaceholderScale = 0.99
        newpasswordTextController?.floatingPlaceholderScale = 0.99
        renewpasswordTextController?.floatingPlaceholderScale = 0.99
        
        confirmBtn.layer.cornerRadius = 24
        confirmBtn.clipsToBounds = true
        confirmBtn.backgroundColor = UIColor(red:0.05, green:0.10, blue:0.25, alpha:1.0)
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "arrow-left")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "arrow-left")
        
        confirmBtn.addTarget(self, action: #selector(confirmEdit), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.tintColor = UIColor.black

    }
    @objc func confirmEdit(){
    
        if newpasswordText.text == renewpasswordText.text
        {
        let link = URL(string: "\(url!)/users")
        
        UpdateProfile.update(link!, name: nil, tel: nil, email: nil, image:nil, position: nil, oldpassword: oldpasswordText.text!, newpassword: renewpasswordText.text!, token: token!){(result,error) in
            
            print(result?.message)
            
            if result?.message == "Update password successfully."{
                
                alert_successView.instance.showAlert(title: "Change password Success", message: "", alertType: .success)
                alert_successView.instance.tmp = {
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }else{
                
                alert_successView.instance.showAlert(title: "Incorrect password", message: "", alertType: .failure)
                }
            
            }
    
        }else{
            
            alert_successView.instance.showAlert(title: "password no match", message: "", alertType: .failure)
        }
    
    }
    

}
