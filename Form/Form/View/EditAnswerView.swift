//
//  EditAnswerView.swift
//  Form
//
//  Created by MSU IT CS on 17/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import Photos
import BSImagePicker
import AVKit
import AVFoundation
import MobileCoreServices
import ImagePicker
import Alamofire

struct answerchoice_id {
  
    var id = [Int]()
}


class EditAnswerView: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    var isfile:Bool = false
    var PhotoArray = [photoarray]()
    var numbersection:Int? = 0
    var SelectedAssets = [selectedassets]()
    var imagepicker : ImagePickerController?
    var deleteidimage = [Int]()
    var token:String?
    var form_id:Int?
    var user_form_id:Int?
    var user_item_id:Int?
    var answercell = [Int]()
    var answervalues = [String]()
    var images = [UIImage]()
    var rowimage = [didRow]()
    var allimages = [UIImage]()
    var allidimages = [Int]()
    var imageapi = [UIImage]()
    var idimageapi = [Int]()
    var sectionfile:Int?
    var namefile:String?
    var file:Data?
    var imageanswers = [imageanswer]()
    var group_id:Int?
    var questionitems = [FormsQuestionModel]()
    var questionarray = [Int]()
    var values = [String]()
    var choices = [DropdownModel]()
    var choicesCheckBox = [choicecheckbox]()
    var rows = [Int]()
    var base64String = [String]()
    var namefiles = [String]()
    var files = [Data]()
    
    var getimage:UIImage?
    var valuesAlls = [valuesall]()
    var checklist = [Bool]()
    var answerchoiceid = [answerchoice_id]()
    var answerModel = [FormsAnswerModel]()
    var somedateString:String?
    
    var dateText:String?
    var stringText:String?
    var textText:String?
    var intText:String?
    var floatText:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        self.hideKeyboardWhenTappedAround()
        self.navigationItem.title = "Edit Answer"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        regisNib()

        let link = URL(string:"\(url!)/form_items/\(form_id!)")
        GetQuestion.get(link!, token: token!){
            (result,error) in
            
            self.questionitems = (result?.questionitems)!
            var j:Int? = 0
            for tmp in self.questionitems{
                
                self.namefiles.append("")
                self.files.append(Data())
                self.questionarray.append(tmp.data_type_id!)
                self.values.append("")
                self.choicesCheckBox.append(choicecheckbox(no: j!,bool: [false], value_id: [0]))
                self.SelectedAssets.append(selectedassets(phasset: [PHAsset()]))
                self.rowimage.append(didRow(type: "\(tmp.data_type!)", rows: [0],choices: [""],image: [UIImage()]))
                self.PhotoArray.append(photoarray(image: [UIImage()]))
                
                
                if (self.questionarray[j!] == 9){
                    for sub in tmp.dropdown{
                        
                        self.choices.append(sub)
                        
                        
                        
                    }
                }else if (self.questionarray[j!] == 7){
                    
                    for sub in tmp.dropdown{
                        self.choicesCheckBox[j!].bool.append(false); self.choicesCheckBox[j!].value_id.append(sub.id!)
                    }
                    
                    self.choicesCheckBox[j!].bool.removeFirst()
                    self.choicesCheckBox[j!].value_id.removeFirst()
                }
                
                self.PhotoArray[j!].image.removeFirst()
                self.rowimage[j!].choices.removeFirst()
                self.SelectedAssets[j!].phasset.removeFirst()

                j = j! + 1
            }
            
            self.rows = [0]
            var i:Int? = 0
            while (i!<self.choicesCheckBox.count){
                
                self.rows.append(1)
                
                
                i = i! + 1
            }
            var a:Int? = 0
            for sub in self.answerModel{
                
                self.answerchoiceid.append(answerchoice_id(id: [0]))
                
                if sub.data_type_id == 7 {
                    var b:Int? = 0
                     var x:Int? = 0
                    
                    while b! < self.questionitems[a!].dropdown.count{
                        
                       
                        if sub.answer != nil{
//                            while x!<sub.answer.count{
//                                self.answerchoiceid.append(answerchoice_id(id: [0]))
//
//                                //self.answerchoiceid[a!].id.append(sub.answer[x!].choice_id!)
//                                self.answerchoiceid[a!].id[x!] = sub.answer[x!].choice_id!
//                                x = x! + 1
//                            }
//                            if x!>=sub.answer.count{
//                                self.answerchoiceid.append(answerchoice_id(id: [0]))
//
//                               //  self.answerchoiceid[a!].id.append(-1)
//                                self.answerchoiceid[a!].id[x!] = -1
//
//                            }
//
                            if x! < sub.answer.count{
                            if self.questionitems[a!].dropdown[b!].id == sub.answer[x!].choice_id{
                                
                                
                                self.answerchoiceid[a!].id.append(sub.answer[x!].choice_id!)
                                // self.answerchoiceid[a!].id[b!] = sub.answer[x!].choice_id!
                                 x = x! + 1
                            }
                            else{
                                 self.answerchoiceid[a!].id.append(-1)
                                }
                            }else{
                                self.answerchoiceid[a!].id.append(-1)

                                
                              
                            }
                            
                        }
                        
                        
                        b = b! + 1
                    }
                    
                }
                
                
                self.answerchoiceid[a!].id.removeFirst()
                a = a! + 1
                
            }
            
            self.tableview.reloadData()
            
        }
     
    
      
        let createBt = UIButton()
        createBt.setImage(UIImage(named: "edit-1"), for: .normal)
        createBt.backgroundColor = UIColor.white
        createBt.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        createBt.addTarget(self, action: #selector(saveEdit), for: .touchUpInside)
        createBt.layer.cornerRadius = createBt.frame.height/2
        createBt.clipsToBounds = true
        let item = UIBarButtonItem(customView: createBt)
        
        
        self.navigationItem.rightBarButtonItem = item
    }
    override func viewDidAppear(_ animated: Bool) {
//        for tmp in questionitems{
//
//            if tmp.data_type_id == 10 && SelectedAssets.count != 0 {
//            self.perform(#selector(EditAnswerView.getAllImages), with: nil, afterDelay: 0.5)
//            }
//        }
        self.perform(#selector(EditAnswerView.getAllImages), with: nil, afterDelay: 0.5)

    }
    @objc func saveEdit(){
        
        
        var tmp = [String:AnyObject]()
        var array = [Any]()
        var a:Int = 0
        var newimage = [String]()
        
        allimages.forEach { (image) in
            if a < imageapi.count {
                if image != imageapi[a] {
                    //new
                    newimage.append(convertImageTobase64(format: .jpeg(1.0), image: image)!)
                }
            }else {
                //new
                newimage.append(convertImageTobase64(format: .jpeg(1.0), image: image)!)

            }
            a = a + 1
        }
        
        
        var i:Int? = 0
        while i!<values.count {
            
            if (questionitems[i!].data_type_id != 7 && questionitems[i!].data_type_id != 10  && questionitems[i!].data_type_id != 8){
                tmp["order_no"] = i!+1 as AnyObject
                tmp["value"] = values[i!] as AnyObject
                
                array.append(tmp)
                
                i = i!+1
                
            }else if (questionitems[i!].data_type_id == 10){
                
                
                
                tmp["order_no"] = i!+1 as AnyObject
             
                if newimage.count != 0 {
                    tmp["value"] = newimage as AnyObject
                }else{
                    tmp["value"] = [] as AnyObject
                }
                
                array.append(tmp)
                
                i = i! + 1
            }else if questionitems[i!].data_type_id == 8{
                isfile = true
                self.namefile = self.namefiles[i!]
                self.file = self.files[i!]
                tmp["order_no"] = i!+1 as AnyObject
                 let link = URL(string: "\(url!)/forms/\(form_id!)/user_forms/\(user_form_id!)")
                Services.shared.services(with: tmp, url: link!, method: .put, isFile: isfile, file: self.file, namefile: self.namefile, token: self.token!){(result,error) in
//                    if error == nil{
//                        alert_successView.instance.showAlert(title: "Make a Questionnaire Success", message: "", alertType: .success)
//                        alert_successView.instance.tmp = {
//                            self.navigationController?.popViewController(animated: true)
//                        }
//                    }
                }
                 i = i! + 1
            }
            else{
                var j:Int? = 0
                while (j!<questionitems[i!].dropdown.count){
                    tmp["order_no"] = i!+1 as AnyObject
                    tmp["value"] = self.choicesCheckBox[i!].value_id[j!] as AnyObject
                    tmp["boolean"] = self.choicesCheckBox[i!].bool[j!] as AnyObject
                    array.append(tmp)
                    j = j! + 1
                }
                i = i! + 1
            }
            
        }
        
        if self.deleteidimage.count != 0{
            
            let link = URL(string: "\(url!)/image")
            var imagedelete = [String:AnyObject]()
            var array = [Any]()
            
            for tmp in self.deleteidimage{
                imagedelete["user_item_id"] = self.user_item_id as AnyObject
                imagedelete["image_id"] = tmp as AnyObject
                array.append(imagedelete)
            }
            
            Deleteimage.delete(link!, token: self.token!, images: array){(result,error) in
            }
            
        }
        
        
        let link = URL(string: "\(url!)/forms/\(form_id!)/user_forms/\(user_form_id!)")
        
       if isfile == false{
        UpdateAnswer.update(link!, group_id: self.group_id!, token: self.token!, user_form_item: array){
            (result,error) in
            
            if result?.message == "Update Successfully."{
                alert_successView.instance.showAlert(title: "Update Successfully.", message: "", alertType: .success)
                alert_successView.instance.tmp = {
            
                     self.navigationController?.popViewController(animated: true)
                }
            }
         

            
            
        }
    }
        
        
        
        
        
        
    
    }
    func regisNib(){
        
        tableview.register(UINib(nibName: "QuestionTypeTextCell", bundle: Bundle.main), forCellReuseIdentifier: "TextCell")
        tableview.register(UINib(nibName: "QuestionTypeStringCell", bundle: Bundle.main), forCellReuseIdentifier: "StringCell")
        tableview.register(UINib(nibName: "QuestionTypeIntCell", bundle: Bundle.main), forCellReuseIdentifier: "IntCell")
        tableview.register(UINib(nibName: "QuestionTypeFloatCell", bundle: Bundle.main), forCellReuseIdentifier: "FloatCell")
        tableview.register(UINib(nibName: "QuestionTypeDateCell", bundle: Bundle.main), forCellReuseIdentifier: "DateCell")
        tableview.register(UINib(nibName: "QuestionTypeImageCell", bundle: Bundle.main), forCellReuseIdentifier: "ImageCell")
        tableview.register(UINib(nibName: "QuestionTypeCheckBoxCell", bundle: Bundle.main), forCellReuseIdentifier: "CheckBoxCell")
        tableview.register(UINib(nibName: "QuestionTypeDropdownCell", bundle: Bundle.main), forCellReuseIdentifier: "DropdownCell")
        tableview.register(UINib(nibName: "QuestionTypeFileCell", bundle: Bundle.main), forCellReuseIdentifier: "filecell")
        tableview.register(UINib(nibName: "ShowMultipleImageCell", bundle: Bundle.main), forCellReuseIdentifier: "ShowMultipleImageCell")
        tableview.register(UINib(nibName: "HeaderCheckBoxCell", bundle: Bundle.main), forCellReuseIdentifier: "HeaderCheckBoxCell")
        self.tableview.setDynamicRowHeight(withEstimatedHeight: 200.0)
    }

    var a:Int? = 0
    
    @objc func getAllImages() -> Void {
        if SelectedAssets.count != 0{
            if SelectedAssets[numbersection!].phasset.count != 0{
                if a != 0{
                    self.PhotoArray[numbersection!].image.removeAll()
                }
                for i in 0..<rowimage[numbersection!].rows.count - 1{
                    
                    let manager = PHImageManager.default()
                    let option = PHImageRequestOptions()
                    var thumbnil = UIImage()
                    
                    
                    option.isSynchronous = true
                    
                    manager.requestImage(for: SelectedAssets[numbersection!].phasset[i], targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: option, resultHandler: {(resulf,info) -> Void in
                        thumbnil = resulf!
                        
                        //                    self.base64String.append(self.convertImageTobase64(format: .jpeg(1.0), image: thumbnil)!)
                        self.rowimage[self.numbersection!].choices.append(self.convertImageTobase64(format: .jpeg(1.0), image: thumbnil)!)
                        
                    })
                    
                    //                let data = UIImage.jpegData(thumbnil)
                    //                let newImage = UIImage()
                    self.PhotoArray[numbersection!].image.append(thumbnil)
                    a = 1
                }
                print("++++++++++++++")
                
                
            }
            
            tableview.reloadData()
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            
            
            base64String.append(convertImageTobase64(format: .jpeg(1.0), image: image)!)
            values = base64String
            
            getimage = image
            
            self.tableview.reloadData()
            
            
            // print("++++++++++\(base64String)")
            
            
        }
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    public enum ImageFormat {
        case png
        case jpeg(CGFloat)
    }
    func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = image.pngData()
        case .jpeg(let compression): imageData = image.jpegData(compressionQuality: compression)
        }
        return imageData?.base64EncodedString()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
  
   
    




    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (questionitems[section].data_type == "Checkbox")
        {
            return  questionitems[section].dropdown.count+1
            
        }else if questionitems[section].data_type == "Image"{
            
            
            if allimages.count == 0{
                return 1
            }else{
                return 2
            }
            
        }
        else{
            return 1
            
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return questionarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        switch questionarray[indexPath.section] {
            
        case 1:
            let celldate = tableView.dequeueReusableCell(withIdentifier: "DateCell",for:indexPath) as! QuestionTypeDateCell
            
            celldate.backgroundColor = .white
            celldate.answerText.backgroundColor = .white
            celldate.answerText.textColor = .gray
            celldate.answerText.layer.borderWidth = 0.25
            celldate.answerText.layer.cornerRadius = 10
            celldate.answerText.clipsToBounds = true
            celldate.questionLabel.textColor = .black
            
          //  celldate.answerText.text = answerModel[indexPath.section].answer[indexPath.row].value
            
            
            if answerModel[indexPath.section].answer[indexPath.row].value != nil{
                let myDateString = answerModel[indexPath.section].answer[indexPath.row].value
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let date = dateFormatter.date(from: myDateString!)
                dateFormatter.dateFormat = "dd MMMM yyyy"
                somedateString = dateFormatter.string(from: date!)
                dateText = somedateString
                celldate.answerText.text = dateText
                self.values[indexPath.section] = answerModel[indexPath.section].answer[indexPath.row].value!
            }
            
            
            celldate.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            
            
            
            celldate.actionText = {(text) in
                
                if self.questionarray.count < self.values.count{
                    self.values.removeLast()
                }
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        
                        
                        
                    }else{
                        self.values.append(text)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                }
                
                
                print(self.values.count)
            }
            
            return celldate
            
        case 2:
            
            
            
            return UITableViewCell()
            
        case 3:
            let cellstring = tableView.dequeueReusableCell(withIdentifier: "StringCell",for:indexPath) as! QuestionTypeStringCell
            cellstring.backgroundColor = .white
            cellstring.answerText.backgroundColor = .white
            cellstring.answerText.textColor = .gray
            cellstring.answerText.layer.borderWidth = 0.25
            cellstring.answerText.layer.cornerRadius = 10
            cellstring.answerText.clipsToBounds = true
            stringText = answerModel[indexPath.section].answer[indexPath.row].value
            cellstring.answerText.text = stringText
            
            if answerModel[indexPath.section].answer[indexPath.row].value != nil{
                 self.values[indexPath.section] = answerModel[indexPath.section].answer[indexPath.row].value!
            }
            
            cellstring.questionLabel.textColor = .black
            
            
            cellstring.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            
            
            cellstring.actionText = {(text) in
                
                
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        
                        
                        
                    }else{
                        self.values.append(text)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                }
                
                
                print(self.values.count)
            }
            
            return cellstring
        case 4:
            let celltext = tableView.dequeueReusableCell(withIdentifier: "TextCell",for: indexPath) as! QuestionTypeTextCell
            
            celltext.backgroundColor = .white
            celltext.answerText.backgroundColor = .white
            celltext.answerText.textColor = .gray
            celltext.questionLabel.textColor = .black
            
            celltext.answerText.layer.borderWidth = 0.25
            celltext.answerText.layer.cornerRadius = 10
            celltext.answerText.clipsToBounds = true
            textText = answerModel[indexPath.section].answer[indexPath.row].value
            celltext.answerText.text = textText
            
            if answerModel[indexPath.section].answer[indexPath.row].value != nil{
                self.values[indexPath.section] = answerModel[indexPath.section].answer[indexPath.row].value!
            }
            celltext.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            
            
            print(questionarray)
            
            
            celltext.actionText = {(text) in
                
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        
                        
                        
                    }else{
                        self.values.append(text)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                }
                
                
                print(self.values.count)
            }
            
            
            return celltext
            
        case 5:
            let cellint = tableView.dequeueReusableCell(withIdentifier: "IntCell",for: indexPath) as! QuestionTypeIntCell
            cellint.backgroundColor = .white
            cellint.answerText.backgroundColor = .white
            cellint.answerText.textColor = .gray
            cellint.answerText.layer.borderWidth = 0.25
            cellint.answerText.layer.cornerRadius = 10
            cellint.answerText.clipsToBounds = true
            cellint.questionLabel.textColor = .black
            
            cellint.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            intText = answerModel[indexPath.section].answer[indexPath.row].value
            cellint.answerText.text = intText
            
            if answerModel[indexPath.section].answer[indexPath.row].value != nil{
                self.values[indexPath.section] = answerModel[indexPath.section].answer[indexPath.row].value!
            }
            
            cellint.actionText = {(text) in
                
                if self.questionarray.count < self.values.count{
                    self.values.removeLast()
                }
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        
                        
                        
                    }else{
                        self.values.append(text)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                }
                
                
                print(self.values.count)
            }
            
            return cellint
        case 6:
            let cellfloat = tableView.dequeueReusableCell(withIdentifier: "FloatCell",for:indexPath) as! QuestionTypeFloatCell
            cellfloat.backgroundColor = .white
            cellfloat.answerText.backgroundColor = .white
            cellfloat.answerText.textColor = .gray
            cellfloat.answerText.layer.borderWidth = 0.25
            cellfloat.answerText.layer.cornerRadius = 10
            cellfloat.answerText.clipsToBounds = true
            floatText = answerModel[indexPath.section].answer[indexPath.row].value
            cellfloat.answerText.text = floatText
            
            if answerModel[indexPath.section].answer[indexPath.row].value != nil{
                self.values[indexPath.section] = answerModel[indexPath.section].answer[indexPath.row].value!
            }
            
            cellfloat.questionLabel.textColor = .black
            cellfloat.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            
            
            cellfloat.actionText = {(text) in
                
                if self.questionarray.count < self.values.count{
                    self.values.removeLast()
                }
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        
                        
                        
                    }else{
                        self.values.append(text)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                }
                
                
                print(self.values.count)
            }
            
            return cellfloat
            
        case 7:
            
            
            switch indexPath.row{
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCheckBoxCell", for: indexPath) as! HeaderCheckBoxCell
                
                cell.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
                
                return cell
                
            default:
                
                let cellcheckbox  = tableView.dequeueReusableCell(withIdentifier: "CheckBoxCell", for: indexPath) as! QuestionTypeCheckBoxCell
                
                                checklist.append(false)
                
                
                cellcheckbox.optionLabel.text = questionitems[indexPath.section].dropdown[indexPath.row - 1].dropdown_value
             
               if questionitems[indexPath.section].dropdown[indexPath.row - 1].id == answerchoiceid[indexPath.section].id[indexPath.row - 1]{

                        cellcheckbox.checkBoxview.backgroundColor = UIColor(red:0.11, green:0.62, blue:0.76, alpha:1.0)
                        cellcheckbox.checkBoxview.isSelected = true
                self.choicesCheckBox[indexPath.section].bool[indexPath.row - 1] = true

               }
//
                cellcheckbox.tmp = {
                    
                    let checkbox = cellcheckbox.checkBoxview
                    
                    
                    if ((checkbox?.isSelected)!){
                        checkbox?.isSelected = false
                        checkbox?.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
                        
                        
                        self.choicesCheckBox[indexPath.section].bool[indexPath.row - 1] = false
                        
                    
                        
                        
                        
                    }else{
                        checkbox?.backgroundColor = UIColor(red:0.11, green:0.62, blue:0.76, alpha:1.0)
                        checkbox?.isSelected = true
                        
                        //  self.checklist[indexPath.row-1] = true
                        
                        self.checklist[indexPath.row-1] = true
                        self.choicesCheckBox[indexPath.section].bool[indexPath.row - 1] = true
                        //
                        
                        // self.valueCheckbox[indexPath.row-1].value? = str
                        //        self.valuesAll[indexPath.row-1].Bool = true
                        
                        
                        
                    }
                    
                    
                }
                return cellcheckbox
            }
            
        case 8:
            
            let cellfile = tableView.dequeueReusableCell(withIdentifier: "filecell", for: indexPath) as! QuestionTypeFileCell
            
            cellfile.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
          
            if namefiles[indexPath.section] != ""{
                
                cellfile.nameFileLabel.text = namefiles[indexPath.section]
                cellfile.importfileView.setImage(UIImage(named: "file"), for: .normal)
                cellfile.importfileView.titleLabel?.text = " "
                
            }else{
                cellfile.importfileView.setImage(UIImage(named: "file"), for: .normal)
                let fileURL = URL(string: answerModel[indexPath.section].answer[indexPath.row].value!)
                cellfile.nameFileLabel.text = fileURL?.lastPathComponent
                
            }
            cellfile.tmp = {
                self.sectionfile = indexPath.section
                let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String], in: .import)
                documentPicker.delegate = self
                documentPicker.allowsMultipleSelection = false
                
                self.present(documentPicker, animated: true, completion: nil)
            }
            
            
            return cellfile
            
            
            
            
            
            
        case 10:
            switch indexPath.row{
            case 0:
            let cellimage = tableView.dequeueReusableCell(withIdentifier: "ImageCell",for:indexPath) as! QuestionTypeImageCell
           
            
            
            cellimage.backgroundColor = .white
            cellimage.questionLabel.textColor = .black
            cellimage.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            
            cellimage.tmp = {
            
                self.imagepicker = ImagePickerController()
                self.imagepicker!.delegate = self
                self.present(self.imagepicker!, animated: true, completion: nil)
                
            }
            
            
            return cellimage
                
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ShowMultipleImageCell", for: indexPath) as! ShowMultipleImageCell
                
                
                if allimages.count != 0 {
                    //   cell.imageView?.image = PhotoArray[indexPath.section].image[indexPath.row - 1]
                    var x:Int = 0
                     cell.allimage = self.allimages
                   // cell.allimage = self.imageanswers
                 //   cell.imageanswers = self.imageanswers
                    
                  cell.awakeFromNib()
                    cell.collectionview.reloadData()
                }
                
                cell.cancelbtstatus = 1
                cell.PhotoArray = self.PhotoArray
                cell.rowimage = self.rowimage
                cell.SelectedAssets = self.SelectedAssets
                
                cell.questionLabel.isHidden = true
                
                
                cell.actionPhotoArray = {(PhotoArray) in
                    
                    self.PhotoArray = PhotoArray
                }
                cell.actionrowimage = {(rowimage) in
                    self.rowimage = rowimage
                }
                cell.actionSelectedAssets = {(SelectedAssets) in
                    self.SelectedAssets = SelectedAssets
                }
                cell.actionindeximage = {(idimage) in
                    
                    self.deleteidimage.append(self.allidimages[idimage])
                    self.allidimages.remove(at: idimage)
                    self.allimages.remove(at: idimage)
                    
                }
                return cell
                
        }
            
        case 9:
            let celldropdown = tableview.dequeueReusableCell(withIdentifier: "DropdownCell", for: indexPath) as! QuestionTypeDropdownCell
            
            celldropdown.questionText.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            
            var item = [Int]()
            var dataSource = [String]()
            celldropdown.dropdownbtn.setTitleColor(UIColor.gray, for: UIControl.State.normal)
            
            for tmp in choices{
                if self.questionitems[indexPath.section].id == tmp.form_item_id{
                    item.append(tmp.id!)
                    dataSource.append(tmp.dropdown_value!)
                }
            }
            celldropdown.dropdown.dataSource = dataSource
            
            
            if answerModel[indexPath.section].data_type_id == 9{
                if answerModel[indexPath.section].answer.count != 0{
                if answerModel[indexPath.section].answer[indexPath.row].value != nil{
                celldropdown.dropdownbtn.setTitle(answerModel[indexPath.section].answer[indexPath.row].value, for: .normal)
                    let id:Int =  dataSource.index(where:{$0 == answerModel[indexPath.section].answer[indexPath.row].value!})!
                    
                    let values_id = item[id]
                    
                    self.values[indexPath.section] = String(describing: values_id)
                    
              //  self.values[indexPath.section] = answerModel[indexPath.section].answer[indexPath.row].value!
                    }
                }
            }
            
            celldropdown.actionText = {(text) in
                
                let id:Int =  dataSource.index(where:{$0 == text})!
                
                let values_id = item[id]
                
                self.values[indexPath.section] = String(describing: values_id)
                
                
            }
            return celldropdown
        default:
            return UITableViewCell()
        }
        
        
        
    }
    
    
    
 }

extension EditAnswerView:ImagePickerDelegate{
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else {return}
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        self.allimages += images
        self.allidimages.append(0)
        
        imagePicker.dismiss(animated: true){
            self.tableview.reloadData()

            DispatchQueue.main.async {
                
            }
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
extension EditAnswerView: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print("URL: \(urls)")
        
        
        tableview.reloadData()
        
        let fileURL = urls[0]
        
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                let url = URL(fileURLWithPath: fileURL.path)
                
                if let data = try? Data(contentsOf: url) {
                    print("Success")
                    
                    self.files[sectionfile!] = data
                    self.namefiles[sectionfile!] = fileURL.lastPathComponent
                    
                    
                    //                    Alamofire.upload(self.file!, to: "https://httpbin.org/post").responseJSON { response in
                    //                        debugPrint(response)
                    //                    }
                    
                    
                    
                }else {
                    print("NOT FOUNC")
                }
                
            } catch {
                print("Error")
            }
        }
        
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("Nothing selected")
        
        
    }
}
