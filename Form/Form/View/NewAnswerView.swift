//
//  NewAnswerView.swift
//  Form
//
//  Created by MSU IT CS on 22/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import Photos
import BSImagePicker
import AVKit
import AVFoundation
import MobileCoreServices
import  Alamofire
import ImagePicker
import ObjectMapper
struct valuesall {
    var order_no:Int?
    var Bool:Bool?
    var value:Int?
}
struct param {
    var order_no:Int?
    var namefile:String?
    var value:String?
    var file:Data?
}
struct choicecheckbox {
    var no:Int?
    var bool = [Bool]()
    var value_id = [Int]()
}
struct photoarray {
    var image = [UIImage]()
}
struct selectedassets {
    var phasset = [PHAsset]()
}
class NewAnswerView: UIViewController,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
  
    var imagearray = [String]()
    var user_form = [Any]()
    var params = [String:AnyObject]()
    var namefile:String?
    var imagepicker : ImagePickerController?
    var createanswermodel:CreateAnswerModel?
    var isDropdown:Bool = false
    var file:Data?
    var isImage:Bool = false
    var token:String = "cc5cd7d952d7f62e7b1f2c072218369f634478bf"
    var form_id:Int = 106
    var image:UIImage?
    var nametitle:String?
    var des:String?
    var rowimage = [didRow]()
    var questionitems = [FormsQuestionModel]()
    var questionarray = [Int]()
    var values = [String]()
    var getimage:UIImage?
    var base64String = [String]()
    var options = [String]()
    var groups_id:Int? = 1
    var SelectedAssets = [selectedassets]()
    var PhotoArray = [photoarray]()
    var numbersection:Int? = 0
    var choices = [DropdownModel]()
    var nochoice = [Int]()
    var choiceCheckbox = [choicecheckbox]()
    var valuesAlls = [valuesall]()
    var rows = [Int]()
    var checklist = [Bool]()
    var valus = [String:AnyObject]()
    var namefiles = [String]()
    var files = [Data]()
    var sectionfile:Int?
    var allimage = [UIImage]()
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var headerview: UIView!
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        self.perform(#selector(NewAnswerView.getAllImages), with: nil, afterDelay: 0.5)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.separatorStyle = .none

        titleLabel.text = nametitle
        imageview.image = image
        descriptionLabel.text = des
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        self.tableview.delegate = self
        self.tableview.dataSource = self
        
        imageview.layer.cornerRadius = 10
        imageview.clipsToBounds = true
        
        headerview.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
        
        let link = URL(string:"\(url!)/form_items/\(form_id)")
        GetQuestion.get(link!, token: token){
            (result,error) in
          
            self.questionitems = (result?.questionitems)!
            var j:Int? = 0
            for tmp in self.questionitems{
                
                self.namefiles.append("")
                self.files.append(Data())
                self.questionarray.append(tmp.data_type_id!)
                self.values.append("")
                self.SelectedAssets.append(selectedassets(phasset: [PHAsset()]))
                self.rowimage.append(didRow(type: "\(tmp.data_type!)", rows: [0],choices: [""],image: [UIImage()]))
                self.PhotoArray.append(photoarray(image: [UIImage()]))
                self.choiceCheckbox.append(choicecheckbox(no: j!,bool: [false], value_id: [0]))
                
                if (self.questionarray[j!] == 9){
                for sub in tmp.dropdown{
                    
                   self.choices.append(sub)
                  
                    
                }
                }else if (self.questionarray[j!] == 7){
                    self.nochoice.append(j!)
                    for sub in tmp.dropdown{
                        
                        self.choiceCheckbox[j!].bool.append(false); self.choiceCheckbox[j!].value_id.append(sub.id!)

                    }
                    self.choiceCheckbox[j!].bool.removeFirst()
                    self.choiceCheckbox[j!].value_id.removeFirst()
                    
                }else if (self.questionarray[j!] == 10){
                    
                }else if (self.questionarray[j!] == 8){
                    self.isImage = true
                }
                
                self.SelectedAssets[j!].phasset.removeFirst()
                self.PhotoArray[j!].image.removeFirst()
                self.rowimage[j!].choices.removeFirst()
                j = j! + 1
            }
         
          self.rows = [0]
            var i:Int? = 0
            while (i!<self.choiceCheckbox.count){
                
                self.rows.append(1)
                
                
                i = i! + 1
            }
    
            
       
           self.tableview.reloadData()
            
        }
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
        

        let createBt = UIButton()
        createBt.setImage(UIImage(named: "edit-1"), for: .normal)
        createBt.backgroundColor = UIColor.white
        createBt.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        createBt.addTarget(self, action: #selector(createAnswer), for: .touchUpInside)
        createBt.layer.cornerRadius = createBt.frame.height/2
        createBt.clipsToBounds = true
        let item = UIBarButtonItem(customView: createBt)
        
        
        self.navigationItem.rightBarButtonItem = item

        tableview.register(UINib(nibName: "QuestionTypeTextCell", bundle: Bundle.main), forCellReuseIdentifier: "TextCell")
        tableview.register(UINib(nibName: "QuestionTypeStringCell", bundle: Bundle.main), forCellReuseIdentifier: "StringCell")
        tableview.register(UINib(nibName: "QuestionTypeIntCell", bundle: Bundle.main), forCellReuseIdentifier: "IntCell")
        tableview.register(UINib(nibName: "QuestionTypeFloatCell", bundle: Bundle.main), forCellReuseIdentifier: "FloatCell")
        tableview.register(UINib(nibName: "QuestionTypeDateCell", bundle: Bundle.main), forCellReuseIdentifier: "DateCell")
        tableview.register(UINib(nibName: "QuestionTypeImageCell", bundle: Bundle.main), forCellReuseIdentifier: "ImageCell")
        tableview.register(UINib(nibName: "QuestionTypeCheckBoxCell", bundle: Bundle.main), forCellReuseIdentifier: "CheckBoxCell")
        tableview.register(UINib(nibName: "QuestionTypeDropdownCell", bundle: Bundle.main), forCellReuseIdentifier: "DropdownCell")
             tableview.register(UINib(nibName: "QuestionTypeFileCell", bundle: Bundle.main), forCellReuseIdentifier: "filecell")
           tableview.register(UINib(nibName: "ShowMultipleImageCell", bundle: Bundle.main), forCellReuseIdentifier: "ShowMultipleImageCell")
        tableview.register(UINib(nibName: "HeaderCheckBoxCell", bundle: Bundle.main), forCellReuseIdentifier: "HeaderCheckBoxCell")
        
        self.tableview.setDynamicRowHeight(withEstimatedHeight: 1000.0)

        self.navigationItem.title = "New Answer"

        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

        
    }
    
  
    @objc func keyBoardWillShow(notification: Notification){
        
        if let userinfo = notification.userInfo as? Dictionary<String,AnyObject>{
            let frame = userinfo[UIResponder.keyboardFrameEndUserInfoKey]
            let keyBoardRect = frame?.cgRectValue
            if (keyBoardRect?.height as? Float) != nil{
              
            }
        }
        
    }
    @objc func keyBoardWillHide(notification: Notification){
        
    }
   

    
    @objc func createAnswer(){
        
        var tmp = [String:AnyObject]()
        var array = [Any]()
        var isfile:Bool = false
        var a:Int? = 1
        for i in allimage{
            imagearray.append(convertImageTobase64(format: .jpeg(1.0), image: i)!)
        }
        let link = URL(string: "\(url!)/forms/\(form_id)/user_forms")
       
        if isImage == true {
            //create&update
        
        
        if questionitems[0].data_type_id != 7 && questionitems[0].data_type_id != 10 && questionitems[0].data_type_id != 8{
        params["value"] = values[0] as AnyObject
        params["order_no"] = 1 as AnyObject
        }else {
            if (questionitems[0].data_type_id == 10){
                params["order_no"] = 1 as AnyObject
                if imagearray.count != 0 {
                    params["value"] = imagearray as AnyObject
                }else{
                    params["value"] = [] as AnyObject
                }
            }else if (self.questionitems[0].data_type_id == 8)
            {
                self.namefile = self.namefiles[0]
                self.file = self.files[0]
                isfile = true
                params["order_no"] = 1 as AnyObject
            }
            else{
                var j:Int? = 0
                while (j!<questionitems[0].dropdown.count){
                    params["order_no"] = 1 as AnyObject
                    params["value"] = self.choiceCheckbox[0].value_id[j!] as AnyObject
                    params["boolean"] = self.choiceCheckbox[0].bool[j!] as AnyObject
                   
                    j = j! + 1
                }
                
            }
        }
        
            if isfile == true{
                
                CreateAnswerModel.createanswermodel(with: params, url: link!, isFile: isfile, file: file, namefile: namefile, token: token) { (result, error) in
                    
                    self.createanswermodel = result
                    while a! < self.questionitems.count{
                        isfile = false
                        self.namefile = nil
                        self.file = nil
                        self.isDropdown = false
                        
                        self.params.removeValue(forKey: "boolean")
                        if (self.questionitems[a!].data_type_id != 7 && self.questionitems[a!].data_type_id != 10 && self.questionitems[a!].data_type_id != 8){
                            //                    multipartFormData.append("\(i!+1)".data(using: String.Encoding.utf8)!, withName: "user_form_item[][order_no]")
                            //                    multipartFormData.append("\(self.values[i!])".data(using: String.Encoding.utf8)!, withName: "user_form_item[][value]")
                            print("normal type")
                            self.isDropdown = false
                            
                            self.params["order_no"] = a!+1 as AnyObject
                            self.params["value"] = self.values[a!] as AnyObject
                            
                            
                            
                            a = a! + 1
                        }else if (self.questionitems[a!].data_type_id == 10){
                            
                            self.isDropdown = true
                            
                            self.params["order_no"] = a!+1 as AnyObject
                            
                            if self.imagearray.count != 0 {
                                self.params["value"] = self.imagearray as AnyObject
                            }else{
                                self.params["value"] = [] as AnyObject
                            }
                            array.append(self.params)

                            let link1 = URL(string: "\(url!)/forms/\(self.form_id)/user_forms/\(self.createanswermodel!.data!.id!)")

                            UpdateAnswer.update(link1!, group_id: nil, token: self.token, user_form_item: array){
                                (result,error) in
                                
                             
                                
                                
                                
                            }
                            
                            a = a! + 1
                        }else if (self.questionitems[a!].data_type_id == 8)
                        {
                            self.isDropdown = false
                            
                            self.namefile = self.namefiles[a!]
                            self.file = self.files[a!]
                            isfile = true
                            self.params["order_no"] = a!+1 as AnyObject
                            self.params.removeValue(forKey: "value")
                            
                            
                            // tmp["value"] = self.file as AnyObject
                            
                            
                            
                            
                            
                            a = a! + 1
                        }
                        else{
                            var j:Int? = 0
                            self.isDropdown = true
                            let link1 = URL(string: "\(url!)/forms/\(self.form_id)/user_forms/\(self.createanswermodel!.data!.id!)")
                            while (j!<self.questionitems[a!].dropdown.count){
                                self.params["order_no"] = a!+1 as AnyObject
                                self.params["value"] = self.choiceCheckbox[a!].value_id[j!] as AnyObject
                                self.params["boolean"] = self.choiceCheckbox[a!].bool[j!] as AnyObject
                                
                                Services.shared.services(with: self.params, url: link1!, method: .put, isFile: isfile, file: self.file, namefile: self.namefile, token: self.token){(result,error) in
                                    
                                }
                                
                                j = j! + 1
                            }
                            a = a! + 1
                            
                        }
                        
                        // let link1 = URL(string:"\(url!)/form_items/\(self.createanswermodel!.data!.id!)")
                        let link1 = URL(string: "\(url!)/forms/\(self.form_id)/user_forms/\(self.createanswermodel!.data!.id!)")
                        
                        if isfile == true && self.isDropdown == false{
                            
                            Services.shared.services(with: self.params, url: link1!, method: .put, isFile: isfile, file: self.file, namefile: self.namefile, token: self.token){(result,error) in
                                if error == nil{
                                    alert_successView.instance.showAlert(title: "Make a Questionnaire Success", message: "", alertType: .success)
                                    alert_successView.instance.tmp = {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }else if isfile == false && self.isDropdown == false{
                            
                            Services.shared.services(with: self.params, url: link1!, method: .put, isFile: isfile, file: self.file, namefile: self.namefile, token: self.token){(result,error) in
                                if error == nil{
                                    alert_successView.instance.showAlert(title: "Make a Questionnaire Success", message: "", alertType: .success)
                                    alert_successView.instance.tmp = {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
            }else{
                
                CreateAnswerModel.createanswermodel(with: params, url: link!, isFile: isfile, file: file, namefile: namefile, token: token) { (result, error) in
                    
                    self.createanswermodel = result
                    while a! < self.questionitems.count{
                        isfile = false
                        self.namefile = nil
                        self.file = nil
                        self.isDropdown = false
                        
                        self.params.removeValue(forKey: "boolean")
                        if (self.questionitems[a!].data_type_id != 7 && self.questionitems[a!].data_type_id != 10 && self.questionitems[a!].data_type_id != 8){
                            //                    multipartFormData.append("\(i!+1)".data(using: String.Encoding.utf8)!, withName: "user_form_item[][order_no]")
                            //                    multipartFormData.append("\(self.values[i!])".data(using: String.Encoding.utf8)!, withName: "user_form_item[][value]")
                            print("normal type")
                            self.isDropdown = false
                            
                            self.params["order_no"] = a!+1 as AnyObject
                            self.params["value"] = self.values[a!] as AnyObject
                            
                            
                            
                            a = a! + 1
                        }else if (self.questionitems[a!].data_type_id == 10){
                            
                            self.isDropdown = true
                            
                            self.params["order_no"] = a!+1 as AnyObject
                            
                            if self.imagearray.count != 0 {
                                self.params["value"] = self.imagearray as AnyObject
                            }else{
                                self.params["value"] = [] as AnyObject
                            }
                            array.append(self.params)
                            
                            let link1 = URL(string: "\(url!)/forms/\(self.form_id)/user_forms/\(self.createanswermodel!.data!.id!)")
                            
                            UpdateAnswer.update(link1!, group_id: nil, token: self.token, user_form_item: array){
                                (result,error) in
                                
                                
                                
                                
                                
                            }
                            
                            a = a! + 1
                        }else if (self.questionitems[a!].data_type_id == 8)
                        {
                            self.isDropdown = false
                            
                            self.namefile = self.namefiles[a!]
                            self.file = self.files[a!]
                            isfile = true
                            self.params["order_no"] = a!+1 as AnyObject
                            self.params.removeValue(forKey: "value")
                            
                            
                            // tmp["value"] = self.file as AnyObject
                            
                            
                            
                            
                            
                            a = a! + 1
                        }
                        else{
                            var j:Int? = 0
                            self.isDropdown = true
                            let link1 = URL(string: "\(url!)/forms/\(self.form_id)/user_forms/\(self.createanswermodel!.data!.id!)")
                            while (j!<self.questionitems[a!].dropdown.count){
                                self.params["order_no"] = a!+1 as AnyObject
                                self.params["value"] = self.choiceCheckbox[a!].value_id[j!] as AnyObject
                                self.params["boolean"] = self.choiceCheckbox[a!].bool[j!] as AnyObject
                                
                                Services.shared.services(with: self.params, url: link1!, method: .put, isFile: isfile, file: self.file, namefile: self.namefile, token: self.token){(result,error) in
                                    
                                }
                                
                                j = j! + 1
                            }
                            a = a! + 1
                            
                        }
                        
                        // let link1 = URL(string:"\(url!)/form_items/\(self.createanswermodel!.data!.id!)")
                        let link1 = URL(string: "\(url!)/forms/\(self.form_id)/user_forms/\(self.createanswermodel!.data!.id!)")
                        
                        if isfile == true && self.isDropdown == false{
                            
                            Services.shared.services(with: self.params, url: link1!, method: .put, isFile: isfile, file: self.file, namefile: self.namefile, token: self.token){(result,error) in
                                if error == nil{
                                    alert_successView.instance.showAlert(title: "Make a Questionnaire Success", message: "", alertType: .success)
                                    alert_successView.instance.tmp = {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }else if isfile == false && self.isDropdown == false{
                            
                            Services.shared.services(with: self.params, url: link1!, method: .put, isFile: isfile, file: self.file, namefile: self.namefile, token: self.token){(result,error) in
                                if error == nil{
                                    alert_successView.instance.showAlert(title: "Make a Questionnaire Success", message: "", alertType: .success)
                                    alert_successView.instance.tmp = {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
            }
        }else{
            //create
              var i:Int? = 0
            tmp.removeAll()
              while i!<questionitems.count {
                tmp.removeValue(forKey: "boolean")
            if (questionitems[i!].data_type_id != 7 && questionitems[i!].data_type_id != 10 && questionitems[i!].data_type_id != 8){
                tmp["order_no"] = i!+1 as AnyObject
                tmp["value"] = values[i!] as AnyObject
                
                array.append(tmp)
                
                i = i!+1
                
            }else if (questionitems[i!].data_type_id == 10){
                
                
                tmp["order_no"] = i!+1 as AnyObject
                if imagearray.count != 0 {
                    tmp["value"] = imagearray as AnyObject
                }else{
                    tmp["value"] = [] as AnyObject
                }
                
                array.append(tmp)
                
                i = i! + 1
            }else if (questionitems[i!].data_type_id == 8)
            {
                tmp["order_no"] = i!+1 as AnyObject
                // tmp["value"] = self.file as AnyObject
                
                
                
                array.append(tmp)
                
                i = i! + 1
                
            }
            else{
                var j:Int? = 0
                while (j!<questionitems[i!].dropdown.count){
                    tmp["order_no"] = i!+1 as AnyObject
                    tmp["value"] = self.choiceCheckbox[i!].value_id[j!] as AnyObject
                    tmp["boolean"] = self.choiceCheckbox[i!].bool[j!] as AnyObject
                    array.append(tmp)
                    j = j! + 1
                }
                
                i = i! + 1
            }
            
        }
            
        }
       
    
        if isImage == true{
            
        }
        else if isImage == false{
            
            createanswer.create(link!,group_id:groups_id!, token:token, isImage: isImage, user_form_item: array){
                (result,error) in
                
                if result != nil{
                    
                    alert_successView.instance.showAlert(title: "Make a Questionnaire Success", message: "", alertType: .success)
                    alert_successView.instance.tmp = {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }else{
                    alert_successView.instance.showAlert(title: "Make a Questionnaire Error", message: "Some thing wrong", alertType: .failure)
                }
                
            }
        }
  

//    Services.shared.services(with: tmp, url: link!
//        , method: .post, setKey: "user_form_item", setValue: "value", isMultipleImage: false, images: [self.file!], token: token!, isActivity: false) { (result, error) in
//
////
////
//       }
        
    }
    //TABLEVIEW
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.white
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (questionitems[section].data_type == "Checkbox")
        {
           return  questionitems[section].dropdown.count+1
            
        }else if questionitems[section].data_type == "Image"
        {
            
            if allimage.count == 0{
                return 1
            }else{
                return 2
            }
            
            
            
        }else
        {
            return 1
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return questionarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        switch questionarray[indexPath.section] {
            
        case 1:
            let celldate = tableView.dequeueReusableCell(withIdentifier: "DateCell",for:indexPath) as! QuestionTypeDateCell
            
            celldate.backgroundColor = .white
            celldate.answerText.backgroundColor = .white
            celldate.answerText.textColor = .gray
            celldate.answerText.layer.borderWidth = 0.25
            celldate.answerText.layer.cornerRadius = 10
            celldate.answerText.clipsToBounds = true
            celldate.questionLabel.textColor = .black

            celldate.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            celldate.answerText.text = self.values[indexPath.section]

            
            
            celldate.actionText = {(text) in
                
                if self.questionarray.count < self.values.count{
                    self.values.removeLast()
                }
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                       
                        
                        
                    }else{
                        self.values.append(text)
                       
                    }
                    
                    
                }else {
                    self.values.append(text)
                    
                }
                
              
                print(self.values.count)
            }
            
            return celldate
            
        case 2:
            
            
            
            return UITableViewCell()
            
        case 3:
            let cellstring = tableView.dequeueReusableCell(withIdentifier: "StringCell",for:indexPath) as! QuestionTypeStringCell
            cellstring.backgroundColor = .white
            cellstring.answerText.backgroundColor = .white
            cellstring.answerText.textColor = .gray
            cellstring.answerText.layer.borderWidth = 0.25
            cellstring.answerText.layer.cornerRadius = 10
            cellstring.answerText.clipsToBounds = true
            
            cellstring.questionLabel.textColor = .black
            cellstring.answerText.text = self.values[indexPath.section]
            cellstring.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            

            cellstring.actionText = {(text) in
                
                
             
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        

                        
                        
                    }else{
                        self.values.append(text)
                       
                    }
                    
                    
                }else {
                    self.values.append(text)
                   
                }
                
               
                print(self.values.count)
            }
            
            return cellstring
        case 4:
            let celltext = tableView.dequeueReusableCell(withIdentifier: "TextCell",for: indexPath) as! QuestionTypeTextCell
            
            celltext.backgroundColor = .white
            celltext.answerText.backgroundColor = .white
            celltext.answerText.textColor = .gray
            celltext.questionLabel.textColor = .black
            celltext.answerText.layer.borderWidth = 0.25
            celltext.answerText.layer.cornerRadius = 10
            celltext.answerText.clipsToBounds = true
            
            celltext.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            celltext.answerText.text = self.values[indexPath.section]


            print(questionarray)
            

            celltext.actionText = {(text) in
                
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                       
                        
                        
                    }else{
                        self.values.append(text)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                }
                
              
                print(self.values.count)
            }
            
            
            return celltext
            
        case 5:
            let cellint = tableView.dequeueReusableCell(withIdentifier: "IntCell",for: indexPath) as! QuestionTypeIntCell
            cellint.backgroundColor = .white
            cellint.answerText.backgroundColor = .white
            cellint.answerText.textColor = .gray
            cellint.answerText.layer.borderWidth = 0.25
            cellint.answerText.layer.cornerRadius = 10
            cellint.answerText.clipsToBounds = true
            cellint.questionLabel.textColor = .black
            cellint.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            cellint.answerText.text = self.values[indexPath.section]


            
            cellint.actionText = {(text) in
                
                if self.questionarray.count < self.values.count{
                    self.values.removeLast()
                }
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        
                        
                        
                    }else{
                        self.values.append(text)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                }
                
                
                print(self.values.count)
            }
            
            return cellint
        case 6:
            let cellfloat = tableView.dequeueReusableCell(withIdentifier: "FloatCell",for:indexPath) as! QuestionTypeFloatCell
            cellfloat.backgroundColor = .white
            cellfloat.answerText.backgroundColor = .white
            cellfloat.answerText.textColor = .gray
            cellfloat.answerText.layer.borderWidth = 0.25
            cellfloat.answerText.layer.cornerRadius = 10
            cellfloat.answerText.clipsToBounds = true
            
            cellfloat.questionLabel.textColor = .black
            cellfloat.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            cellfloat.answerText.text = self.values[indexPath.section]

            
            cellfloat.actionText = {(text) in
                
                if self.questionarray.count < self.values.count{
                    self.values.removeLast()
                }
                
                if self.values.count > 0{
                    
                    
                    
                    if (self.values.count != indexPath.section && (self.values.index(where:{$0 == self.values[indexPath.section]})) != nil ){
                        
                        
                        print("ok")
                        self.values[indexPath.section] = text
                        
                        
                        
                    }else{
                        self.values.append(text)
                        
                    }
                    
                    
                }else {
                    self.values.append(text)
                }
                
               
                print(self.values.count)
            }
            
            return cellfloat
            
             case 7:
                
                 
                switch indexPath.row{
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCheckBoxCell", for: indexPath) as! HeaderCheckBoxCell
                   
                    cell.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
                    
                    return cell
                    
                default:
                    
                    let cellcheckbox  = tableView.dequeueReusableCell(withIdentifier: "CheckBoxCell", for: indexPath) as! QuestionTypeCheckBoxCell

                    checklist.append(false)
                  
                        
                        cellcheckbox.optionLabel.text = questionitems[indexPath.section].dropdown[indexPath.row - 1].dropdown_value
                    
                    
                    cellcheckbox.tmp = {
                        
                        let checkbox = cellcheckbox.checkBoxview
//
//
                        if ((checkbox?.isSelected)!){
                            checkbox?.isSelected = false
                            checkbox?.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
                            
                            
                            self.choiceCheckbox[indexPath.section].bool[indexPath.row - 1] = false

                            
                            
                            
                        }else{
                            checkbox?.backgroundColor = UIColor(red:0.11, green:0.62, blue:0.76, alpha:1.0)
                            checkbox?.isSelected = true
                            
                           self.checklist[indexPath.row-1] = true
                            self.choiceCheckbox[indexPath.section].bool[indexPath.row - 1] = true
                     
                            
                            
                        
                        }
                        
                    }
                    return cellcheckbox
                }
                
                
            case 8:
            
            
            let cellfile = tableView.dequeueReusableCell(withIdentifier: "filecell", for: indexPath) as! QuestionTypeFileCell
            
            cellfile.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            if namefiles != nil{
                
                cellfile.nameFileLabel.text = namefiles[indexPath.section]
                cellfile.importfileView.setImage(UIImage(named: "file"), for: .normal)
                cellfile.importfileView.titleLabel?.text = " "
              
            }
                cellfile.tmp = {
                    UINavigationBar.appearance().tintColor = UIColor.black
                    
                    self.sectionfile = indexPath.section
                let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String], in: .import)
                documentPicker.delegate = self
                documentPicker.allowsMultipleSelection = false
                
                self.present(documentPicker, animated: true, completion: nil)
            }
            
            
            return cellfile
            
            
            
        
            case 10:
                
                switch indexPath.row{
                case 0:
                
            let cellimage = tableView.dequeueReusableCell(withIdentifier: "ImageCell",for:indexPath) as! QuestionTypeImageCell
            
            
            //            self.myImageView.animationImages = self.PhotoArray
            //            self.myImageView.animationDuration = 3.0
            //            self.myImageView.startAnimating()
           
            
            cellimage.backgroundColor = .white
              cellimage.questionLabel.textColor = .black
            cellimage.questionLabel.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            
            cellimage.tmp = {
                
                self.imagepicker = ImagePickerController()
                self.imagepicker!.delegate = self
                self.present(self.imagepicker!, animated: true, completion: nil)
                

                
            }
            
            
            return cellimage
            
                    
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ShowMultipleImageCell", for: indexPath) as! ShowMultipleImageCell
                    
                    
                    if allimage.count != 0 {
                 //   cell.imageView?.image = PhotoArray[indexPath.section].image[indexPath.row - 1]
                        var x:Int = 0
                        cell.allimage = self.allimage

                        cell.collectionview.reloadData()
                    }
                    cell.cancelbtstatus = 1
//                    cell.PhotoArray = self.PhotoArray
//                    cell.rowimage = self.rowimage
//                    cell.SelectedAssets = self.SelectedAssets
//
                    cell.questionLabel.isHidden = true
//
//
//                    cell.actionPhotoArray = {(PhotoArray) in
//
//                        self.PhotoArray = PhotoArray
//                        }
//                    cell.actionrowimage = {(rowimage) in
//                        self.rowimage = rowimage
//                        }
//                    cell.actionSelectedAssets = {(SelectedAssets) in
//                                self.SelectedAssets = SelectedAssets
//                    }
                    cell.actionindeximage = {(index) in
                        
                        self.allimage.remove(at: index)
                        
                    }
                    return cell
                    
            
            }
          
            case 9:
                let celldropdown = tableview.dequeueReusableCell(withIdentifier: "DropdownCell", for: indexPath) as! QuestionTypeDropdownCell
                 
                  celldropdown.questionText.text = "\(indexPath.section+1).  \(questionitems[indexPath.section].title!)"
            
                var item = [Int]()
                var dataSource = [String]()
                celldropdown.dropdownbtn.setTitleColor(UIColor.gray, for: UIControl.State.normal)

                for tmp in choices{
                    if self.questionitems[indexPath.section].id == tmp.form_item_id{
                    item.append(tmp.id!)
                        dataSource.append(tmp.dropdown_value!)
                    }
                }
                    celldropdown.dropdown.dataSource = dataSource
                
                
                celldropdown.actionText = {(text) in
                    
                    let id:Int =  dataSource.index(where:{$0 == text})!
                    
                    let values_id = item[id]
                    
                    self.values[indexPath.section] = String(describing: values_id)
                    
                    
                }
            return celldropdown
            
            default:
            return UITableViewCell()
        }
        
        
      
    }
    //ENDTABLEVIEW
    var a:Int? = 0
    
    @objc func getAllImages() -> Void {
        
        if SelectedAssets.count != 0{
        if SelectedAssets[numbersection!].phasset.count != 0{
            if a != 0{
            self.PhotoArray[numbersection!].image.removeAll()
            }
            for i in 0..<rowimage[numbersection!].rows.count - 1{
                
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                var thumbnil = UIImage()
                
                
                option.isSynchronous = true
                
                manager.requestImage(for: SelectedAssets[numbersection!].phasset[i], targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: option, resultHandler: {(resulf,info) -> Void in
                    thumbnil = resulf!
                    
//                    self.base64String.append(self.convertImageTobase64(format: .jpeg(1.0), image: thumbnil)!)
                    self.rowimage[self.numbersection!].choices.append(self.convertImageTobase64(format: .jpeg(1.0), image: thumbnil)!)
                })
                
                //                let data = UIImage.jpegData(thumbnil)
                //                let newImage = UIImage()
                self.PhotoArray[numbersection!].image.append(thumbnil)
                a = 1
            }
            print("++++++++++++++")
            
            
        }
       
        tableview.reloadData()
        
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
           
           
            
            base64String.append(convertImageTobase64(format: .jpeg(1.0), image: image)!)
            values = base64String
            
            getimage = image
            
          self.tableview.reloadData()
            
            
            // print("++++++++++\(base64String)")
            
            
        }
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    public enum ImageFormat {
        case png
        case jpeg(CGFloat)
    }
    func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = image.pngData()
        case .jpeg(let compression): imageData = image.jpegData(compressionQuality: compression)
        }
        return imageData?.base64EncodedString()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NewAnswerView: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print("URL: \(urls)")
 
     
        tableview.reloadData()
        
        let fileURL = urls[0]
        
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                let url = URL(fileURLWithPath: fileURL.path)
                
                if let data = try? Data(contentsOf: url) {
                    print("Success")
                    
                    self.files[sectionfile!] = data
                    self.namefiles[sectionfile!] = fileURL.lastPathComponent
                    
                    
//                    Alamofire.upload(self.file!, to: "https://httpbin.org/post").responseJSON { response in
//                        debugPrint(response)
//                    }
                    
                  
                    
                }else {
                    print("NOT FOUNC")
                }
                
            } catch {
                print("Error")
            }
        }
        
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("Nothing selected")
        
        
    }
}
extension NewAnswerView:ImagePickerDelegate{
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else {return}
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
      allimage += images
        
        imagePicker.dismiss(animated: true){
            self.tableview.reloadData()
            
            DispatchQueue.main.async {
                
            }
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
}
