//
//  SideMenuView.swift
//  Form
//
//  Created by MSU IT CS on 10/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import DropDown

struct data {
    var open = Bool()
    var title = String()
    var image = UIImage()
    var expandable : [expandabled]?
}
struct expandabled {
    var title = String()
    var image = UIImage()
}
class SideMenuView: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var changepasswordBtn: UIButton!
    @IBOutlet weak var logoutbtn: UIButton!
    @IBOutlet weak var tableview: UITableView!
    
    var name:String?
    var image:String?
    var role:String?
    let dropdown = DropDown()
    var collapesdata = [data]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collapesdata = [
            data(open: false, title: "Profile", image: UIImage(named: "user")!, expandable: [expandabled(title: "Edit Profile", image: UIImage(named: "edit-2")!),expandabled(title: "Change Password", image: UIImage(named: "change")!)]),
            data(open: false, title: "Setting", image: UIImage(named: "settings")!, expandable: nil)
        ]
        tableview.separatorStyle = .none
        
        
        name = UserDefaults.standard.getname()
        image = UserDefaults.standard.getimage()
        role = UserDefaults.standard.getrole()
        
        
        nameLabel.text = name
        roleLabel.text = role
        // Do any additional setup after loading the view.
        imageview.layer.cornerRadius =  imageview.frame.height/2
        imageview.clipsToBounds = true
        
        roleLabel.layer.cornerRadius = 10
        roleLabel.clipsToBounds = true
        roleLabel.backgroundColor = UIColor(red:0.13, green:0.42, blue:0.25, alpha:1.0)
       
        tableview.dataSource = self
        tableview.delegate  = self

        tableview.register(UINib(nibName: "CollapsCell", bundle: Bundle.main), forCellReuseIdentifier: "CollapsCell")
        tableview.register(UINib(nibName: "ChildColladpsCell", bundle: Bundle.main), forCellReuseIdentifier: "ChildColladpsCell")
        logoutbtn.addTarget(self, action: #selector(logout), for: .touchUpInside)

        
        let raw = "\(url!)\(image!)"
        let  Nsurl = NSURL(string: raw )!
        
        if let data = try? Data(contentsOf: Nsurl as URL)
        {
            //  images.append(UIImage(data: data)!)
            imageview.image = UIImage(data:data)
            
        }
    }
    @objc func dropdownAction(){
        dropdown.show()
        
    }
    @objc func editprofile(){
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"),object:nil)
        
                       NotificationCenter.default.post(name: NSNotification.Name("profilesegue"),object:nil)
    }
    @objc func changepassword(){
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"),object:nil)
        NotificationCenter.default.post(name: NSNotification.Name("changepasswordsegue"),object:nil)
    }
    @objc func logout(){
            let alert = UIAlertController(title: "Are you sure?", message: "Log out", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                
                UserDefaults.standard.logout()
               
              
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginvc") as! LogInView
                   self.present(vc, animated: true, completion: nil)
                
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }}))
      
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("cancel")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
        
        
        
    }

}
extension SideMenuView:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if section == 0 {
            if collapesdata[section].open == true {
                if let ex = collapesdata[section].expandable {
                    return ex.count + 1
                }
            }
            return 1
        }
        return 1
    
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return collapesdata.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section)
        
        if collapesdata[indexPath.section].open == true {
            self.collapesdata[indexPath.section].open = false
            let indexPath = IndexSet(arrayLiteral: indexPath.section)
            tableView.reloadSections(indexPath, with: .none)
            
        }else {
            self.collapesdata[indexPath.section].open = true
            let indexPath = IndexSet(arrayLiteral: indexPath.section)
            tableView.reloadSections(indexPath, with: .none)
        }
        if indexPath.row == 1{
            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"),object:nil)
            NotificationCenter.default.post(name: NSNotification.Name("profilesegue"),object:nil)
        }else if indexPath.row == 2{
            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"),object:nil)
            NotificationCenter.default.post(name: NSNotification.Name("changepasswordsegue"),object:nil)
        
        }
//        if collapesdata[indexPath.section].expandable![indexPath.row ].title == "Edit Profile"{
//
//            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"),object:nil)
//            NotificationCenter.default.post(name: NSNotification.Name("profilesegue"),object:nil)
//
//        }else if collapesdata[indexPath.section].expandable![indexPath.row ].title == "Change Password"{
//
//            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"),object:nil)
//            NotificationCenter.default.post(name: NSNotification.Name("changepasswordsegue"),object:nil)
//
//        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollapsCell", for: indexPath) as! CollapsCell
            
            cell.titleLabel.text = collapesdata[indexPath.section].title
            cell.firstimageview.image = collapesdata[indexPath.section].image
            
            
            
            
            return cell
        }
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChildColladpsCell", for: indexPath) as! ChildColladpsCell
    
        
        cell.titleLabel.text = collapesdata[indexPath.section].expandable![indexPath.row-1].title
        cell.firstimageview.image = collapesdata[indexPath.section].expandable![indexPath.row-1].image
        
        return cell
        
        
    }
    
    
    
    
}
