//
//  MainFormView.swift
//  Form
//
//  Created by MSU IT CS on 30/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import SwiftEntryKit
import ParallaxHeader
import NVActivityIndicatorView

class MainFormView: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,NVActivityIndicatorViewable {
    
    var isloadform:Bool = true
    var getformdetailadmin = [GetFormDetail]()
    var getformdetail = [GetFormDetail]()
    var getformdetailnew = [GetFormDetail]()
    var getformdetailnew2 = [GetFormDetail]()
    var getformshare = [GetFormShare]()
    var dataforms = [GetFormDetail]()
    var formdetailimage = [UIImage]()
    var formdetailimage2 = [UIImage]()
     var token:String? = "feed5d13631b180fdc31d872b9fb077f8dc39a7e"
    var username:String?
    var titlegroup:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    var image:String?
    var image_group:UIImage?
    let toolbar = UIToolbar()
    let refreshControl = UIRefreshControl()
    var groups_id:Int? = 1
    var user_id:Int?
    var description_group:String?
    var typeview:Int = 1
    var offset:Int = 0
    var amount:Int?
    var role:String?
    var admin:Bool = false
    var arrSection = ["My form","Share with you"]

 

    @IBOutlet weak var myFormCollectionView: UICollectionView!
    
    func setHeaderView() -> Void {
        let headerview = MainFormheaderView.instanceFromNib()
        headerview.setUpView()
        headerview.setNeedsLayout()
        headerview.layoutIfNeeded()
        
        myFormCollectionView.parallaxHeader.view = headerview
        myFormCollectionView.parallaxHeader.height = 250
        myFormCollectionView.parallaxHeader.minimumHeight = 0
        myFormCollectionView.parallaxHeader.mode = .bottomFill
        
      //  headerview.view.bottomAnchor.constraint(equalTo: headerview.bottomAnchor, constant: -50).isActive = true
        
        if self.role == "User Admin"{
            headerview.firstBtn.setTitle("All form", for: .normal)
            headerview.secondBtn.setTitle("My form", for: .normal)
        }
        headerview.imageview.image = image_group
        headerview.titleLabel.text = titlegroup
        headerview.titleLabel.textColor = .black
        headerview.desLabel.text  = description_group
        
       
        headerview.tmp = {
            self.typeview = 1
            self.isloadform = true
            self.offset = 0
            
            let link = URL(string: "\(url!)/forms")
            if self.role == "User Admin"{
                self.admin = true
                GetFormDetail.get(link!, group_id: self.groups_id!,offset: self.offset, allform: self.admin, keyword: nil, token: self.token!){
                    (result,error)in
                    self.getformdetailadmin = result!
                    print(self.offset)
                    if self.getformdetailadmin != nil {
                        
                        for tmp in self.getformdetailadmin{
                            let raw = "\(url!)\(tmp.image!)"
                            let  Nsurl = NSURL(string: raw )!
                            
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                //  images.append(UIImage(data: data)!)
                                self.formdetailimage.append(UIImage(data:data)!)
                                //  cell.imageview.image = UIImage(data:data)
                                
                            }
                        }
                        
                        
                    }
                    self.myFormCollectionView.reloadData()
                    
                }
            }else{
                GetFormDetail.get(link!, group_id: self.groups_id!,offset: self.offset, allform: self.admin, keyword: nil, token: self.token!){
                    (result,error)in
                    self.getformdetail = result!
                    print(self.offset)
                    if self.getformdetail != nil {
                        
                        for tmp in self.getformdetail{
                            let raw = "\(url!)\(tmp.image!)"
                            let  Nsurl = NSURL(string: raw )!
                            
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                //  images.append(UIImage(data: data)!)
                                self.formdetailimage.append(UIImage(data:data)!)
                                //  cell.imageview.image = UIImage(data:data)
                                
                            }
                        }
                        
                        
                    }
                    self.myFormCollectionView.reloadData()
                    
                }
            }
        }
        headerview.tmp2 = {
            self.typeview = 2
            self.isloadform = true
             self.offset = 0
             
             let link = URL(string: "\(url!)/forms")
            if self.role == "User Admin"{
                GetFormDetail.get(link!, group_id: self.groups_id!,offset: self.offset, allform: false, keyword: nil, token: self.token!){
                    (result,error)in
                    if result != nil{
                        self.getformdetail = result!

                    }
                    print(self.offset)
                    if self.getformdetail != nil {
                        
                        for tmp in self.getformdetail{
                            let raw = "\(url!)\(tmp.image!)"
                            let  Nsurl = NSURL(string: raw )!
                            
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                //  images.append(UIImage(data: data)!)
                                self.formdetailimage2.append(UIImage(data:data)!)
                                //  cell.imageview.image = UIImage(data:data)
                                
                            }
                        }
                        
                        
                    }
                    self.myFormCollectionView.reloadData()
                    
                }
            }else {
                GetFormShare.get(link!, group_id: self.groups_id!, token: self.token!){
                    (result,error)in
                    if result != nil {
                        self.getformshare = result!
                    }
                    if self.getformshare != nil {
                        
                        for tmp in self.getformshare{
                            let raw = "\(url!)\(tmp.image!)"
                            let  Nsurl = NSURL(string: raw )!
                            
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                //  images.append(UIImage(data: data)!)
                                self.formdetailimage2.append(UIImage(data:data)!)
                                //  cell.imageview.image = UIImage(data:data)
                                
                            }
                        }
                        
                        
                    }
                    self.myFormCollectionView.reloadData()
                }
            }
            
        }
     
        
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
 //       print("Mainform Token : \(token!)")
        myFormCollectionView.delegate = self
        myFormCollectionView.dataSource = self
        setHeaderView()
        let link = URL(string: "\(url!)/forms")
        DispatchQueue.global(qos: .background).async {
            self.startAnimating()
            if self.role == "User Admin"{
                self.admin = true
                GetFormDetail.get(link!, group_id: self.groups_id!,offset: self.offset, allform: self.admin, keyword: nil, token: self.token!){
                    (result,error)in
                    if result != nil{
                        self.getformdetailadmin = result!

                    }
                    print(self.offset)
                    if self.getformdetailadmin != nil {
                        
                        for tmp in self.getformdetailadmin{
                            let raw = "\(url!)\(tmp.image!)"
                            let  Nsurl = NSURL(string: raw )!
                            
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                //  images.append(UIImage(data: data)!)
                                self.formdetailimage.append(UIImage(data:data)!)
                                //  cell.imageview.image = UIImage(data:data)
                                
                            }
                        }
                        
                        
                    }
                   
                    DispatchQueue.main.async {
                        // reload your collection view here:
                        
                        self.myFormCollectionView.reloadData()
                        self.stopAnimating()
                        
                    }
                }
            }else{
                GetFormDetail.get(link!, group_id: self.groups_id!,offset: self.offset, allform: self.admin, keyword: nil, token: self.token!){
                    (result,error)in
                    if result != nil{
                        self.getformdetail = result!
                        
                    }
                    print(self.offset)
                    if self.getformdetail != nil {
                        
                        for tmp in self.getformdetail{
                            let raw = "\(url!)\(tmp.image!)"
                            let  Nsurl = NSURL(string: raw )!
                            
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                //  images.append(UIImage(data: data)!)
                                self.formdetailimage.append(UIImage(data:data)!)
                                //  cell.imageview.image = UIImage(data:data)
                                
                            }
                        }
                        
                        
                    }
                    
                    DispatchQueue.main.async {
                        // reload your collection view here:
                        
                        self.myFormCollectionView.reloadData()
                        self.stopAnimating()
                        
                    }
                }
            }
          
        }
      
            
        
     
      
    
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        self.myFormCollectionView.refreshControl = refreshControl
        
       let createBtn = UIButton(type: .custom)
        createBtn.setImage(UIImage(named: "add-from"), for: .normal)
        //add function for button
        createBtn.addTarget(self, action: #selector(createpage), for: .touchUpInside)
        createBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        createBtn.clipsToBounds = true

        let barButton = UIBarButtonItem(customView: createBtn)
        //assign button to navigationbar
        
        let searchBtn = UIButton(type: .custom)
        searchBtn.setImage(UIImage(named: "search"), for: .normal)
        searchBtn.addTarget(self, action: #selector(searchform), for: .touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        searchBtn.clipsToBounds = true
        let barButton2 = UIBarButtonItem(customView: searchBtn)

        
        
        self.navigationItem.rightBarButtonItems = [barButton,barButton2]
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "arrow-left")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "arrow-left")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
     
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "arrow-left")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "arrow-left")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.black

       

       
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
       
    }
 
    
    
    
    @objc func createpage() {
    self.performSegue(withIdentifier: "newformviewsegue", sender: self)
        
        
    }
    @objc func searchform(){
        searchView.instance.show()
        searchView.instance.token = self.token
        searchView.instance.group_id = self.groups_id
        print("search")
        
        searchView.instance.dataForm = {(dataForm) in
            
            if dataForm != nil{
                
            self.dataforms = dataForm
            self.performSegue(withIdentifier: "searchformsegue", sender: self)
            }
            
        }

    }
    
    
    
    @objc func doSomething(refreshControl: UIRefreshControl) {
        print("Hello World!")
        offset = 0
        isloadform = true
        let link = URL(string: "\(url!)/forms")

//        GetFormDetail.get(link!,group_id: groups_id!,offset: self.offset, allform: self.admin, keyword: nil, token: token!){
//            (result,error)in
//            self.getformdetail = result!
//
//            self.myFormCollectionView.reloadData()
//
//        }
//        GetFormShare.get(link!,group_id: groups_id!,token: token!){
//            (result,error)in
//            self.getformshare = result!
//
//            self.myFormCollectionView.reloadData()
//        }
        
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline){
            refreshControl.endRefreshing()
        }
        // somewhere in your code you might need to call:
        
    }

    //COLLECTIONVIEW
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        DispatchQueue.global(qos: .background).async {
           
        
            if self.typeview == 1 && indexPath.row == self.getformdetailadmin.count - 1 && self.admin == true{
            
                if self.isloadform == true{
                
                    self.offset = self.offset + 6
                 self.startAnimating()
                let link = URL(string: "\(url!)/forms")
                
                    GetFormDetail.get(link!,group_id: self.groups_id!,offset: self.offset, allform: self.admin, keyword: nil, token: self.token!){
                    (result,error)in
                    
                    if result != nil {
                         self.getformdetailnew2 = result!
                    }else{
                        self.isloadform = false
                    }
                   
                    
                    if self.getformdetailnew2.count < 6 {
                        
                        self.isloadform = false
                        print(self.offset)
                        
                    }else if self.isloadform != false{
                        
                        var i:Int = 0
                        while i < (self.getformdetailnew2.count){
                            
                            self.getformdetailadmin.append(self.getformdetailnew2[i])
                            
                            let raw = "\(url!)\(self.getformdetailnew2[i].image!)"
                                    let  Nsurl = NSURL(string: raw )!
                                    
                                    if let data = try? Data(contentsOf: Nsurl as URL)
                                    {
                                        //  images.append(UIImage(data: data)!)
                                        self.formdetailimage.append(UIImage(data:data)!)
                                        //  cell.imageview.image = UIImage(data:data)
                                        
                                    }
                            
                                
                                
                            
                            i = i + 1
                            
                        }
                        
                    }
                        DispatchQueue.main.async {
                            // reload your collection view here:
                            
                            self.myFormCollectionView.reloadData()
                            self.stopAnimating()
                            
                        }
                    
                }
            }
                
          
                
            
            
        }else if self.typeview == 2 && indexPath.row == self.getformdetail.count - 1 && self.admin == true{
                if self.isloadform == true{
                
                    self.offset = self.offset + 6
                 self.startAnimating()
                let link = URL(string: "\(url!)/forms")
                
                    GetFormDetail.get(link!,group_id: self.groups_id!,offset: self.offset, allform: false, keyword: nil, token: self.token!){
                    (result,error)in
                    
                    
                    if result != nil {
                         self.getformdetailnew = result!
                    }else{
                        self.isloadform = false
                    }
                    
                    if self.getformdetailnew.count == 0 {
                        
                        self.isloadform = false
                        print(self.offset)
                        
                    }else if self.isloadform != false{
                        
                        var i:Int = 0
                        while i < (self.getformdetailnew.count){
                            
                            self.getformdetail.append(self.getformdetailnew[i])
                            
                            let raw = "\(url!)\(self.getformdetailnew[i].image!)"
                            let  Nsurl = NSURL(string: raw )!
                            
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                //  images.append(UIImage(data: data)!)
                                self.formdetailimage2.append(UIImage(data:data)!)
                                //  cell.imageview.image = UIImage(data:data)
                                
                            }
                            
                            
                            
                            
                            i = i + 1
                            
                        }
                        
                    }
                        DispatchQueue.main.async {
                            // reload your collection view here:
                            
                            self.myFormCollectionView.reloadData()
                            self.stopAnimating()
                            
                        }
                    
                }
            }
            
            
            
        }else if self.typeview == 1 && indexPath.row == self.getformdetail.count - 1 && self.admin == false{
                if self.isloadform == true{
                
                    self.offset = self.offset + 6
                 self.startAnimating()
                let link = URL(string: "\(url!)/forms")
                
                    GetFormDetail.get(link!,group_id: self.groups_id!,offset: self.offset, allform: false, keyword: nil, token: self.token!){
                    (result,error)in
                    
                   
                    if result != nil {
                        self.getformdetailnew = result!
                    }else{
                        self.isloadform = false
                    }
                    if self.getformdetailnew.count == 0 {
                        
                        self.isloadform = false
                        print(self.offset)
                        
                    }else if self.isloadform != false{
                        
                        var i:Int = 0
                        while i < (self.getformdetailnew.count){
                            
                            self.getformdetail.append(self.getformdetailnew[i])
                            
                            let raw = "\(url!)\(self.getformdetailnew[i].image!)"
                            let  Nsurl = NSURL(string: raw )!
                            
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                //  images.append(UIImage(data: data)!)
                                self.formdetailimage.append(UIImage(data:data)!)
                                //  cell.imageview.image = UIImage(data:data)
                                
                            }
                            
                            
                            
                            
                            i = i + 1
                            
                        }
                        
                    }
                        DispatchQueue.main.async {
                            // reload your collection view here:
                            
                            self.myFormCollectionView.reloadData()
                            self.stopAnimating()
                            
                        }
                    
                }
            }
            
            
        }
     }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reuse : UICollectionReusableView? = nil
        if(kind == UICollectionView.elementKindSectionHeader){
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "reuse", for: indexPath) as! ReusableView
            
            view.labelheader.text = arrSection[indexPath.section]
            reuse = view
            
        }
        return reuse!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        

     
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 2
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: size)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count:Int?
        if typeview == 1 && admin == true{
            count =  getformdetailadmin.count
        }else if typeview == 1 && admin == false{
            count = getformdetail.count
        }
        else if typeview == 2 && admin == false{
            count = getformshare.count
        }else if typeview == 2 && admin == true{
             count = getformdetail.count
        }
        
        return count!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myFormCell", for:  indexPath) as! MyFormCollectionViewCell
       
        
        if typeview == 1 && admin == true{
            
            cell.titleLabel.text = getformdetailadmin[indexPath.row].title
            cell.imageview.image = formdetailimage[indexPath.row]
//
        }else if typeview == 2 && admin == true{
        
            cell.titleLabel.text = getformdetail[indexPath.row].title
            cell.imageview.image = formdetailimage2[indexPath.row]
            //
            
            
        }else if typeview == 1 && admin == false{
            cell.titleLabel.text = getformdetail[indexPath.row].title
            cell.imageview.image = formdetailimage[indexPath.row]
            
        }
        else if typeview == 2 && admin == false{
            cell.titleLabel.text = getformshare[indexPath.row].title
             cell.imageview.image = formdetailimage2[indexPath.row]
        }
        return cell
        
    }
    
    // END COLLECTIONVIEW
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newformviewsegue"{
            let next = segue.destination as! NewFormView
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email
            next.position = self.position
            next.tel = self.tel
            next.groups_id = self.groups_id
            
        }else if segue.identifier == "profilesegue"{
            let next = segue.destination  as! ProfileView
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email
            next.position = self.position
            next.tel = self.tel
            
//            let raw = "\(url!)\(image ?? nil)"
//            let  Nsurl = NSURL(string: raw )!
//
//            if let data = try? Data(contentsOf: Nsurl as URL)
//            {
//              //  images.append(UIImage(data: data)!)
//                next.image = UIImage(data:data)
//            }
            
            
        }else if segue.identifier == "myformsegue"{
          
            let next = segue.destination as! FormView
            let cell = sender as? MyFormCollectionViewCell
            let index = myFormCollectionView.indexPath(for: cell!)
            
           
            
            if typeview == 1 && admin == false{
                next.username = self.username
             next.des = getformdetail[(index?.item)!].des
                next.owner = getformdetail[(index?.item)!].owner
            next.token = self.token
            next.nametitle = getformdetail[(index?.item)!].title
            next.id = getformdetail[(index?.item)!].id
            let raw = "\(url!)\(getformdetail[(index?.item)!].image!)"
            let  Nsurl = NSURL(string: raw )!
            
            if let data = try? Data(contentsOf: Nsurl as URL)
            {
                //  images.append(UIImage(data: data)!)
                next.image = UIImage(data:data)
                
            }
            }else if typeview == 2 && admin == false{
                next.username = self.username
                next.owner = getformshare[(index?.item)!].owner
                next.user_id = self.user_id
                next.token = self.token
                next.des = getformshare[(index?.item)!].des
                next.nametitle = getformshare[(index?.item)!].title
                
                next.id = getformshare[(index?.item)!].id
                let raw = "\(url!)\(getformshare[(index?.item)!].image ?? "nil")"
                let  Nsurl = NSURL(string: raw )!
                
                if let data = try? Data(contentsOf: Nsurl as URL)
                {
                    //  images.append(UIImage(data: data)!)
                    next.image = UIImage(data:data)
                }
            }else if typeview == 1 && admin == true{
                next.username = self.username
                next.des = getformdetailadmin[(index?.item)!].des
                next.owner = getformdetailadmin[(index?.item)!].owner
                next.token = self.token
                next.nametitle = getformdetailadmin[(index?.item)!].title
                next.id = getformdetailadmin[(index?.item)!].id
                let raw = "\(url!)\(getformdetailadmin[(index?.item)!].image!)"
                let  Nsurl = NSURL(string: raw )!
                
                if let data = try? Data(contentsOf: Nsurl as URL)
                {
                    //  images.append(UIImage(data: data)!)
                    next.image = UIImage(data:data)
                    
                }
            }else if typeview == 2 && admin == true{
                next.username = self.username
                next.des = getformdetail[(index?.item)!].des
                next.owner = getformdetail[(index?.item)!].owner
                next.token = self.token
                next.nametitle = getformdetail[(index?.item)!].title
                next.id = getformdetail[(index?.item)!].id
                let raw = "\(url!)\(getformdetail[(index?.item)!].image!)"
                let  Nsurl = NSURL(string: raw )!
                
                if let data = try? Data(contentsOf: Nsurl as URL)
                {
                    //  images.append(UIImage(data: data)!)
                    next.image = UIImage(data:data)
                    
                }
            }
        }else if segue.identifier == "groupssegue"{
            
            let next = segue.destination as! GroupView
            
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email
            next.position = self.position
            next.tel = self.tel
            
        }else if segue.identifier == "searchformsegue"{
            
            let next = segue.destination as! searchGroupandFormView
            
            next.datamyform = self.dataforms
        }
        
        
    }

  


}
