//
//  QuestionTypeTextCell.swift
//  Form
//
//  Created by MSU IT CS on 19/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class QuestionTypeTextCell: UITableViewCell {

    
    var actionText: ((String) -> ())?

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerText: UITextView!
    @IBOutlet weak var noLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        answerText.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}

extension QuestionTypeTextCell:UITextViewDelegate{
     func textViewDidEndEditing(_ textView: UITextView)
    {
        if let text = textView.text{
            actionText?(text)
        }
    }
}
