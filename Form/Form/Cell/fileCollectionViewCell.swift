//
//  fileCollectionViewCell.swift
//  Form
//
//  Created by MSU IT CS on 22/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class fileCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
