//
//  QuestionTypeDateCell.swift
//  Form
//
//  Created by MSU IT CS on 21/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class QuestionTypeDateCell: UITableViewCell {

    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerText: UITextField!
    @IBOutlet weak var noLabel:UILabel!
    
    var datePickerView:UIDatePicker!
    var actionText: ((String) -> ())?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        answerText.delegate = self
        
        
    }
    @IBAction func showDatePicker(_ sender: UITextField) {
        datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        
        sender.inputView = datePickerView
        
        let toolbar:UIToolbar = UIToolbar(frame:  CGRect(x: 0, y: 0, width: 0, height: 44))
        toolbar.barStyle = UIBarStyle.default
        
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped))
        let flexibSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped))
        
        toolbar.items = [cancelBtn,flexibSpace,doneBtn]
        
        sender.inputAccessoryView = toolbar
        
    }
    @objc func doneTapped() {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
//
        dateFormatter.setLocalizedDateFormatFromTemplate("YYYY-MM-dd")
        
        answerText.text = dateFormatter.string(from: datePickerView.date)
        
        answerText.resignFirstResponder()
        
    }
    @objc func cancelTapped() {
        answerText.resignFirstResponder()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension QuestionTypeDateCell:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text{
            actionText?(text)
        }
    }
}
