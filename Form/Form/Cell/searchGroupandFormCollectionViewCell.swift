//
//  searchGroupandFormCollectionViewCell.swift
//  Form
//
//  Created by MSU IT CS on 17/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import DropDown

class searchGroupandFormCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var viewcell: UIView!
    
    let dropdown = DropDown()
    var desText:String?
    var id_group:Int?
    var tmp: (() -> ())?
    var delete: (() -> ())?
    var actiontitle: ((String) -> ())?
    var actiondes: ((String) -> ())?
    var actionimage: ((UIImage) -> ())?
    var actionid:((Int) -> ())?
    
    override func awakeFromNib() {
        viewcell.layer.cornerRadius = 10
        viewcell.clipsToBounds = true
        viewcell.layer.borderWidth = 0.25
        
        dropdown.anchorView = editBtn
        dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.dataSource = ["Edit Group","Delete Group"]
        editBtn.addTarget(self, action: #selector(dropdownAction), for: .touchUpInside)
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            if item == "Edit Group"{
                self.actionid?(self.id_group!)
                self.actiontitle?(self.titleLabel.text!)
                self.actiondes?(self.desText!)
                self.actionimage?(self.imageview.image!)
                self.tmp?()
                
            }else if item == "Delete Group"{
                
                self.delete?()
                
            }
            
            
        }
        

    }
    
    @objc func dropdownAction(){
        dropdown.show()
        
    }
}
