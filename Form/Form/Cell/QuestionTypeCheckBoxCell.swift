//
//  QuestionTypeCheckBoxCell.swift
//  Form
//
//  Created by MSU IT CS on 25/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class QuestionTypeCheckBoxCell: UITableViewCell {

    @IBOutlet weak var checkBoxview: UIButton!
    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var view: UIView!
    
    var tmp:(() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        checkBoxview.layer.cornerRadius = checkBoxview.frame.height/2
        checkBoxview.clipsToBounds = true
        checkBoxview.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
        checkBoxview.layer.borderWidth = 0.125
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func checkBoxBtn(_ sender: Any) {
        tmp?()
    }
    
}
