//
//  imagecollectioncell.swift
//  Form
//
//  Created by MSU IT CS on 3/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class imagecollectioncell: UICollectionViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var cancelBtn: UIButton!
    var tmp: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cancelBtn.addTarget(self, action: #selector(cancel), for: .touchUpInside)
    }
    
    @objc func cancel(){
        tmp?()
    }
}
