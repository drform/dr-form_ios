//
//  QuestionTypeDropdownCell.swift
//  Form
//
//  Created by MSU IT CS on 25/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import DropDown

class QuestionTypeDropdownCell: UITableViewCell {

    @IBOutlet weak var questionText: UILabel!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var dropdownbtn: UIButton!
    @IBOutlet weak var answerLabel: UILabel!
    
    var actionText:((String) -> ())?
    let dropdown = DropDown()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      dropdown.anchorView = dropdownbtn
        dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.dataSource = ["Car", "Motorcycle", "Truck"]
        
        dropdownbtn.addTarget(self, action: #selector(dropdownAction), for: .touchUpInside)
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.dropdownbtn.setTitle(item, for: .normal)
            self.actionText?(item)
            
        }
        
        dropdownbtn.layer.cornerRadius = 10
        dropdownbtn.clipsToBounds = true
        dropdownbtn.layer.borderWidth = 0.125
        
        
        // Initialization code
    }

    @objc func dropdownAction(){
        dropdown.show()
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
