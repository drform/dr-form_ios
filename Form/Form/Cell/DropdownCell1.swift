//
//  DropdownCell1.swift
//  Form
//
//  Created by MSU IT CS on 1/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class DropdownCell1: UITableViewCell {

    
    @IBOutlet weak var titleText: MDCTextField!
    @IBOutlet weak var dataLabel: MDCTextField!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var deleteBt:UIView!
    @IBOutlet weak var view2: UIView!
    
    
    var titleTextController:MDCTextInputControllerOutlined?
    var TypeTextController:MDCTextInputControllerOutlined?
    var actionText: ((String) -> ())?
    var delete:(() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleText.delegate = self
        view.addBorder(edge: .right, color: UIColor.gray, borderWidth: 0.5)
        view.addBorder(edge: .left, color: UIColor.gray, borderWidth: 0.5)
        view.addBorder(edge: .top, color: UIColor.gray, borderWidth: 0.5)
        view2.addBorder(edge: .right, color: UIColor.gray, borderWidth: 0.5)
        view2.addBorder(edge: .left, color: UIColor.gray, borderWidth: 0.5)

        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        
        deleteBt.clipsToBounds = true
        deleteBt.layer.cornerRadius = deleteBt.frame.height/2
        
        dataLabel.isUserInteractionEnabled = false

        titleTextController = MDCTextInputControllerOutlined(textInput: titleText)
        TypeTextController = MDCTextInputControllerOutlined(textInput: dataLabel)
        
        titleTextController?.floatingPlaceholderScale = 0.99
        TypeTextController?.floatingPlaceholderScale = 0.99
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func deleteBtn(_ sender: UIView) {
        //delegate?.deleteTapped(self)
        delete?()
    }
    
    
}
extension DropdownCell1: UITextFieldDelegate {
   
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            actionText?(text)
        }
        
    }
    
}
extension UIView {
    func addBorder(edge: UIRectEdge, color: UIColor, borderWidth: CGFloat) {
        
        let seperator = UIView()
        self.addSubview(seperator)
        seperator.translatesAutoresizingMaskIntoConstraints = false
        
        seperator.backgroundColor = color
        
        if edge == .top || edge == .bottom
        {
            seperator.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
            seperator.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
            seperator.heightAnchor.constraint(equalToConstant: borderWidth).isActive = true
            
            if edge == .top
            {
                seperator.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
            }
            else
            {
                seperator.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
            }
        }
        else if edge == .left || edge == .right
        {
            seperator.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
            seperator.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
            seperator.widthAnchor.constraint(equalToConstant: borderWidth).isActive = true
            
            if edge == .left
            {
                seperator.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
            }
            else
            {
                seperator.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
            }
        }
    }
    
}
