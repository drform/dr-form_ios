//
//  QuestionTypeStringCell.swift
//  Form
//
//  Created by MSU IT CS on 19/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class QuestionTypeStringCell: UITableViewCell {

    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerText: UITextField!
    @IBOutlet weak var noLabel: UILabel!
    var actionText: ((String) -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
     answerText.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension QuestionTypeStringCell:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let text = textField.text{
            actionText?(text)
        }
    }
}
