//
//  ClicktolinkTableViewCell.swift
//  Form
//
//  Created by MSU IT CS on 22/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class ClicktolinkTableViewCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    
    var tap:(() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ClicktolinkTableViewCell.tapFunction))
        answerLabel.isUserInteractionEnabled = true
        answerLabel.addGestureRecognizer(tap)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        
                tap?()
            }
    
}
