//
//  HeaderCheckBoxCell.swift
//  Form
//
//  Created by MSU IT CS on 4/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class HeaderCheckBoxCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var questionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
