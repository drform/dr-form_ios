//
//  DropdownCell3.swift
//  Form
//
//  Created by MSU IT CS on 1/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class DropdownCell3: UITableViewCell {

    
    @IBOutlet weak var filterView: UIButton!
    @IBOutlet weak var requireView: UIButton!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var view2: UIView!
    
    var actionRequired: ((Bool) -> ())?
    var actionFilter: ((Bool) ->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainview.addBorder(edge: .right, color: UIColor.gray, borderWidth: 0.5)
        mainview.addBorder(edge: .left, color: UIColor.gray, borderWidth: 0.5)
        mainview.addBorder(edge: .bottom, color: UIColor.gray, borderWidth: 0.5)
        view2.addBorder(edge: .right, color: UIColor.gray, borderWidth: 0.5)
        view2.addBorder(edge: .left, color: UIColor.gray, borderWidth: 0.5)

        mainview.layer.cornerRadius = 10
        mainview.clipsToBounds = true
        
        requireView.layer.cornerRadius = 15
        requireView.layer.borderWidth = 0.25
        requireView.clipsToBounds = true
        
        filterView.layer.cornerRadius = 15
        filterView.layer.borderWidth = 0.25
        filterView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func ActionRequired(_ sender: Any) {
        
        
        
        if (requireView.isSelected){
            requireView.isSelected = false
            requireView.backgroundColor = .white
            actionRequired!(false)
            
            
            
        }else
        {
            requireView.isSelected = true
            requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)

            actionRequired!(true)
        }
        
    }
    
    @IBAction func ActionFilter(_ sender: Any) {
        
        
        if (filterView.isSelected){
            filterView.isSelected = false
            filterView.backgroundColor = .white
            actionFilter!(false)
            
            
            
        }else
        {
            filterView.isSelected = true
            filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            actionFilter!(true)
        }

    }
    
    
}
