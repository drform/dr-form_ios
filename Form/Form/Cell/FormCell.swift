//
//  FormCell.swift
//  Form
//
//  Created by MSU IT CS on 5/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit



class FormCell: UITableViewCell {

    
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var buttomview: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       mainview.clipsToBounds = true
        mainview.backgroundColor = UIColor.white
        mainview.layer.cornerRadius = 10
        mainview.layer.borderWidth = 0.125
        
        imageview.layer.cornerRadius = imageview.frame.height/2
        imageview.clipsToBounds = true
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        let padding = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
//        bounds = bounds.inset(by: padding)
//    }

}
