//
//  MyFormCollectionViewCell.swift
//  Form
//
//  Created by MSU IT CS on 30/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class MyFormCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var view: UIView!
    
    override func awakeFromNib() {
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.layer.borderWidth = 0.25
    }
}
