//
//  EditShareCell.swift
//  Form
//
//  Created by MSU IT CS on 14/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class EditShareCell: UITableViewCell {

    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!

    var tmp: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func cancelBt(_ sender: Any) {
        tmp?()
    }
}
