//
//  allimagevcollectionView.swift
//  Form
//
//  Created by MSU IT CS on 3/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import SafariServices

class allimagevcollectionView: UITableViewCell {

    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var view: UIView!
    var count:Int? = 10
    var show:Int? = 3
     var somedateString = [String]()
    var somedatefileString = [String]()
    var section = [imageanddate]()
    var sectionfile = [fileanddate]()
    var actionPath: ((String) -> ())?

    private let sectionInsets = UIEdgeInsets(top: 15.0,
                                             left: 15.0,
                                             bottom: 50.0,
                                             right: 40.0)
    private let itemsPerRow: CGFloat = 3
    
    var typeview:Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        somedateString.append("")
        somedatefileString.append("")
       let nibcell = UINib(nibName: "imagecollectioncell", bundle: nil)
        collectionview.register(nibcell, forCellWithReuseIdentifier: "imagecollectioncell")
        let nibcell2 = UINib(nibName: "fileCollectionViewCell", bundle: nil)
        collectionview.register(nibcell2, forCellWithReuseIdentifier: "fileCollectionViewCell")
        collectionview.register(UINib(nibName: "allimageReuseable", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "allimageReuseable")

        collectionview.dataSource = self
        collectionview.delegate = self
        
     
        self.collectionview.reloadData()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
 
    
}

extension allimagevcollectionView:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reuse : UICollectionReusableView? = nil
        if(kind == UICollectionView.elementKindSectionHeader){
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "allimageReuseable", for: indexPath) as! allimageReuseable
            
            if typeview == 2 {
                
           
            view.dateLabel.text = section[indexPath.section].date
                
            view.showallBtn.setImage(UIImage(named: "chevron-right"), for: .normal)
            view.showallBtn.tintColor =
                UIColor(red:0.65, green:0.65, blue:0.65, alpha:1.0)
            reuse = view
            }else if typeview == 3{
              
                
                view.dateLabel.text = sectionfile[indexPath.section].date

                view.showallBtn.setImage(UIImage(named: "chevron-right"), for: .normal)
                view.showallBtn.tintColor =
                    UIColor(red:0.65, green:0.65, blue:0.65, alpha:1.0)
                
                reuse = view
            }
            
            
        }
        return reuse!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if typeview == 3{
            collectionView.deselectItem(at: indexPath, animated: true)
             let path = sectionfile[indexPath.section].linkfile[indexPath.row]
          actionPath?(path)
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if typeview == 2{
            return section.count
        }else if typeview == 3{
           return sectionfile.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        return sectionInsets
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bound = collectionView.bounds
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize.init(width: widthPerItem,height: widthPerItem+50)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        

        if typeview == 2{
             return self.section[section].image.count
       
        }else if typeview == 3{
            return self.sectionfile[section].linkfile.count
      
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        if typeview == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagecollectioncell", for: indexPath) as! imagecollectioncell
            
            //        cell.imageview.image = UIImage(named: "galaxy")
            
            cell.imageview.image = section[indexPath.section].image[indexPath.row]
            
            return cell

        }else if typeview == 3{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fileCollectionViewCell", for: indexPath) as! fileCollectionViewCell
            
            if sectionfile[indexPath.section].name.count > 0 {
                cell.imageview.image = UIImage(named: "file")
                cell.nameLabel.text = sectionfile[indexPath.section].name[indexPath.row]
            }
            
         
            return cell
        }
        return UICollectionViewCell()
        
    }
    
    
    
    
    
}
