//
//  QuestionTypeIntCell.swift
//  Form
//
//  Created by MSU IT CS on 19/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class QuestionTypeIntCell: UITableViewCell {

    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerText: UITextField!
    
    @IBOutlet weak var noLabel: UILabel!
    var actionText: ((String) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        answerText.delegate = self
        answerText.keyboardType = UIKeyboardType.numberPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension QuestionTypeIntCell:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text{
            actionText?(text)
        }
    }
}
