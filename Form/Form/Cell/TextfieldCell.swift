//
//  TextfieldCell.swift
//  Form
//
//  Created by MSU IT CS on 7/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

protocol TextfieldCellDelegate {
    func sendToTitle(_ string: String)
    func sendTodes(_ string: String)
    func turnonoffrequired(_ required:Bool)
    func turnonofffilter(_ filter:Bool)
    func deleteTapped(_ cell: UITableViewCell)
}

class TextfieldCell: UITableViewCell {
    
    var actionText: ((String) -> ())?
    var actionRequired: ((Bool) -> ())?
    var actionFilter: ((Bool) ->())?
    var actionSave: ((String) -> ())?
    
    var tmp: (() -> ())?
    
    @IBOutlet weak var TitleText: MDCTextField!
    @IBOutlet weak var mainview: UIView!
    
   
    
    @IBOutlet weak var filterView: UIButton!
    @IBOutlet weak var requireView: UIButton!
    @IBOutlet weak var DatatypeLabel: MDCTextField!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var deleteBtnview: UIButton!
    
    var index: Int?
    var titleTextController:MDCTextInputControllerOutlined?
    var TypeTextController:MDCTextInputControllerOutlined?
    
    @IBAction func ActionRequired(_ sender: Any) {
      
       
        if (requireView.isSelected){
            requireView.isSelected = false
            actionRequired!(false)
            requireView.backgroundColor = .white

            
            
        }else
        {
            requireView.isSelected = true
            requireView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            actionRequired!(true)
        }
        
       
    }
    
    @IBAction func ActionFilter(_ sender: Any) {
       
       
        
        if (filterView.isSelected){
            filterView.isSelected = false
            filterView.backgroundColor = .white

            actionFilter!(false)
            
            
            
        }else
        {
            filterView.isSelected = true
            filterView.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.25, alpha:1.0)
            actionFilter!(true)
        }
    }
    
    @IBAction func deleteBtn(_ sender: UIView) {
        //delegate?.deleteTapped(self)
        tmp?() 
    }
    
   
    var delegate: TextfieldCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainview.layer.borderWidth = 0.25
        mainview.layer.cornerRadius = 10
        mainview.clipsToBounds = true
        TitleText.delegate = self
        
        deleteBtnview.clipsToBounds = true
        deleteBtnview.layer.cornerRadius = deleteBtnview.frame.height/2

        titleTextController = MDCTextInputControllerOutlined(textInput: TitleText)
        TypeTextController = MDCTextInputControllerOutlined(textInput: DatatypeLabel)
        
        titleTextController?.floatingPlaceholderScale = 0.99
        TypeTextController?.floatingPlaceholderScale = 0.99
        
        DatatypeLabel.isUserInteractionEnabled = false
        
        filterView.layer.cornerRadius = 15
        filterView.layer.borderWidth = 0.25
        filterView.clipsToBounds = true
        
        requireView.layer.cornerRadius = 15
        requireView.layer.borderWidth = 0.25
        requireView.clipsToBounds = true
         
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    
}



extension TextfieldCell: UITextFieldDelegate {

  
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            actionText?(text)
        }
        
    }
    
}
