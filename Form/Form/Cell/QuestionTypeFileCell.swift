//
//  QuestionTypeFileCell.swift
//  Form
//
//  Created by MSU IT CS on 20/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class QuestionTypeFileCell: UITableViewCell {

    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var nameFileLabel: UILabel!
    @IBOutlet weak var importfileView: UIButton!
    
    
    var tmp:(() -> ())?
    var tap:(() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        let tap = UITapGestureRecognizer(target: self, action: #selector(QuestionTypeFileCell.tapFunction))
//        nameFileLabel.isUserInteractionEnabled = true
//        nameFileLabel.addGestureRecognizer(tap)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func importfile(_ sender: Any) {
            tmp?()
        
    }
//    @objc func tapFunction(sender:UITapGestureRecognizer) {
//
//        tap?()
//    }
    
    
}
