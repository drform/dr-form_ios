//
//  QuestionTypeFloatCell.swift
//  Form
//
//  Created by MSU IT CS on 21/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class QuestionTypeFloatCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerText: UITextField!
    var actionText: ((String) -> ())?

    @IBOutlet weak var noLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        answerText.delegate = self
        answerText.keyboardType = UIKeyboardType.decimalPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension QuestionTypeFloatCell:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text{
            actionText?(text)
        }
    }
}
