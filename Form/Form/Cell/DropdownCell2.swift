//
//  DropdownCell2.swift
//  Form
//
//  Created by MSU IT CS on 1/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class DropdownCell2: UITableViewCell {

    
    @IBOutlet weak var choiceText: MDCTextField!
    @IBOutlet weak var AddBtn: UIButton!
    @IBOutlet weak var view: UIView!
    
    var actionToAdd: (() -> ())?
    var actionTextField: ((String) -> ())?
    var choiceTextController: MDCTextInputControllerOutlined?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        AddBtn.addTarget(self, action: #selector(ActionAddBtn), for: .touchUpInside)
        choiceText.delegate = self
        // Initialization code
        view.addBorder(edge: .right, color: UIColor.gray, borderWidth: 0.5)
        view.addBorder(edge: .left, color: UIColor.gray, borderWidth: 0.5)
        
        AddBtn.layer.cornerRadius = AddBtn.frame.height/2
        AddBtn.clipsToBounds = true
        
        choiceTextController = MDCTextInputControllerOutlined(textInput: choiceText)
        
        choiceTextController?.floatingPlaceholderScale = 0.99
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func ActionAddBtn(_ sender: Any) {
        actionToAdd?()
        
    }
    
    
}
extension DropdownCell2:UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
           actionTextField?(text)
        }
        
    }
    
}
