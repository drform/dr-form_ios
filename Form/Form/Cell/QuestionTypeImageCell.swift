//
//  QuestionTypeImageCell.swift
//  Form
//
//  Created by MSU IT CS on 19/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class QuestionTypeImageCell: UITableViewCell,UIImagePickerControllerDelegate {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var imageviews: UIImageView!
    @IBOutlet weak var importBtnview: UIButton!
    @IBOutlet weak var noLabel: UILabel!
    
    var tmp:(() -> ())?
    var base64String:String?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func importBtn(_ sender: Any) {
        tmp?()
    }

   
}
