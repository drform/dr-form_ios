//
//  ShowMultipleImageCell.swift
//  Form
//
//  Created by MSU IT CS on 13/3/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class ShowMultipleImageCell: UITableViewCell{
    

    
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var view: UIView!
    var tmp:(() -> ())?
    var allimage = [UIImage]()
    var images = [UIImage]()
    var PhotoArray = [photoarray]()
    var rowimage = [didRow]()
    var SelectedAssets = [selectedassets]()
    var cancelbtstatus:Int?
    //var imageanswers = [allimage]()
    var idimage:Int?
    
    var actionPhotoArray: (([photoarray]) -> ())?
    var actionrowimage: (([didRow]) -> ())?
    var actionSelectedAssets: (([selectedassets]) -> ())?
    var actionindeximage: ((Int) -> ())?


    @IBOutlet weak var questionLabel: UILabel!
    
    private let sectionInsets = UIEdgeInsets(top: 15.0,
                                            left: 15.0,
                                            bottom: 15.0,
                                            right: 40.0)
    private let itemsPerRow: CGFloat = 3

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      collectionview.delegate = self
        collectionview.dataSource = self
        
        let nibcell = UINib(nibName: "imagecollectioncell", bundle: nil)
        collectionview.register(nibcell, forCellWithReuseIdentifier: "imagecollectioncell")
        
        
//        if imageanswers.count > 0 {
//            allimage.removeAll()
//            imageanswers.forEach { (image) in
//                self.allimage.append(image.image)
//                self.collectionview.reloadData()
//            }
//        }
        self.collectionview.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
}

extension ShowMultipleImageCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
      return allimage.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return sectionInsets
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bound = collectionView.bounds
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize.init(width: widthPerItem,height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagecollectioncell", for: indexPath) as! imagecollectioncell
        
        if cancelbtstatus == 1{
            cell.cancelBtn.isHidden = false
        }
        //        cell.imageview.image = UIImage(named: "galaxy")
//        if PhotoArray.count != 0{
//        cell.imageview.image = PhotoArray[indexPath.section].image[indexPath.row]
//        }else{
//            cell.imageview.image = allimage[indexPath.row]
//        }
        
         cell.imageview.image = allimage[indexPath.row]
        
        cell.tmp = {
            if self.idimage != nil{
            self.actionindeximage?(indexPath.row)
            
            self.allimage.remove(at: indexPath.row)
            }else{
//                self.PhotoArray[indexPath.section].image.remove(at: indexPath.row)
//                self.rowimage[indexPath.section].rows.remove(at: indexPath.row)
//                self.rowimage[indexPath.section].choices.remove(at: indexPath.row )
//                self.SelectedAssets[indexPath.section].phasset.remove(at: indexPath.row )
//                self.allimage.remove(at: indexPath.row)
//                self.actionPhotoArray?(self.PhotoArray)
//                self.actionrowimage?(self.rowimage)
//                self.actionSelectedAssets?(self.SelectedAssets)
                self.actionindeximage?(indexPath.row)
                
                self.allimage.remove(at: indexPath.row)
            }
            //
//
//
           // self.idimage = self.imageanswers[indexPath.row].id
           // self.imageanswers.remove(at: indexPath.row).id
     
            
            
            
            
            self.collectionview.reloadData()
            
            
            
        }
        
        return cell
    }
    
    
    
    
    
}

